<?php

namespace frontend\modules\api\models;

use Yii;

/**
 * This is the model class for table "restaurant_discounts".
 *
 * @property string $pkRestaurantDiscountsID
 * @property string $fkRestaurantID
 * @property integer $discount
 * @property string $discountImage
 * @property integer $pointsEarned
 */
class RestaurantDiscounts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restaurant_discounts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkRestaurantID', 'discount', 'pointsEarned'], 'integer'],
            [['discountImage'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkRestaurantDiscountsID' => 'Pk Restaurant Discounts ID',
            'fkRestaurantID' => 'Fk Restaurant ID',
            'discount' => 'Discount',
            'discountImage' => 'Discount Image',
            'pointsEarned' => 'Points Earned',
        ];
    }
}
