<?php

namespace frontend\modules\api\models;

use Yii;

/**
 * This is the model class for table "user_restaurant_favourite".
 *
 * @property string $pkUserRestaurantFavouriteID
 * @property string $fkUserID
 * @property string $fkRestaurantID
 * @property integer $favourite
 */
class UserRestaurantFavourite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_restaurant_favourite';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkUserID', 'fkRestaurantID', 'favourite'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkUserRestaurantFavouriteID' => 'Pk User Restaurant Favourite ID',
            'fkUserID' => 'Fk User ID',
            'fkRestaurantID' => 'Fk Restaurant ID',
            'favourite' => 'Favourite',
        ];
    }
}
