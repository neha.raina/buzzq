<?php

namespace frontend\modules\api\models;

use Yii;

/**
 * This is the model class for table "user_points_earned".
 *
 * @property string $pkUserPointsEarnedID
 * @property string $fkUserID
 * @property string $fkRestaurantID
 * @property string $fkUserRestaurantBookingID
 * @property integer $userPointsEarned
 */
class UserPointsEarned extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_points_earned';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkUserID', 'fkRestaurantID', 'fkUserRestaurantBookingID', 'userPointsEarned'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkUserPointsEarnedID' => 'Pk User Points Earned ID',
            'fkUserID' => 'Fk User ID',
            'fkRestaurantID' => 'Fk Restaurant ID',
            'fkUserRestaurantBookingID' => 'Fk User Restaurant Booking ID',
            'userPointsEarned' => 'User Points Earned',
        ];
    }
}
