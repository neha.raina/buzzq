<?php

namespace frontend\modules\api\models;

use Yii;

/**
 * This is the model class for table "restaurant_notifications".
 *
 * @property string $pkRestaurantNotificationsID
 * @property string $fkRestaurantID
 * @property string $notificationImage
 * @property integer $discount
 * @property integer $pointsEarned
 */
class RestaurantNotifications extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restaurant_notifications';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkRestaurantID', 'discount', 'pointsEarned'], 'integer'],
            [['notificationImage'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkRestaurantNotificationsID' => 'Pk Restaurant Notifications ID',
            'fkRestaurantID' => 'Fk Restaurant ID',
            'notificationImage' => 'Notification Image',
            'discount' => 'Discount',
            'pointsEarned' => 'Points Earned',
        ];
    }
}
