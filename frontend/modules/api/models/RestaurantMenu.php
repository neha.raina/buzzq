<?php

namespace frontend\modules\api\models;

use Yii;

/**
 * This is the model class for table "restaurant_menu".
 *
 * @property string $pkRestaurantMenuID
 * @property string $fkRestaurantID
 * @property string $restaurantPicture
 */
class RestaurantMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restaurant_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkRestaurantID'], 'integer'],
            [['restaurantPicture'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkRestaurantMenuID' => 'Pk Restaurant Menu ID',
            'fkRestaurantID' => 'Fk Restaurant ID',
            'restaurantPicture' => 'Restaurant Picture',
        ];
    }
}
