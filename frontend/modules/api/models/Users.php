<?php

namespace frontend\modules\api\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property string $pkUserID
 * @property string $userEmail
 * @property string $userPassword
 * @property string $userResetToken
 * @property string $userAuthKey
 * @property string $userOTP
 * @property string $userSignupType
 * @property string $userName
 * @property string $userNumber
 * @property string $userDOB
 * @property string $userSex
 * @property string $userNationality
 * @property string $userPicture
 * @property string $userDeviceToken
 * @property integer $userVip
 * @property integer $userEmailVerified
 * @property string $userStatus
 * @property string $userAdded
 * @property string $userModified
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userDOB', 'userAdded', 'userModified'], 'safe'],
            [['userVip', 'userEmailVerified'], 'integer'],
            [['userEmail', 'userPassword'], 'string', 'max' => 150],
            [['userResetToken'], 'string', 'max' => 255],
            [['userAuthKey'], 'string', 'max' => 100],
            [['userOTP'], 'string', 'max' => 15],
            [['userSignupType', 'userName'], 'string', 'max' => 50],
            [['userNumber', 'userSex', 'userNationality'], 'string', 'max' => 50],
            [['userPicture'], 'string', 'max' => 1000],
            [['userDeviceToken'], 'string', 'max' => 500],
            [['userStatus'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkUserID' => 'Pk User ID',
            'userEmail' => 'User Email',
            'userPassword' => 'User Password',
            'userResetToken' => 'User Reset Token',
            'userAuthKey' => 'User Auth Key',
            'userOTP' => 'User Otp',
            'userSignupType' => 'User Signup Type',
            'userName' => 'User Name',
            'userNumber' => 'User Number',
            'userDOB' => 'User Dob',
            'userSex' => 'User Sex',
            'userNationality' => 'User Nationality',
            'userPicture' => 'User Picture',
            'userDeviceToken' => 'User Device Token',
            'userVip' => 'User Vip',
            'userEmailVerified' => 'User Email Verified',
            'userStatus' => 'User Status',
            'userAdded' => 'User Added',
            'userModified' => 'User Modified',
        ];
    }
}
