<?php

namespace frontend\modules\api\models;

use Yii;

/**
 * This is the model class for table "restaurant_city".
 *
 * @property integer $pkRestaurantCityID
 * @property string $cityName
 */
class RestaurantCity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restaurant_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cityName'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkRestaurantCityID' => 'Pk Restaurant City ID',
            'cityName' => 'City Name',
        ];
    }
}
