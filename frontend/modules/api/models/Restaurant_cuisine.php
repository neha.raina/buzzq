<?php

namespace frontend\modules\api\models;

use Yii;

/**
 * This is the model class for table "restaurant_cuisine".
 *
 * @property string $pkRestaurantCuisineID
 * @property string $fkRestaurantID
 * @property string $cuisineName
 */
class Restaurant_cuisine extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restaurant_cuisine';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkRestaurantID'], 'integer'],
            [['cuisineName'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkRestaurantCuisineID' => 'Pk Restaurant Cuisine ID',
            'fkRestaurantID' => 'Fk Restaurant ID',
            'cuisineName' => 'Cuisine Name',
        ];
    }
}
