<?php

namespace frontend\modules\api\models;


use Yii;
use yii\base\Model;
use yii\db\Expression;

use frontend\modules\api\models\Users;
use frontend\modules\api\models\Restaurant;
use frontend\modules\api\models\Restaurant_cuisine;
use frontend\modules\api\models\RestaurantCity;
use frontend\modules\api\models\RestaurantLocation;
use frontend\modules\api\models\Restaurant_menu;
use frontend\modules\api\models\RestaurantCuisineNames;
use frontend\modules\api\models\UserRestaurantBooking;
use frontend\modules\api\models\UserRestaurantFavourite; 
use frontend\modules\api\models\RestaurantDiscounts;
use frontend\modules\api\models\UserPointsEarned;
use frontend\modules\api\models\RestaurantNotifications;


use yii\helpers\Url;

use common\models\Users AS BaseUser;
use frontend\models\PasswordResetRequestForm;

/**
 * This is the model class for table "restaurant".
 *
 * @property string $pkRestaurantID
 * @property string $restaurantName
 * @property string $restaurantDescription
 * @property string $restaurantCity
 * @property string $restaurantTimings
 * @property string $restaurantType
 * @property string $restaurantLogo
 * @property string $restaurantLocation
 * @property string $restaurantAddress
 */
class App extends Model
{

    public static $errorMessage;
    public static $errorCode;

    /*****************************************************************
     *          Account and Profile Related WebServices
     ****************************************************************/
    
    /**
     * getOTP
     * Generate OTP and send it to mobile number
     * Insert a record in user table with that number
     * @param $varNumber
     */
    public static function validateOTP($request){
    
        
        $data = array();
        $otpTimePeriod = 6;

        // Requests
        //$varEmail = (isset($request->email)) ? $request->email : NULL;
        $varNumber = (isset($request->number)) ? $request->number : NULL;
        $varOTP = (isset($request->otp)) ? $request->otp : NULL;
        $varUserID = (isset($request->userId)) ? $request->userId : NULL;
        $varPassword = (isset($request->password)) ? $request->password : NULL;
        /**
         * @var $type ['signup', 'forgetPassword', 'updateNumber']
         */
        $varType = (isset($request->type)) ? $request->type : NULL;

        if($varOTP && $varUserID && $varType)
        {
                /** Find user with that userID */
                $user = Users::findOne(['pkUserID' => $varUserID]);
            
            if($user)
            {
                $time1 = $user->userOtpTime;
                $time2 = time();
                $timeInterval  = abs($time1 - $time2);
                $minutes   = round($timeInterval / 60);
                
                if($minutes < $otpTimePeriod){

                if($user->userOTP == $varOTP)
                {   

                    if($varType == 'signup')
                    {
                        /**
                         * If type is signup
                         * Just active the user account
                         */

                        $user->userStatus = 'Active';
                        if($user->save(false))
                        {
                            $data['code'] = 200;
                            $data['message'] = 'Registration Successful';
                        }
                        else
                        {
                            $modalErrors = $user->getErrors();
                            $data['code'] = 500;
                            foreach($modalErrors as $modalError)
                            {
                                $data['message'] = $modalError[0];
                            }
                        }

                    }
                    elseif($varType == 'forgetPassword')
                    {
                        /**
                         * if type is forgetPassword
                         * change the user password
                         */
                        if($varOTP && $varUserID && $varPassword){

                        $varPasswordHash = Yii::$app->getSecurity()->generatePasswordHash($varPassword);
                        $user = Users::findOne(['pkUserID' => $varUserID]);
                        if($user){
                        // User Properties
                        $user->userPassword = $varPasswordHash;
                         if($user->update()){


                        $data['code'] = 200;
                        $data['message'] = 'Password has been changed';
                          
                        }else{
                        
                        $data['code'] = 500;
                        $data['message'] = 'Error while changing your password';

                        }
                        }else{

                        $data['code'] = 500;
                        $data['message'] = 'User doesnot exists';
                        }
                        }else{

                        $data['code'] = 500;
                        $data['message'] = 'One of more parameters are missing for forget password';

                        }
                        


                    }
                    else
                    {
                        /**
                         * if type is updateNumber
                         * Move number from temp to permanent
                         */
                        if($varNumber){
                        $user->userNumber = $varNumber;
                        
                        if($user->save(false))
                        {
                            $data['code'] = 200;
                            $data['message'] = 'Number updated successfully';
                        }
                        else
                        {
                            $modalErrors = $user->getErrors();
                            $data['code'] = 500;
                            foreach($modalErrors as $modalError)
                            {
                                $data['message'] = $modalError[0];
                            }
                        }
                    }else{
                        $data['code'] = 500;
                        $data['message'] = 'Number cannot be empty';
                    }
                    }
                }
                else
                {
                    $data['code'] = 500;
                    $data['message'] = 'OTP doesnot match';                    
                }
            }else{

                $data['code'] = 500;
                $data['message'] = 'OTP expired'; 
            }
            }
            else
            {
                $data['code'] = 500;
                $data['message'] = 'This user id is not registered';
            }
        }
        else
        {
            $data['code'] = 500;
            $data['message'] = 'One or more parameters are missing';
        }

        return $data;
    }

    public static function signup($request){
        $data = array();

        $userNumber = (isset($request->contactNumber)) ? $request->contactNumber : NULL;
        $userPassword = (isset($request->password)) ? $request->password : NULL;
        $deviceToken = (isset($request->deviceToken)) ? $request->deviceToken : NULL;
        
        if($userPassword && $userNumber)
        {
            $user = Users::findOne(['userNumber' => $userNumber]);
            if($user)
            {

               $data['code'] = 500;
               $data['message'] = 'This number is already exists';
                     
            }else{
                $varPasswordHash = Yii::$app->getSecurity()->generatePasswordHash($userPassword);
                $otp = rand(100000,999999);
                $user2 = new Users();
                // User Properties
                $user2->userPassword = $varPasswordHash;
                $user2->userNumber = $userNumber;
                $user2->userOTP = (string) $otp;
                $user2->userAdded = new Expression('NOW()');
                $user2->userStatus = 'Pending';
                $user2->userAuthKey = Yii::$app->security->generateRandomString();
                $user2->userSignupType = 'custom';
                $user2->userOtpTime = time();


                if($deviceToken)
                {
                    $user2->userDeviceToken = $deviceToken;
                }

                // Signup a new account
                if($user2->save(false))
                {

                    $newUser = Users::findOne(['userNumber' => $userNumber]);
                    // /**
                    //  * @todo Send OTP to email
                    //  */
                    // Yii::$app
                    //     ->mailer
                    //     ->compose(
                    //         ['html' => 'otp/html', 'text' => 'otp/text'],
                    //         ['user' => $user]
                    //     )
                    //     ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
                    //     ->setTo($user->userEmail)
                    //     ->setSubject('Welcome to '.Yii::$app->params['appName'])
                    //     ->send();

                    // /**
                    //  * @todo Send OTP to SMS
                    //  */
                    // $smsTemplate = sprintf('Hi %s'.PHP_EOL.'%s is your OTP. Please dont share it with anyone. OTP is also sent to your registered email id.', $user->userName, $user->userOTP);

                    // Yii::$app->sms->send($userNumber, $smsTemplate);

                     
                     $data['code'] = 200;
                     $data['message'] = 'Validate OTP';
                     $data['userId'] = $newUser->pkUserID;
                     $data['otpType'] = 'signup';
                     $data['otp'] = $otp;   

                }
                else
                {

                    $data['code'] = 500;

                    $modalErrors = $user->getErrors();
                    foreach($modalErrors as $modalError)
                    {
                        $data['message'] = $modalError[0];
                    }
                }  
            }

        }
        else
        {
            $data['code'] = 501;
            $data['message'] = 'One or more parameters are missing';
        }

        return $data;

    }

    /**
     * Resend OTP WebService
     * Regenrate OTP and Send it to the User SMS/Email
     *
     * @param integer $userId
     */
    public static function resendOTP($request)
    {

        $data = array();

        $userId = (isset($request->userId)) ? $request->userId : NULL;

        if($userId)
        {
            $user = Users::findOne(['pkUserID' => $userId]);

            // Find or fail
            if($user)
            {
                
                $otp = rand(100000,999999);
                $user->userOTP = (string) $otp;
                $user->userOtpTime = time();
                if($user->update())
                {

                    /**
                     * Send OTP code to SMS and Email
                     */
                    // Yii::$app
                    //     ->mailer
                    //     ->compose(
                    //         ['html' => 'otp/html', 'text' => 'otp/text'],
                    //         ['user' => $user]
                    //     )
                    //     ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
                    //     ->setTo($user->userEmail)
                    //     ->setSubject('Validate OTP | '.Yii::$app->params['appName'])
                    //     ->send();


                    /**
                     * @todo Send OTP to SMS
                     */
                    // $smsTemplate = sprintf('Hi %s'.PHP_EOL.'%s is your OTP. Please dont share it with anyone. OTP is also sent to your registered email id.', $user->userName, $user->userOTP);

                    // Yii::$app->sms->send($user->userNumber, $smsTemplate);


                    $data['code'] = 200;
                    $data['message'] = 'OTP resend successfully';
                    $data['otp'] = $otp;
                }
                else
                {
                    $data['code'] = 500;
                    $data['message'] = 'Error while resending otp';
                }
            }
            else
            {
                $data['code'] = 500;
                $data['message'] = 'This user id is not registered';
            }
        }
        else
        {
            $data['code'] = 500;
            $data['message'] = 'One or more parameters are missing';
        }

        return $data;
    }

    /**
     * Login user to app
     * @param $varNumber
     * @param $varPassword
     * @param $socialID
     * @param $deviceToken
     * @param $loginType
     */
    public static function login($request)
    {

        $data = array();

        $userNumber = (isset($request->number)) ? $request->number : NULL;
        $userPassword = (isset($request->password)) ? $request->password : NULL;
        $socialID = (isset($request->socialID)) ? $request->socialID : NULL;    
        $deviceToken = (isset($request->deviceToken)) ? $request->deviceToken : NULL;
        /*
        *loginType :- facebook/google/custom
        */
        $loginType = (isset($request->loginType)) ? $request->loginType : NULL;

        if($loginType == 'custom')
        {

            if($userNumber && $userPassword)
            {
                $user = BaseUser::find()->where(['userNumber' => $userNumber])->one();
                
                if($user)
                {
                    $validatePassword = $user->validatePassword($userPassword);
                    if($validatePassword)
                    {   
                        if($user->userStatus == 'Active')
                        {


                            if($deviceToken)
                            {
                                $user->userDeviceToken = $deviceToken;
                                $user->save();
                            }

                            $data['code'] = 200;
                            $data['message'] = "Login successfull";
                            $data['userId'] = $user->pkUserID;
                            $data['name'] = $user->userName;
                            $data['email'] = $user->userEmail;
                            $data['mobileNumber'] = $user->userNumber;
                            $data['loginType'] = $user->userSignupType;
                            $data['dob'] = $user->userDOB;
                            $data['gender'] = $user->userSex;
                            $data['nationality'] = $user->userNationality;
                                if($user->userPicture)
                            {
                               $data['profileImage'] =  $user->userPicture;
                            }
                            else
                            {
                               $data['profileImage'] =  "NULL"; 
                            }
                               $data['userVip'] = $user->userVip;
                        }
                        elseif ($user->userStatus == 'Pending')
                        {
                            
                            $otp = rand(100000,999999);
                            if($deviceToken)
                            {
                                $user->userDeviceToken = $deviceToken;
                            }
                            $user->userSignupType = 'custom';
                            $user->userOTP = (string) $otp;
                            $user->userOtpTime = time();
                            if($user->save())
                            {
                            // Signup a new account
                           
                                // /**
                                //  * @todo Send OTP to email
                                //  */
                                // Yii::$app
                                //     ->mailer
                                //     ->compose(
                                //         ['html' => 'otp/html', 'text' => 'otp/text'],
                                //         ['user' => $user]
                                //     )
                                //     ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
                                //     ->setTo($user->userEmail)
                                //     ->setSubject('Welcome to '.Yii::$app->params['appName'])
                                //     ->send();

                                // /**
                                //  * @todo Send OTP to SMS
                                //  */
                                // $smsTemplate = sprintf('Hi %s'.PHP_EOL.'%s is your OTP. Please dont share it with anyone. OTP is also sent to your registered email id.', $user->userName, $user->userOTP);

                                // Yii::$app->sms->send($userNumber, $smsTemplate);


                                 $data['code'] = 201;
                                 $data['message'] = 'OTP not verify';
                                 $data['userId'] = $user->pkUserID;
                                 $data['otpType'] = 'signup';   
                                 $data['otp'] = $otp;

                            }
                            else
                            {

                                $data['code'] = 500;

                                $modalErrors = $user->getErrors();
                                foreach($modalErrors as $modalError)
                                {
                                    $data['message'] = $modalError[0];
                                }
                            }

                        }
                        else
                        {
                            $data['code'] = 500;
                            $data['message'] = 'Your account is currently Deactivated';
                        }


                    }
                    else
                    {
                        $data['code'] = 500;
                        $data['message'] = 'Incorrect password';
                    }
                }
                else
                {
                    $data['code'] = 500;
                    $data['message'] = 'Incorrect Number';
                }
            }
            else
            {
                $data['code'] = 500;
                $data['message'] = 'One or more parameters are missing for custom login';
            }

        }elseif(($loginType == 'facebook') || ($loginType == 'google')){

           if($socialID)
           {

            $user = BaseUser::findOne(['userSocialID' => $socialID]);
            if($user)
            {
                if($userNumber)
                {
                $user->userNumber = $userNumber;
                $user->save();
                }
                /*
                *User already exists ,login to user
                */
                    $data['code'] = 200;
                    $data['message'] = "Login successfull";
                    $data['userId'] = $user->pkUserID;
                    $data['name'] = $user->userName;
                    $data['email'] = $user->userEmail;
                    $data['mobileNumber'] = $user->userNumber;
                    $data['loginType'] = $user->userSignupType;
                    $data['socialID'] = $user->userSocialID;
                    $data['dob'] = $user->userDOB;
                    $data['gender'] = $user->userSex;
                    $data['nationality'] = $user->userNationality;
                         if($user->userPicture)
                         {
                            $data['profileImage'] =  $user->userPicture;
                         }
                         else
                         {
                            $data['profileImage'] =  "NULL"; 
                         }  
                    $data['userVip'] = $user->userVip;  

            }
            else
            {
                /*
                *Make new user
                */
                $user = new Users();
                // User Properties
                $user->userNumber = $userNumber;
                $user->userAdded = new Expression('NOW()');
                $user->userStatus = 'Active';
                $user->userAuthKey = Yii::$app->security->generateRandomString();
                $user->userSignupType = $loginType;
                $user->userSocialID = $socialID;
                if($deviceToken)
                {
                    $user->userDeviceToken = $deviceToken;
                }
                // Signup a new account
                if($user->save())
                {
                    $currentUser = BaseUser::findOne(['pkUserID' => $user->pkUserID]);

                    $data['code'] = 200;
                    $data['message'] = "Login successfull";
                    $data['userId'] = $currentUser->pkUserID;
                    $data['name'] = $currentUser->userName;
                    $data['email'] = $user->userEmail;
                    $data['mobileNumber'] = $currentUser->userNumber;
                    $data['loginType'] = $currentUser->userSignupType;
                    $data['socialID'] = $currentUser->userSocialID;
                    $data['dob'] = $user->userDOB;
                    $data['gender'] = $user->userSex;
                    $data['nationality'] = $user->userNationality;
                         if($user->userPicture)
                         {
                            $data['profileImage'] =  $user->userPicture;
                         }
                         else
                         {
                            $data['profileImage'] =  "NULL"; 
                         }
                    $data['userVip'] = $currentUser->userVip;
                }
                else
                {

                    $data['code'] = 500;

                    $modalErrors = $user->getErrors();
                    foreach($modalErrors as $modalError)
                    {
                        $data['message'] = $modalError[0];
                    }
                }  


            }
           }
           else
           {

                $data['code'] = 500;
                $data['message'] = 'socialID is not empty';
           }


       }else{
        $data['code'] = 500;
        $data['message'] = 'Login type is not correct';

      }

        return $data;
    }

    /**
     * Forget Password
     * @param $varEmail
     */
    public static function forgetPassword($request)
    {

        $data = array();

        $varNumber = (isset($request->contactNumber)) ? $request->contactNumber : NULL;
        $varNewPassword = (isset($request->newPassword)) ? $request->newPassword : NULL;

        if($varNumber && $varNewPassword)
        {
            
            $user = Users::findOne(['userNumber' => $varNumber]);
            // $data['test'] = $user;
            
            if($user)
            {
            // $data['test2'] = $otpp;
                $otp = rand(100000,999999);
                $user->userOTP = (string) $otp;
                $user->userOtpTime = time();

                if($user->save())
                {

                    // if($model->sendEmail())
                    // {
                    //     $userModal = Users::findOne(['userEmail' => $varEmail]);

                    //     if($userModal)
                    //     {
                    //         /**
                    //          * @todo Send OTP to SMS
                    //          */
                    //         $smsTemplate = sprintf(PHP_EOL.'%s is your OTP. Please dont share it with anyone. OTP is also sent to your registered email id.', $model->otp);
                    //         Yii::$app->sms->send($userModal->userNumber, $smsTemplate);
                    //     }


                    //     $data['code'] = 200;
                    //     $data['message'] = 'OTP to reset password sent to your registered mobile number';
                    //     $data['OTP'] = $model->otp;
                    // }
                    // else
                    // {
                    //     $data['code'] = 500;
                    //     $data['message'] = 'This phone number is not registered with us';
                    // }
                        $data['code'] = 200;
                        $data['message'] = 'OTP to forget password sent to your registered mobile number';
                        $data['otp'] = $user->userOTP;
                        $data['otpType'] = 'forgetPassword';
                        $data['userId'] = $user->pkUserID;
                        $data['userPassword'] = $varNewPassword;
                }
                else
                {

                    $data['code'] = 500;
                    $data['message'] = 'Error while sending OTP';
                }
            }
            else
            {

                $data['code'] = 500;
                $data['message'] = 'This number is not exists';
            }
        }
        else
        {
            $data['code'] = 500;
            $data['message'] = 'One or more parameters are missing';
        }
            

        return $data;
    }

    /**
     * All cities names
     * @return json cities names
     */
    public static function getCities($request)
    {

        $data = array();
        $restaurantCity = RestaurantCity::find()->select(['cityName', 'pkRestaurantCityID'])->groupBy('cityName')->all();
        if($restaurantCity)
        {
            $cities = array();

            foreach($restaurantCity as $restaurantCities)
            {

                $cityName = (isset($restaurantCities->cityName)) ? $restaurantCities->cityName : NULL;
                $cityID = (isset($restaurantCities->pkRestaurantCityID)) ? $restaurantCities->pkRestaurantCityID : NULL;

                $cities[] = array(
                    'cityID'   => $cityID,
                    'cityName' => $cityName
                    );
            }

                $data['code'] = 200;
                $data['message'] = 'City Names';
                $data['cities'] = $cities;

        }
        else
        {
                $data['code'] = 500;
                $data['message'] = 'No city found';
        }

        return $data;
        
    }

    /**
     * All cuisines names
     * @return json cities names
     */
    public static function getCuisines($request)
    {

        $data = array();
        $restaurantCuisines = RestaurantCuisineNames::find()->select(['cuisineName', 'pkRestaurantCuisineNameID'])->groupBy('cuisineName')->all();
        if($restaurantCuisines)
        {
            $cuisines = array();

            foreach($restaurantCuisines as $newRestaurantCuisines)
            {

                $cuisineName = (isset($newRestaurantCuisines->cuisineName)) ? $newRestaurantCuisines->cuisineName : NULL;
                $cuisineID = (isset($newRestaurantCuisines->pkRestaurantCuisineNameID)) ? $newRestaurantCuisines->pkRestaurantCuisineNameID : NULL;

                $cuisines[] = array(
                    'cuisineID'   => $cuisineID,
                    'cuisineName' => $cuisineName
                    );
            }

                $data['code'] = 200;
                $data['message'] = 'Cuisine Names';
                $data['cuisines'] = $cuisines;

        }
        else
        {
                $data['code'] = 500;
                $data['message'] = 'No cuisine found';
        }

        return $data;
        
    }

    /**
     * All locationa names by city id
     * @return json location names
     */
    public static function getLocations($request)
    {

        $data = array();

        $cityID = (isset($request->cityID)) ? $request->cityID : NULL;
     if($cityID)
     {

        if(($cityID == 'all') || ($cityID == 'All'))
        {
            $restaurantLocations = RestaurantLocation::find()->groupBy('locationName')->all();
        }
        else
        {
            $restaurantLocations = RestaurantLocation::find()->select(['locationName', 'pkRestaurantLoactionID'])->where(['fkRestaurantCityID' => $cityID])->groupBy('locationName')->all();
                
        }
                if($restaurantLocations)
                {
                    $locations = array();

                    foreach($restaurantLocations as $newRestaurantLocations)
                    {

                        $locationName = (isset($newRestaurantLocations->locationName)) ? $newRestaurantLocations->locationName : NULL;
                        $locationID = (isset($newRestaurantLocations->pkRestaurantLoactionID)) ? $newRestaurantLocations->pkRestaurantLoactionID : NULL;

                        $locations[] = array(
                            'locationID'   => $locationID,
                            'locationName' => $locationName
                            );
                    }

                        $data['code'] = 200;
                        $data['message'] = 'Location Names';
                        $data['locations'] = $locations;

                }
                else
                {
                        $data['code'] = 500;
                        $data['message'] = 'No location found';
                }
        

     }
     else
     {
                $data['code'] = 500;
                $data['message'] = 'One or more parameters are missing';
     }      

        return $data;
        
    }

    /**
     * Search restaurant
     * @return json restaurant info
     */
    public static function searchRestaurant($request)
    {
        $data = array();
        // Required
        $restaurantName = (isset($request->name)) ? $request->name : NULL;
        $userId = (isset($request->userId)) ? $request->userId : NULL;
        $cityID = (isset($request->cityID)) ? $request->cityID : NULL;
        $locationID = (isset($request->locationID)) ? $request->locationID : NULL;
        $cuisineID = (isset($request->cuisineID)) ? $request->cuisineID : NULL;
        $latitude = (isset($request->latitude)) ? $request->latitude : NULL;
        $longitudeID = (isset($request->longitude)) ? $request->longitude : NULL;
        $pageNumber = (isset($request->page)) ? $request->page : NULL;
        $limit = (isset($request->limit)) ? $request->limit : 20;
        //$limit = '10';
        if($pageNumber)
        {

            if($pageNumber == '1')
            {
                $offset = $limit*($pageNumber - 1);
                
            }
            elseif ($pageNumber > 1) 
            {
                $offset = $limit*($pageNumber - 1);
            
            }
            else
            {
                $limit = '-1';
                $offset = '0';
                $data['code'] = 500;
                $data['message'] = 'Page number has not correct value';
            }

        }else{

            $limit = '-1';
            $offset = '0';
        }

        if((@$restaurantName) && (!@$cityID) && (!@$locationID) && (!@$cuisineID))
        {
            $restaurantResults = Restaurant::find()->where(['like', 'restaurantName', $restaurantName.'%', false])->offset($offset)->limit($limit)->all(); 
           // print_r($restaurantResults);
            if($restaurantResults)
            {
                $restaurantList = array();
                foreach($restaurantResults as $restaurantResult)
                {
                    $restaurantID = (isset($restaurantResult->pkRestaurantID)) ? $restaurantResult->pkRestaurantID : NULL;
                    $image = (isset($restaurantResult->restaurantLogo)) ? $restaurantResult->restaurantLogo : NULL;
                    $status = (isset($restaurantResult->restaurantStatus)) ? $restaurantResult->restaurantStatus : NULL;
                    $name = (isset($restaurantResult->restaurantName)) ? $restaurantResult->restaurantName : NULL;
                    $timing = (isset($restaurantResult->restaurantTimings)) ? $restaurantResult->restaurantTimings : NULL;
                    $cityID = (isset($restaurantResult->restaurantCityID)) ? $restaurantResult->restaurantCityID : NULL;
                    $locationID = (isset($restaurantResult->restaurantLocationID)) ? $restaurantResult->restaurantLocationID : NULL;
                    $latitude = (isset($restaurantResult->restaurantLatitude)) ? $restaurantResult->restaurantLatitude : NULL;
                    $longitude = (isset($restaurantResult->restaurantLongitude)) ? $restaurantResult->restaurantLongitude : NULL;
                    $QRCode = (isset($restaurantResult->restaurantQRCode)) ? $restaurantResult->restaurantQRCode : NULL;

                     $cityList = RestaurantCity::findOne(['pkRestaurantCityID' => $cityID]);
                     $cityName = (isset($cityList->cityName)) ? $cityList->cityName : NULL;


                     $locationList = RestaurantLocation::findOne(['pkRestaurantLoactionID' => $locationID]);
                     $locationName = (isset($locationList->locationName)) ? $locationList->locationName : NULL;


                     $userVisitedNumbers = UserRestaurantBooking::findAll(['fkRestaurantID' => $restaurantID, 'fkUserID' => $userId, 'userCancelBookong' => '0']);
                     $countVisited = count($userVisitedNumbers);
                     $userVisited = (isset($countVisited)) ? $countVisited : 0;

                     $restaurantCuisineIDs = Restaurant_cuisine::findAll(['fkRestaurantID' => $restaurantID]);
                     //  print_r($restaurantCuisineIDs);
                      if($restaurantCuisineIDs)
                      { 
                        $restaurantCuisineList = array();
                        foreach($restaurantCuisineIDs as $restaurantCuisineID)
                        {
                            $cuisuineID = $restaurantCuisineID->pkRestaurantCuisineNameID;
                            $restaurantCuisineNamesResult = RestaurantCuisineNames::findOne(['pkRestaurantCuisineNameID' => $cuisuineID]);
                            $cuisineName = (isset($restaurantCuisineNamesResult->cuisineName)) ? $restaurantCuisineNamesResult->cuisineName : NULL;
                            $restaurantCuisineList[] = $cuisineName;
                        }

                      }
                      else
                      {
                        $restaurantCuisineList = array();
                      }
                      
                        $restaurantList[] = array(
                        "restaurantID" => $restaurantID,
                        "image" => $image,
                        "status" => $status,
                        "name" => $name,
                        "timimng" => $timing,
                        "city" => $cityName,
                        "locationName" => $locationName,
                        "cuisineList" => $restaurantCuisineList,
                        "latitude" => $latitude,
                        "longitude" => $longitude,
                        "QRCode" => $QRCode,
                        "noOfTimesVisited" => $userVisited
                        );
                }
                    $data['code'] = 200;
                    $data['message'] = 'Restaurant list';
                    $data['pageNumber'] = $pageNumber;
                    $data['restaurantList'] = $restaurantList;
                
            }
            else
            {

                $data['code'] = 500;
                $data['message'] = 'No result found';  

            }

        }
        elseif($restaurantName || $cityID || $locationID || $cuisineID)
        {
            
            if(($cityID == 'all') || ($cityID == 'All'))
            {
                $restaurantCities = RestaurantCity::find()->select(['pkRestaurantCityID'])->all();
                if($restaurantCities)
                {
                    foreach ($restaurantCities as $restaurantCiti) {
                        $cityArray[] = $restaurantCiti->pkRestaurantCityID;
                    }
                }
            }
            else
            {
                if(@$cityID)
                {
                    $cityArray[] = $cityID;    
                }
                else
                {
                    $cityArray = NULL;
                }
                
            }
            
            if(($locationID == 'all') || ($locationID == 'All'))
            {
                $restaurantLocations = RestaurantLocation::find()->select(['pkRestaurantLoactionID'])->all();
                if($restaurantLocations)
                {
                    foreach($restaurantLocations as $restaurantLocation)
                    {
                        $locationArray[] = $restaurantLocation->pkRestaurantLoactionID;
                    }
                }
            }
            else
            {   
                if(@$locationID)
                {
                    $locationArray[] = $locationID;    
                }
                else
                {
                    $locationArray = NULL;
                }
            }
            //print_r($locationArray); die;

            if(($cuisineID == 'all') || ($cuisineID == 'All'))
            {

                $restaurantResults = Restaurant::find()->andFilterWhere(['and',
                ['like', 'restaurantName', $restaurantName.'%', false],
                ['restaurantCityID' => $cityArray],
                ['restaurantLocationID' => $locationArray],
                ])->offset($offset)->limit($limit)
                ->all();

            }
            else
            {   
                if($cuisineID){
                    $restaurantIDs = Restaurant_cuisine::findAll(['pkRestaurantCuisineNameID' => $cuisineID]);
                }
                                
                 if(@$restaurantIDs)
                 {
                    foreach($restaurantIDs as $newRestaurantIDs)
                    {
                        $restaurantIDList[] = $newRestaurantIDs->fkRestaurantID;
                    }
                 }
                $restaurantIDValue = (isset($restaurantIDList)) ? $restaurantIDList : NULL;

                $restaurantResults = Restaurant::find()->andFilterWhere(['and',
                ['like', 'restaurantName', $restaurantName.'%', false],
                ['restaurantCityID' => $cityArray],
                ['restaurantLocationID' => $locationArray],
                ['pkRestaurantID' => $restaurantIDValue]
                ])->offset($offset)->limit($limit)
                ->all();
            }             
            
             //print_r($restaurantResults); die;

            if($restaurantResults)
            {
                $restaurantList = array();
                foreach($restaurantResults as $restaurantResult)
                {
                $restaurantID = (isset($restaurantResult->pkRestaurantID)) ? $restaurantResult->pkRestaurantID : NULL;
                $image = (isset($restaurantResult->restaurantLogo)) ? $restaurantResult->restaurantLogo : NULL;
                $status = (isset($restaurantResult->restaurantStatus)) ? $restaurantResult->restaurantStatus : NULL;
                $name = (isset($restaurantResult->restaurantName)) ? $restaurantResult->restaurantName : NULL;
                $timing = (isset($restaurantResult->restaurantTimings)) ? $restaurantResult->restaurantTimings : NULL;
                $cityID = (isset($restaurantResult->restaurantCityID)) ? $restaurantResult->restaurantCityID : NULL;
                $locationID = (isset($restaurantResult->restaurantLocationID)) ? $restaurantResult->restaurantLocationID : NULL;
                $latitude = (isset($restaurantResult->restaurantLatitude)) ? $restaurantResult->restaurantLatitude : NULL;
                $longitude = (isset($restaurantResult->restaurantLongitude)) ? $restaurantResult->restaurantLongitude : NULL;
                $QRCode = (isset($restaurantResult->restaurantQRCode)) ? $restaurantResult->restaurantQRCode : NULL;

             $cityList = RestaurantCity::findOne(['pkRestaurantCityID' => $cityID]);
             $cityName = (isset($cityList->cityName)) ? $cityList->cityName : NULL;


             $locationList = RestaurantLocation::findOne(['pkRestaurantLoactionID' => $locationID]);
             $locationName = (isset($locationList->locationName)) ? $locationList->locationName : NULL;


             $userVisitedNumbers = UserRestaurantBooking::findAll(['fkRestaurantID' => $restaurantID, 'fkUserID' => $userId, 'userCancelBookong' => '0']);
             $countVisited = count($userVisitedNumbers);
             $userVisited = (isset($countVisited)) ? $countVisited : 0;

             $restaurantCuisineIDs = Restaurant_cuisine::findAll(['fkRestaurantID' => $restaurantID]);
              //  print_r($restaurantCuisineIDs);
                if($restaurantCuisineIDs)
                  { 
                    $restaurantCuisineList = array();
                    foreach($restaurantCuisineIDs as $restaurantCuisineID)
                    {
                        $cuisuineID = $restaurantCuisineID->pkRestaurantCuisineNameID;
                        $restaurantCuisineNamesResult = RestaurantCuisineNames::findOne(['pkRestaurantCuisineNameID' => $cuisuineID]);
                        $cuisineName = (isset($restaurantCuisineNamesResult->cuisineName)) ? $restaurantCuisineNamesResult->cuisineName : NULL;
                        $restaurantCuisineList[] = $cuisineName;
                    }

                  }else{
                    $restaurantCuisineList = array();
                  }

                    $restaurantList[] = array(
                    "restaurantID" => $restaurantID,
                    "image" => $image,
                    "status" => $status,
                    "name" => $name,
                    "timimng" => $timing,
                    "city" => $cityName,
                    "locationName" => $locationName,
                    "cuisineList" => $restaurantCuisineList,
                    "latitude" => $latitude,
                    "longitude" => $longitude,
                    "QRCode" => $QRCode,
                    "noOfTimesVisited" => $userVisited
                    );
                }
                $data['code'] = 200;
                $data['message'] = 'Restaurant list';
                $data['pageNumber'] = $pageNumber;
                $data['restaurantList'] = $restaurantList;
                
            }
            else
            {

                $data['code'] = 500;
                $data['message'] = 'No result found';  

            }
         
        }
        else
        {

            $data['code'] = 500;
            $data['message'] = 'One or more parameters are missing';

        }
        return $data;   
    }

    /**
     * 
     * @return json restaurant info
     */
    public static function restaurantDetail($request)
    {
        $data = array();
        // Required
        $restaurantID = (isset($request->restaurantID)) ? $request->restaurantID : NULL;
        $userId = (isset($request->userId)) ? $request->userId : NULL;
        
        //$favourite = (isset($request->favourite)) ? $request->favourite : NULL;

        if((@$restaurantID) && ($userId))
        {
            $restaurantResult = Restaurant::findOne(['pkRestaurantID', $restaurantID]);

            if($restaurantResult)
            {
                $restaurantList = array();
                
                $restaurantID = (isset($restaurantResult->pkRestaurantID)) ? $restaurantResult->pkRestaurantID : NULL;
                $image = (isset($restaurantResult->restaurantLogo)) ? $restaurantResult->restaurantLogo : NULL;
                $link = (isset($restaurantResult->restaurantLink)) ? $restaurantResult->restaurantLink : NULL;
                $description = (isset($restaurantResult->restaurantDescription)) ? $restaurantResult->restaurantDescription : NULL;
                $status = (isset($restaurantResult->restaurantStatus)) ? $restaurantResult->restaurantStatus : NULL;
                $name = (isset($restaurantResult->restaurantName)) ? $restaurantResult->restaurantName : NULL;
                $timing = (isset($restaurantResult->restaurantTimings)) ? $restaurantResult->restaurantTimings : NULL;
                $cityID = (isset($restaurantResult->restaurantCityID)) ? $restaurantResult->restaurantCityID : NULL;
                $locationID = (isset($restaurantResult->restaurantLocationID)) ? $restaurantResult->restaurantLocationID : NULL;
                $latitude = (isset($restaurantResult->restaurantLatitude)) ? $restaurantResult->restaurantLatitude : NULL;
                $longitude = (isset($restaurantResult->restaurantLongitude)) ? $restaurantResult->restaurantLongitude : NULL;
                $QRCode = (isset($restaurantResult->restaurantQRCode)) ? $restaurantResult->restaurantQRCode : NULL;

             $cityList = RestaurantCity::findOne(['pkRestaurantCityID' => $cityID]);
             $cityName = (isset($cityList->cityName)) ? $cityList->cityName : NULL;


             $locationList = RestaurantLocation::findOne(['pkRestaurantLoactionID' => $locationID]);
             $locationName = (isset($locationList->locationName)) ? $locationList->locationName : NULL;

             if($userId){
             /*find if user has visited to restaurant or not*/
             $userVisitedNumbers = UserRestaurantBooking::findAll(['fkRestaurantID' => $restaurantID, 'fkUserID' => $userId, 'userCancelBookong' => '0']);
             $countVisited = count($userVisitedNumbers); 

             /*find if user has marked favourite to restaurant or not*/
             $userFavourite = UserRestaurantFavourite::findOne(['fkRestaurantID' => $restaurantID, 'fkUserID' => $userId]);
             }
             $userFavouriteValue = (isset($userFavourite->favourite)) ? $userFavourite->favourite : 0;
             $userVisited = (isset($countVisited)) ? $countVisited : 0;


             $cuisineIDsList = Restaurant_cuisine::findAll(['fkRestaurantID' => $restaurantID]);
            
             if($cuisineIDsList)
             {
                foreach($cuisineIDsList as $newcuisineIDsList)
                {
                    $cuisineIDs = RestaurantCuisineNames::findOne(['pkRestaurantCuisineNameID' => $newcuisineIDsList->pkRestaurantCuisineNameID]);      
                    $listCuisineIDs[] = $cuisineIDs->cuisineName;
                }

             }             
             $listCuisineIDsValue = (isset($listCuisineIDs)) ? $listCuisineIDs : NULL;

             $menuDetails = Restaurant_menu::findAll(['fkRestaurantID' => $restaurantID]);
             if($menuDetails)
             {  
                 $menuImages = array();
                 foreach ($menuDetails as $newMenuDetails) {
                 $menuImages[] = array(
                    "name" => $newMenuDetails->menuName,
                    "image" => $newMenuDetails->restaurantPicture,
                    );
                 
                }
             }
             $menuImagesResult = (isset($menuImages)) ? $menuImages : NULL;

                    $restaurantList = array(
                    "restaurantID" => $restaurantID,
                    "image" => $image,
                    "status" => $status,
                    "name" => $name,
                    "timimng" => $timing,
                    "city" => $cityName,
                    "locationName" => $locationName,
                    "cuisineNames" => $listCuisineIDsValue,
                    "description" => $description,
                    "link" => $link,
                    "latitude" => $latitude,
                    "longitude" => $longitude,
                    "QRCode" => $QRCode,
                    "likeStatus" => $userFavouriteValue,
                    "noOfTimesVisited" => $userVisited,
                    "menuImages" => $menuImagesResult
                    );

                $data['code'] = 200;
                $data['message'] = 'Restaurant details';
                $data['restaurantList'] = $restaurantList;
            }
            else
            {

                $data['code'] = 500;
                $data['message'] = 'No result found';  

            }

        }
        else
        {

            $data['code'] = 500;
            $data['message'] = 'One or more parameters are missing';

        }
        return $data;   
    }

    /**
     * restaurant booking
     * @return json restaurant booking info
     */
    public static function restaurantBooking($request)
    {
        $data = array();
        // Required
        $QRCode = (isset($request->QRCode)) ? $request->QRCode : NULL;
        $restaurantID = (isset($request->restaurantID)) ? $request->restaurantID : NULL;
        $userId = (isset($request->userId)) ? $request->userId : NULL;
        $numberSeats = (isset($request->numberSeats)) ? $request->numberSeats : NULL;

            if(($restaurantID) && ($userId) && ($numberSeats))
            {

            $restaurantResult = Restaurant::find()->where(['pkRestaurantID' => $restaurantID])->select('restaurantWaitingTime')->one();
            $waitingTime = (isset($restaurantResult->restaurantWaitingTime)) ? $restaurantResult->restaurantWaitingTime : NULL;
            
                $user2 = new UserRestaurantBooking();
                // User Properties
                $user2->fkRestaurantID = $restaurantID;
                $user2->fkUserID = $userId;
                $user2->userRestaurantQRCode = $QRCode;
                $user2->userNumberSeats = $numberSeats;
                $user2->waitingTime = $waitingTime;
                $user2->date = date("Y-m-d");
                $user2->userCancelBookong = '0';
                if($user2->save(false))
                {
                    
                   $bookingID = $user2->pkUserRestaurantBookingID;
                   if($bookingID)
                   {

                    $data['code'] = 200;
                    $data['message'] = 'Booking Successful';
                    $data['bookingID'] = $bookingID;
                    $data['waitingTime'] = $waitingTime;

                   }
                   else
                   {

                    $data['code'] = 500;
                    $data['message'] = 'Error while booking';

                   }

                }
                else
                {
                    $data['code'] = 500;
                    $modalErrors = $user->getErrors();
                    foreach($modalErrors as $modalError)
                    {
                        $data['message'] = $modalError[0];
                    }
                }
            }
            else
            {

            $data['code'] = 500;
            $data['message'] = 'One or more parameters are missing';

            }

            return $data;
    }

    /**
     * cancel restaurant's booking
     * @return json cancel booking info
     */
    public static function cancelBooking($request)
    {
        $data = array();
        // Required
        $restaurantID = (isset($request->restaurantID)) ? $request->restaurantID : NULL;
        $bookingID = (isset($request->bookingID)) ? $request->bookingID : NULL;
        $userId = (isset($request->userId)) ? $request->userId : NULL;

            if(($restaurantID) && ($userId) && ($bookingID))
            {

            $restaurantResult = UserRestaurantBooking::findone(['pkUserRestaurantBookingID' => $bookingID,'fkRestaurantID' => $restaurantID , 'fkUserID' => $userId]);
                    
                if($restaurantResult)
                {

                    $restaurantResult->userCancelBookong = '1';
                    if($restaurantResult->save(false))
                    {
                       
                        $data['code'] = 200;
                        $data['message'] = 'Cancel Successful';
                        $data['bookingID'] = $bookingID;
                    }
                    else
                    {
                        $data['code'] = 500;
                        $modalErrors = $user->getErrors();
                        foreach($modalErrors as $modalError)
                        {
                            $data['message'] = $modalError[0];
                        }
                    }
                }
                else
                {
                    $data['code'] = 500;
                    $data['message'] = 'No booking found';

                }
                             
            }
            else
            {

            $data['code'] = 500;
            $data['message'] = 'One or more parameters are missing';

            }

            return $data;
    }

    /**
     * restaurant offers
     * @return json offers result
     */
    public static function offers($request)
    {
        $data = array();
        // Required
         $restaurantID = (isset($request->restaurantID)) ? $request->restaurantID : NULL;
        
            if($restaurantID)
            {

            $restaurantOffers = RestaurantDiscounts::find()->where(['fkRestaurantID' => $restaurantID])->all();
                    
                if($restaurantOffers)
                {
                    foreach($restaurantOffers as $restaurantOffer) 
                    {   
                        $discountID = $restaurantOffer->pkRestaurantDiscountsID;
                        $image = $restaurantOffer->discountImage;
                        $discountData[] = array(
                            'discountID' => $discountID, 
                            'imageUrls' => $image
                            );
                    }

                        $data['code'] = 200;
                        $data['message'] = 'Offers Images';
                        $data['data'] = $discountData;

                }
                else
                {
                    $data['code'] = 500;
                    $data['message'] = 'No offers found';

                }
                             
            }
            else
            {

            $data['code'] = 500;
            $data['message'] = 'One or more parameters are missing';

            }

            return $data;
    }

    /**
     * update profile
     * @return json profile result
     */
    public static function updateProfile($request)
    {
        $data = array();
        // Required
         $profileImage = (isset($request->profileImage)) ? $request->profileImage : NULL;
         $name = (isset($request->name)) ? $request->name : NULL;
         $email = (isset($request->email)) ? $request->email : NULL;
         $mobile = (isset($request->mobile)) ? $request->mobile : NULL;
         $dob = (isset($request->dob)) ? $request->dob : NULL;
         $gender = (isset($request->gender)) ? $request->gender : NULL;
         $nationality = (isset($request->nationality)) ? $request->nationality : NULL;
         $userId = (isset($request->userId)) ? $request->userId : NULL;

            if($userId)
            {

            $user = Users::findOne(['pkUserID' => $userId]);
                    
                if($user)
                {   

                    if($profileImage)
                    {
                      // Upload or Fail
                        $imageName = 'image-'.$user->pkUserID.'-'.Yii::$app->security->generateRandomString();

                        $boolUploadImage = self::uploadImageAndroid($profileImage, $imageName, 'profilePath', false);

                        if($boolUploadImage && is_array($boolUploadImage))
                        {
                            $user->userPicture = $boolUploadImage['url'];
                            $data['profileImage'] =  $boolUploadImage['url'];
                        }
                        else
                        {
                           $data['profileImage'] =  "NULL"; 
                        }

                    }else
                    {

                        $profileImage = $user->userPicture;
                        //if(file_exists($profileImage)){
                        $data['profileImage'] = $profileImage;
                       //    }

                    }
                    $user->userName = $name;
                    $user->userEmail = $email;
                    $user->userDOB = $dob;
                    $user->userSex = $gender;
                    $user->userNationality = $nationality;
                    
                    if($user->save())
                    {
                       
                        $data['code'] = 200;
                        $data['message'] = 'Update Successful';
                        $data['name'] = $name;
                        $data['email'] = $email;
                        $data['phone'] = $user->userNumber;
                        $data['dob'] = $user->userDOB;
                        $data['nationality'] = $nationality;
                        $data['gender'] = $gender;

                    }
                    else
                    {
                        $data['code'] = 500;
                        $modalErrors = $user->getErrors();
                        foreach($modalErrors as $modalError)
                        {
                            $data['message'] = $modalError[0];
                        }
                    }

                }
                else
                {
                    $data['code'] = 500;
                    $data['message'] = 'No user found';

                }
                             
            }
            else
            {

            $data['code'] = 500;
            $data['message'] = 'One or more parameters are missing';

            }

            return $data;
    }

    /**
     * Upload image method for Android
     * @param string $varImage (required) Base64 Encoded Image
     * @param string $varImageName (required) imageName
     * @param string $varPath (required) Path to save image
     * @return mixed array on success, false on fail
     */
    private static function uploadImageAndroid($varImage, $varImageName, $varPath, $ext = '.png')
    {
        if($varImage)
        {
            $ext = '.png';
            $image_data = str_replace(' ',  '+',  $varImage);
            $image_data = base64_decode($image_data);
            
            $name =  \yii\helpers\Inflector::slug($varImageName).$ext;
            //$file = Url::to('/uploads').Yii::$app->params[$varPath].$name;
            $file = Url::to('@frontend').'/web'.Yii::$app->params[$varPath].$name;
            
            if(file_put_contents($file, $image_data))
            {                
                $data['name'] = $name;
                $data['url'] = Url::base(true).Yii::$app->params[$varPath].$name;
                return $data;
            }

            return false;
        }
    }

     /**
     * buzz in restaurant's booking
     * @return json buzzin booking info
     */
    public static function buzzIn($request)
    {
        $data = array();
        // Required
        
        $bookingID = (isset($request->bookingID)) ? $request->bookingID : NULL;
        $userId = (isset($request->userId)) ? $request->userId : NULL;
        $buzzInTime = (isset($request->buzzInTime)) ? $request->buzzInTime : NULL;
            if(($buzzInTime) && ($userId) && ($bookingID))
            {

            $restaurantResult = UserRestaurantBooking::findone(['pkUserRestaurantBookingID' => $bookingID, 'fkUserID' => $userId]);
                    
                if($restaurantResult)
                {

                    $restaurantResult->buzzInTime = $buzzInTime;
                    if($restaurantResult->save(false))
                    {
                       
                        $data['code'] = 200;
                        $data['message'] = 'Update Successful';
                        $data['bookingID'] = $bookingID;
                    }
                    else
                    {
                        $data['code'] = 500;
                        $modalErrors = $user->getErrors();
                        foreach($modalErrors as $modalError)
                        {
                            $data['message'] = $modalError[0];
                        }
                    }

                }
                else
                {
                    $data['code'] = 500;
                    $data['message'] = 'No booking found';

                }
                             
            }
            else
            {

            $data['code'] = 500;
            $data['message'] = 'One or more parameters are missing';

            }

            return $data;
    }

     /**
     * buzzout restaurant's booking
     * @return json buzzout booking info
     */
    public static function buzzOut($request)
    {
        $data = array();
        // Required
        $bookingID = (isset($request->bookingID)) ? $request->bookingID : NULL;
        $userId = (isset($request->userId)) ? $request->userId : NULL;

            if(($userId) && ($bookingID))
            {

            $restaurantResult = UserRestaurantBooking::findone(['pkUserRestaurantBookingID' => $bookingID, 'fkUserID' => $userId]);
                    
                if($restaurantResult)
                {

                    $restaurantResult->userCancelBookong = '1';
                    if($restaurantResult->save(false))
                    {
                       
                        $data['code'] = 200;
                        $data['message'] = 'Cancel Successful';
                        $data['bookingID'] = $bookingID;
                    }
                    else
                    {
                        $data['code'] = 500;
                        $modalErrors = $user->getErrors();
                        foreach($modalErrors as $modalError)
                        {
                            $data['message'] = $modalError[0];
                        }
                    }

                }
                else
                {
                    $data['code'] = 500;
                    $data['message'] = 'No booking found';

                }
                             
            }
            else
            {

            $data['code'] = 500;
            $data['message'] = 'One or more parameters are missing';

            }

            return $data;
    }

    /**
     * transaction last 10 bookings
     * @return json last 10 restaurant booking result
     */
    public static function transaction($request)
    {
        $data = array();
        // Required
        $userId = (isset($request->userId)) ? $request->userId : NULL;
            if($userId)
            {
                $restaurantBookingResult = UserRestaurantBooking::find()->where(['fkUserID' => $userId])->orderBy(['pkUserRestaurantBookingID' => SORT_DESC])->offset(0)->limit(10)->all();
                  
                if($restaurantBookingResult)
                {   
                     $restaurantList = array();
                    foreach($restaurantBookingResult as $restaurantBookingResults)
                    {
                          //echo "<pre>"; print_r($restaurantBookingResults); echo "</pre>";

                        $pkUserRestaurantBookingID = $restaurantBookingResults->pkUserRestaurantBookingID;
                        $fkRestaurantID = $restaurantBookingResults->fkRestaurantID;
                        $userNumberSeats = $restaurantBookingResults->userNumberSeats;
                        $bookingDate = $restaurantBookingResults->date;
                        $waitingTime = $restaurantBookingResults->waitingTime;
                        $buzzInTime = $restaurantBookingResults->buzzInTime;
                        $userCancelBookong = $restaurantBookingResults->userCancelBookong;

                        $restaurantResult = Restaurant::findOne(['pkRestaurantID', $fkRestaurantID]);
                        if($restaurantResult)
                        {
                           
                            $restaurantID = (isset($restaurantResult->pkRestaurantID)) ? $restaurantResult->pkRestaurantID : NULL;
                            $image = (isset($restaurantResult->restaurantLogo)) ? $restaurantResult->restaurantLogo : NULL;
                            $name = (isset($restaurantResult->restaurantName)) ? $restaurantResult->restaurantName : NULL;
                            $cityID = (isset($restaurantResult->restaurantCityID)) ? $restaurantResult->restaurantCityID : NULL;
                            $locationID = (isset($restaurantResult->restaurantLocationID)) ? $restaurantResult->restaurantLocationID : NULL;

                            $cityList = RestaurantCity::findOne(['pkRestaurantCityID' => $cityID]);
                            $cityName = (isset($cityList->cityName)) ? $cityList->cityName : NULL;

                            $locationList = RestaurantLocation::findOne(['pkRestaurantLoactionID' => $locationID]);
                            $locationName = (isset($locationList->locationName)) ? $locationList->locationName : NULL;
                            
                             if($userId)
                             {
                                 /*find if user has visited to restaurant or not*/
                                 $userVisitedNumbers = UserRestaurantBooking::findAll(['fkRestaurantID' => $restaurantID, 'fkUserID' => $userId, 'userCancelBookong' => '0']);
                                 $countVisited = count($userVisitedNumbers); 

                                 $userPointsEarned = UserPointsEarned::findOne(['fkRestaurantID' => $restaurantID, 'fkUserID' => $userId, 'fkUserRestaurantBookingID' => $pkUserRestaurantBookingID]);
                                 $userPoints = (isset($userPointsEarned->userPointsEarned)) ? $userPointsEarned->userPointsEarned : NULL;
                                 
                                 /*find if user has marked favourite to restaurant or not*/
                                 $userFavourite = UserRestaurantFavourite::findOne(['fkRestaurantID' => $restaurantID, 'fkUserID' => $userId]);
                             }
                                $userFavouriteValue = (isset($userFavourite->favourite)) ? $userFavourite->favourite : 0;
                                $userVisited = (isset($countVisited)) ? $countVisited : 0;

                                $restaurantList[] = array(
                                "bookingID" => $pkUserRestaurantBookingID,
                                "restaurantID" => $restaurantID,
                                "image" => $image,
                                "name" => $name,
                                "cityName" => $cityName,
                                "locationName" => $locationName,
                                "likeStatus" => $userFavouriteValue,
                                "noOfTimesVisited" => $userVisited,
                                "bookingDate" => $bookingDate,
                                "userNumberSeats" => $userNumberSeats,
                                "waitingTime" => $waitingTime,
                                "buzzInTime" => $buzzInTime,
                                "userPointsEarned" => $userPoints,
                                "userCancelBookong" => $userCancelBookong,
                                );
                        }
                    }
                    
                        $data['code'] = 200;
                        $data['message'] = 'Reward Summary details';
                        $data['rewardSummary'] = $restaurantList;

                    

                }
                else
                {
                    $data['code'] = 500;
                    $data['message'] = 'No booking found';

                }
                             
            }
            else
            {

            $data['code'] = 500;
            $data['message'] = 'One or more parameters are missing';

            }

            return $data;
    }

    /**
     * reward Summary Date wise
     * @return json date wise restaurant booking result
     */
    public static function rewardSummaryDate($request)
    {
        $data = array();
        // Required
        $userId = (isset($request->userId)) ? $request->userId : NULL;
        $dateFrom = (isset($request->dateFrom)) ? $request->dateFrom : NULL;
        $dateTo = (isset($request->dateTo)) ? $request->dateTo : NULL;
        $startLimit = (isset($request->startLimit)) ? $request->startLimit : '0';
        $endLimit = (isset($request->endLimit)) ? $request->endLimit : '10';

         $finalLimit = $endLimit - $startLimit;

            if(($userId) && ($dateFrom) && ($dateTo))
            {

             $restaurantBookingResult = UserRestaurantBooking::find()
             ->where(['and', ['and',['>=', 'date', $dateFrom], ['<=', 'date', $dateTo]],['fkUserID' => $userId]])
             ->offset($startLimit)
             ->limit($finalLimit)
             ->orderBy(['pkUserRestaurantBookingID' => SORT_DESC])
             ->all();
                
                //print_r($restaurantBookingResult); die;  
                
                if($restaurantBookingResult)
                {   
                     $restaurantList = array();
                    foreach($restaurantBookingResult as $restaurantBookingResults)
                    {
                          //echo "<pre>"; print_r($restaurantBookingResults); echo "</pre>";

                        $pkUserRestaurantBookingID = $restaurantBookingResults->pkUserRestaurantBookingID;
                        $fkRestaurantID = $restaurantBookingResults->fkRestaurantID;
                        $userNumberSeats = $restaurantBookingResults->userNumberSeats;
                        $bookingDate = date("d-m-Y", strtotime($restaurantBookingResults->date));
                        $waitingTime = $restaurantBookingResults->waitingTime;
                        $buzzInTime = $restaurantBookingResults->buzzInTime;
                        $userCancelBookong = $restaurantBookingResults->userCancelBookong;


                        $restaurantResult = Restaurant::findOne(['pkRestaurantID', $fkRestaurantID]);
                        if($restaurantResult)
                        {
                           
                            $restaurantID = (isset($restaurantResult->pkRestaurantID)) ? $restaurantResult->pkRestaurantID : NULL;
                            $image = (isset($restaurantResult->restaurantLogo)) ? $restaurantResult->restaurantLogo : NULL;
                            $name = (isset($restaurantResult->restaurantName)) ? $restaurantResult->restaurantName : NULL;
                            

                            $cityID = (isset($restaurantResult->restaurantCityID)) ? $restaurantResult->restaurantCityID : NULL;
                            $locationID = (isset($restaurantResult->restaurantLocationID)) ? $restaurantResult->restaurantLocationID : NULL;

                            $cityList = RestaurantCity::findOne(['pkRestaurantCityID' => $cityID]);
                            $cityName = (isset($cityList->cityName)) ? $cityList->cityName : NULL;

                            $locationList = RestaurantLocation::findOne(['pkRestaurantLoactionID' => $locationID]);
                            $locationName = (isset($locationList->locationName)) ? $locationList->locationName : NULL;

                             if($userId){
                             /*find if user has visited to restaurant or not*/
                             $userVisitedNumbers = UserRestaurantBooking::findAll(['fkRestaurantID' => $restaurantID, 'fkUserID' => $userId, 'userCancelBookong' => '0']);
                             $countVisited = count($userVisitedNumbers); 

                             $userPointsEarned = UserPointsEarned::findOne(['fkRestaurantID' => $restaurantID, 'fkUserID' => $userId, 'fkUserRestaurantBookingID' => $pkUserRestaurantBookingID]);
                             $userPoints = (isset($userPointsEarned->userPointsEarned)) ? $userPointsEarned->userPointsEarned : NULL;

                             /*find if user has marked favourite to restaurant or not*/
                             $userFavourite = UserRestaurantFavourite::findOne(['fkRestaurantID' => $restaurantID, 'fkUserID' => $userId]);
                             }
                                $userFavouriteValue = (isset($userFavourite->favourite)) ? $userFavourite->favourite : 0;
                                $userVisited = (isset($countVisited)) ? $countVisited : 0;

                                $restaurantList[] = array(
                                "bookingID" => $pkUserRestaurantBookingID,
                                "restaurantID" => $restaurantID,
                                "image" => $image,
                                "name" => $name,
                                "cityName" => $cityName,
                                "locationName" => $locationName,
                                "likeStatus" => $userFavouriteValue,
                                "noOfTimesVisited" => $userVisited,
                                "bookingDate" => $bookingDate,
                                "userNumberSeats" => $userNumberSeats,
                                "waitingTime" => $waitingTime,
                                "buzzInTime" => $buzzInTime,
                                "userPointsEarned" => $userPoints,
                                "userCancelBookong" => $userCancelBookong,
                                );
                        }
                    }
                    
                    $data['code'] = 200;
                    $data['message'] = 'Reward Summary details';
                    $data['rewardSummary'] = $restaurantList;

                    

                }
                else
                {
                    $data['code'] = 500;
                    $data['message'] = 'No booking found between these dates';

                }
                             
            }
            else
            {

            $data['code'] = 500;
            $data['message'] = 'One or more parameters are missing';

            }

            return $data;
    }

    /**
     * reward Summary Date wise
     * @return json date wise restaurant booking result
     */
    public static function rewardSummaryRestaurant($request)
    {
        $data = array();
        // Required
        $restaurantID = (isset($request->restaurantID)) ? $request->restaurantID : NULL;
        $userId = (isset($request->userId)) ? $request->userId : NULL;
        
            if(($restaurantID) && ($userId)) 
            {

             
                $restaurantResult = Restaurant::findOne(['pkRestaurantID', $restaurantID]);
                        if($restaurantResult)
                        {
                           
                            $restaurantID = (isset($restaurantResult->pkRestaurantID)) ? $restaurantResult->pkRestaurantID : NULL;
                            $image = (isset($restaurantResult->restaurantLogo)) ? $restaurantResult->restaurantLogo : NULL;
                            $name = (isset($restaurantResult->restaurantName)) ? $restaurantResult->restaurantName : NULL;
                            
                             if($userId)
                             {
                                 /*find if user has visited to restaurant or not*/
                                 $userVisitedNumbers = UserRestaurantBooking::findAll(['fkRestaurantID' => $restaurantID, 'fkUserID' => $userId, 'userCancelBookong' => '0']);
                                 $countVisited = count($userVisitedNumbers); 

                                 /*find if user has marked favourite to restaurant or not*/
                                 $userFavourite = UserRestaurantFavourite::findOne(['fkRestaurantID' => $restaurantID, 'fkUserID' => $userId]);
                             }
                                 $userFavouriteValue = (isset($userFavourite->favourite)) ? $userFavourite->favourite : 0;
                                 $userVisited = (isset($countVisited)) ? $countVisited : 0;

                        }

                $restaurantBookingResult = UserRestaurantBooking::find()->where(['fkRestaurantID' => $restaurantID, 'fkUserID' => $userId])->orderBy(['pkUserRestaurantBookingID' => SORT_DESC])->all();
                
                if($restaurantBookingResult)
                {   

                     $bookingList = array();
                    foreach($restaurantBookingResult as $restaurantBookingResults)
                    {
                          //echo "<pre>"; print_r($restaurantBookingResults); echo "</pre>";

                        $pkUserRestaurantBookingID = $restaurantBookingResults->pkUserRestaurantBookingID;
                        $fkRestaurantID = $restaurantBookingResults->fkRestaurantID;
                        $userNumberSeats = $restaurantBookingResults->userNumberSeats;
                        $bookingDate = $restaurantBookingResults->date;
                        $waitingTime = $restaurantBookingResults->waitingTime;
                        $buzzInTime = $restaurantBookingResults->buzzInTime;
                        $userCancelBookong = $restaurantBookingResults->userCancelBookong;

                        $userPointsEarned = UserPointsEarned::findOne(['fkRestaurantID' => $restaurantID, 'fkUserID' => $userId, 'fkUserRestaurantBookingID' => $pkUserRestaurantBookingID]);
                        $userPoints = (isset($userPointsEarned->userPointsEarned)) ? $userPointsEarned->userPointsEarned : NULL;

                        $bookingList[] = 
                                array(
                                "bookingID" => $pkUserRestaurantBookingID,
                                "bookingDate" => $bookingDate,
                                "userNumberSeats" => $userNumberSeats,
                                "waitingTime" => $waitingTime,
                                "buzzInTime" => $buzzInTime,
                                "userPointsEarned" => $userPoints,
                                "userCancelBookong" => $userCancelBookong,
                                );
                        
                    }
                    
                    $data['code'] = 200;
                    $data['message'] = 'Restaurant Summary details';
                    $data['restaurantID'] = $restaurantID;
                    $data['image'] = $image;
                    $data['name'] = $name;
                    $data['likeStatus'] = $userFavouriteValue;
                    $data['noOfTimesVisited'] = $userVisited;
                    $data['bookingList'] = $bookingList;

                }
                else
                {
                    $data['code'] = 500;
                    $data['message'] = 'No booking found';

                }
                             
            }
            else
            {

            $data['code'] = 500;
            $data['message'] = 'One or more parameters are missing';

            }

            return $data;
    }

    /**
     * notifications
     * @return json notifications of restaurant
     */
    public static function notifications($request)
    {
        $data = array();
        // Required
        $tokenID = (isset($request->tokenID)) ? $request->tokenID : NULL;
        $userId = (isset($request->userId)) ? $request->userId : NULL;
        $startLimit = (isset($request->startLimit)) ? $request->startLimit : '0';
        $endLimit = (isset($request->endLimit)) ? $request->endLimit : '10';
        $finalLimit = $endLimit - $startLimit;
            if($userId)
            {
                $userRestaurantFavourite = UserRestaurantFavourite::find()->where(['fkUserID' => $userId, 'favourite' => '1'])->orderBy(['fkRestaurantID' => SORT_DESC])->all();
                
                if($userRestaurantFavourite)
                {
                foreach ($userRestaurantFavourite as $userRestaurantFavourites) 
                {
                    $restaurantID[] = $userRestaurantFavourites->fkRestaurantID;

                }

               $restaurantNotification = RestaurantNotifications::find()->where(['fkRestaurantID' => $restaurantID])->offset($startLimit)->limit($finalLimit)->orderBy(['pkRestaurantNotificationsID' => SORT_DESC])->all();

                    if($restaurantNotification){

                     $restaurantUserNotification = array();
                     foreach($restaurantNotification as $restaurantNotifications)
                     {

                         $pkRestaurantNotificationsID = $restaurantNotifications->pkRestaurantNotificationsID;
                         $fkRestaurantID = $restaurantNotifications->fkRestaurantID;
                         $notificationImage = $restaurantNotifications->notificationImage;
                         $discount = $restaurantNotifications->discount;
                         $pointsEarned = $restaurantNotifications->pointsEarned;
                         $description = $restaurantNotifications->description;

$restaurantResults = Restaurant::find()->select(['pkRestaurantID', 'restaurantCityID','restaurantLogo','restaurantLocationID'])->where(['pkRestaurantID'=>$fkRestaurantID])->one();
                            
                        $restaurantImage = (isset($restaurantResults->restaurantLogo)) ? $restaurantResults->restaurantLogo : NULL;
                        $cityID = (isset($restaurantResults->restaurantCityID)) ? $restaurantResults->restaurantCityID : NULL;
                        $locationID = (isset($restaurantResults->restaurantLocationID)) ? $restaurantResults->restaurantLocationID : NULL;

                         $cityList = RestaurantCity::findOne(['pkRestaurantCityID' => $cityID]);
                         $cityName = (isset($cityList->cityName)) ? $cityList->cityName : NULL;


                         $locationList = RestaurantLocation::findOne(['pkRestaurantLoactionID' => $locationID]);
                         $locationName = (isset($locationList->locationName)) ? $locationList->locationName : NULL;

                         $restaurantUserNotification[] = array(
                                "notificationID" => $pkRestaurantNotificationsID,
                                "restaurantID" => $fkRestaurantID,
                                "restaurantImage" => $restaurantImage,
                                "description" => $description,
                                "discount" => $discount,
                                "pointsEarned" => $pointsEarned,
                                "cityName" => $cityName,
                                "locationName" => $locationName
                                );

                     }

                        $data['code'] = 200;
                        $data['message'] = 'Notifications details';
                        $data['notifationDetails'] = $restaurantUserNotification;

                    }
                    else
                    {
                        $data['code'] = 500;
                        $data['message'] = 'No result found'; 
                    }  

                }
                else
                {
                    $data['code'] = 500;
                    $data['message'] = 'No result found';

                }

            }
            else
            {

            $data['code'] = 500;
            $data['message'] = 'One or more parameters are missing';

            }

            return $data;
    }

    /**
     * auto search
     * @return json names of restaurant
     */
    public static function autoSearch($request)
    {
        $data = array();
        // Required
        $restaurantName = (isset($request->name)) ? $request->name : NULL;
       
            if($restaurantName)
            {
                $restaurantResults = Restaurant::find()
                ->select(['pkRestaurantID','restaurantName','restaurantLocationID'])
                ->where(['like', 'restaurantName', $restaurantName.'%', false])->all(); 
           // print_r($restaurantResults);
            if($restaurantResults)
            {
                $restaurantList = array();
                foreach($restaurantResults as $restaurantResult)
                {
                $restaurantID = (isset($restaurantResult->pkRestaurantID)) ? $restaurantResult->pkRestaurantID : NULL;
                $locationID = (isset($restaurantResult->restaurantLocationID)) ? $restaurantResult->restaurantLocationID : NULL;
                
                $locationList = RestaurantLocation::findOne(['pkRestaurantLoactionID' => $locationID]);
                $locationName = (isset($locationList->locationName)) ? $locationList->locationName : NULL;

                $name = (isset($restaurantResult->restaurantName)) ? $restaurantResult->restaurantName.' '.$locationName : NULL;
                
                    $restaurantList[] = array(
                    "restaurantID" => $restaurantID,
                    "name" => $name,
                    );
                }
                $data['code'] = 200;
                $data['message'] = 'Restaurant list';
                $data['restaurantList'] = $restaurantList;
                
            }
            else
            {

                $data['code'] = 500;
                $data['message'] = 'No result found';  

            }

            }
            else
            {

            $data['code'] = 500;
            $data['message'] = 'One or more parameters are missing';

            }

            return $data;
    }

    /**
     * setLike
     * @return json like status of restaurant
     */
    public static function setLike($request)
    {
       
        $data = array();
        // Required
        $restaurantID = (isset($request->restaurantID)) ? $request->restaurantID : NULL;
        $userId = (isset($request->userId)) ? $request->userId : NULL;
        $like = (isset($request->like)) ? $request->like : NULL;

       
            if($restaurantID && $userId)
            {
                $restaurantResults = UserRestaurantFavourite::find()->where(['fkRestaurantID' => $restaurantID, 'fkUserID' => $userId])->one();
                if($restaurantResults)
                {
                    
                    $restaurantResults->favourite = $like;
                    if($restaurantResults->update()){

                        $restaurantDetail = Restaurant::find()->select(['restaurantName'])->where(['pkRestaurantID' => $restaurantID])->one();
                        
                        $userDetail = Users::find()->select(['userDeviceToken'])->where(['pkUserID' => $userId])->one();
                        $restaurantName = $restaurantDetail->restaurantName;
                        $userToken = $userDetail->userDeviceToken;
                        if(($like == '1') && ($restaurantName) && ($userToken))
                        {
                            $message = 'Thankyou for liking the '.$restaurantName.' restaurant.'.
                            $extraParams = NULL;


                            $data = array('message' => $message);

                            // The recipient registration tokens for this notification
                            // https://developer.android.com/google/gcm/    

                            // Send push notification via Google Cloud Messaging
                            sendPushNotification($message, $userToken);
                            //$gcm = Yii::$app->gcm->send($userToken, $message, $extraParams);

                        }
                        $data['code'] = 200;
                        $data['message'] = 'Update successfully';
                        $data['like'] = $restaurantResults->favourite;

                    }
                    else
                    {
                        $data['code'] = 200;
                        $data['message'] = 'Error while doing favourite';
                    }
                    
                }
                else
                {
                    $user2 = new UserRestaurantFavourite();
                    $user2->fkRestaurantID = $restaurantID;
                    $user2->fkUserID = $userId;
                    $user2->favourite = $like;
                    if($user2->save(false))
                    {
                        
                       $pkUserRestaurantBookingID = $user2->pkUserRestaurantFavouriteID;
                       if($pkUserRestaurantBookingID)
                       {

                        $data['code'] = 200;
                        $data['message'] = 'Update successfully';
                        $data['like'] = $user2->favourite;

                       }
                       else
                       {

                        $data['code'] = 500;
                        $data['message'] = 'Error while doing favourite';

                       }

                    }
                    else
                    {
                        $data['code'] = 500;
                        $modalErrors = $user->getErrors();
                        foreach($modalErrors as $modalError)
                        {
                            $data['message'] = $modalError[0];
                        }
                    }   
                }
            }
            else
            {

            $data['code'] = 500;
            $data['message'] = 'One or more parameters are missing';

            }
            return $data;
    }

    /**
     * favourite Restaurants list
     * @return json like status of restaurant
     */
    public static function favouriteRestaurants($request)
    {
        $data = array();
        // Required
        $userId = (isset($request->userId)) ? $request->userId : NULL;
        $startLimit = (isset($request->startLimit)) ? $request->startLimit : '0';
        $endLimit = (isset($request->endLimit)) ? $request->endLimit : '10';
        $finalLimit = $endLimit - $startLimit;

            if($userId)
            {
                $restaurantFavourites = UserRestaurantFavourite::find()->
                where(['fkUserID' => $userId, 'favourite' => '1'])->
                orderBy(['pkUserRestaurantFavouriteID' => SORT_DESC])->
                offset($startLimit)->
                limit($finalLimit)->
                all();

                if($restaurantFavourites)
                {
                    $restaurantList = array();
                    foreach($restaurantFavourites as $restaurantFavouritesList)
                    {
                        $restaurantResult = Restaurant::find()->where(['pkRestaurantID' => $restaurantFavouritesList->fkRestaurantID])->one(); 
                        //print_r($restaurantResults);
                        if($restaurantResult)
                        {
                            $restaurantID = (isset($restaurantResult->pkRestaurantID)) ? $restaurantResult->pkRestaurantID : NULL;
                            $image = (isset($restaurantResult->restaurantLogo)) ? $restaurantResult->restaurantLogo : NULL;
                            $status = (isset($restaurantResult->restaurantStatus)) ? $restaurantResult->restaurantStatus : NULL;
                            $name = (isset($restaurantResult->restaurantName)) ? $restaurantResult->restaurantName : NULL;
                            $timing = (isset($restaurantResult->restaurantTimings)) ? $restaurantResult->restaurantTimings : NULL;
                            $cityID = (isset($restaurantResult->restaurantCityID)) ? $restaurantResult->restaurantCityID : NULL;
                            $locationID = (isset($restaurantResult->restaurantLocationID)) ? $restaurantResult->restaurantLocationID : NULL;
                            $latitude = (isset($restaurantResult->restaurantLatitude)) ? $restaurantResult->restaurantLatitude : NULL;
                            $longitude = (isset($restaurantResult->restaurantLongitude)) ? $restaurantResult->restaurantLongitude : NULL;
                            $QRCode = (isset($restaurantResult->restaurantQRCode)) ? $restaurantResult->restaurantQRCode : NULL;

                         $cityList = RestaurantCity::findOne(['pkRestaurantCityID' => $cityID]);
                         $cityName = (isset($cityList->cityName)) ? $cityList->cityName : NULL;


                         $locationList = RestaurantLocation::findOne(['pkRestaurantLoactionID' => $locationID]);
                         $locationName = (isset($locationList->locationName)) ? $locationList->locationName : NULL;


                         $userVisitedNumbers = UserRestaurantBooking::findAll(['fkRestaurantID' => $restaurantID, 'fkUserID' => $userId, 'userCancelBookong' => '0']);
                         $countVisited = count($userVisitedNumbers);
                         $userVisited = (isset($countVisited)) ? $countVisited : 0;

                         $restaurantCuisineIDs = Restaurant_cuisine::findAll(['fkRestaurantID' => $restaurantID]);
                          //  print_r($restaurantCuisineIDs);
                            if($restaurantCuisineIDs)
                              { 
                                $restaurantCuisineList = array();
                                foreach($restaurantCuisineIDs as $restaurantCuisineID)
                                {
                                    $cuisuineID = $restaurantCuisineID->pkRestaurantCuisineNameID;
                                    $restaurantCuisineNamesResult = RestaurantCuisineNames::findOne(['pkRestaurantCuisineNameID' => $cuisuineID]);
                                    $cuisineName = (isset($restaurantCuisineNamesResult->cuisineName)) ? $restaurantCuisineNamesResult->cuisineName : NULL;
                                    $restaurantCuisineList[] = $cuisineName;
                                }

                              }else{
                                $restaurantCuisineList = array();
                              }
                              
                                $restaurantList[] = array(
                                "restaurantID" => $restaurantID,
                                "image" => $image,
                                "status" => $status,
                                "name" => $name,
                                "timimng" => $timing,
                                "city" => $cityName,
                                "locationName" => $locationName,
                                "cuisineList" => $restaurantCuisineList,
                                "latitude" => $latitude,
                                "longitude" => $longitude,
                                "QRCode" => $QRCode,
                                "noOfTimesVisited" => $userVisited
                                );
                        }
                        else
                        {

                            $data['code'] = 500;
                            $data['message'] = 'No result found';  

                        }
                    }
                
                            $data['code'] = 200;
                            $data['message'] = 'Restaurant list';
                            $data['restaurantList'] = $restaurantList;
                    
                    
                }
                else
                {
                     $data['code'] = 500;
                     $data['message'] = 'No result found';
                }
            }
            else
            {

            $data['code'] = 500;
            $data['message'] = 'One or more parameters are missing';

            }
            return $data;
    }

    /*
     *function is for change the password
    */
    public static function changePassword($request)
    {

        $userId = (isset($request->userId)) ? $request->userId : NULL;
        $userPassword = (isset($request->password)) ? $request->password : NULL;
        
        if($userId && $userPassword)
        {
            $user = Users::findOne(['pkUserID' => $userId]); 
            
            if($user) 
            {
                $varPasswordHash = Yii::$app->getSecurity()->generatePasswordHash($userPassword);

                $user->userPassword = $varPasswordHash;
                    if($user->update())
                    {
                        $data['code'] = 200;
                        $data['message'] = 'Password has been changed successfully';
                    }
                    else
                    {

                        $data['code'] = 500;
                        $modalErrors = $user->getErrors();
                        foreach($modalErrors as $modalError)
                        {
                            $data['message'] = $modalError[0];
                        }
                    } 

            }
            else
            {
                $data['code'] = 500;
                $data['message'] = 'User doesnot exists';
            }
        }
        else
        {
            $data['code'] = 500;
            $data['message'] = 'One or more parameters are missing';   
        }

        return $data; 

    }

    /**
     * restaurant offers
     * @return json offers result
     */
    public static function offersDetails($request)
    {
        $data = array();
        // Required
         $discountID = (isset($request->discountID)) ? $request->discountID : NULL;
        
            if($discountID)
            {
                $restaurantOffer = RestaurantDiscounts::find()->where(['pkRestaurantDiscountsID' => $discountID])->one();

                if($restaurantOffer)
                {
                        $discountID = $restaurantOffer->pkRestaurantDiscountsID;
                        $restaurantID = $restaurantOffer->fkRestaurantID;
                        $offerImage = $restaurantOffer->discountImage;
                        $discount = $restaurantOffer->discount;
                        $pointsEarned = $restaurantOffer->pointsEarned;
                        //print_r($image); die;
                        
                    $restaurantResult = Restaurant::findOne(['pkRestaurantID', $restaurantID]);

                    if($restaurantResult)
                    {
                        $restaurantID = (isset($restaurantResult->pkRestaurantID)) ? $restaurantResult->pkRestaurantID : NULL;
                        $image = (isset($restaurantResult->restaurantLogo)) ? $restaurantResult->restaurantLogo : NULL;
                        $description = (isset($restaurantResult->restaurantDescription)) ? $restaurantResult->restaurantDescription : NULL;
                        $status = (isset($restaurantResult->restaurantStatus)) ? $restaurantResult->restaurantStatus : NULL;
                        $name = (isset($restaurantResult->restaurantName)) ? $restaurantResult->restaurantName : NULL;
                        $timing = (isset($restaurantResult->restaurantTimings)) ? $restaurantResult->restaurantTimings : NULL;
                        $cityID = (isset($restaurantResult->restaurantCityID)) ? $restaurantResult->restaurantCityID : NULL;
                        $locationID = (isset($restaurantResult->restaurantLocationID)) ? $restaurantResult->restaurantLocationID : NULL;
                        $latitude = (isset($restaurantResult->restaurantLatitude)) ? $restaurantResult->restaurantLatitude : NULL;
                        $longitude = (isset($restaurantResult->restaurantLongitude)) ? $restaurantResult->restaurantLongitude : NULL;
                        $QRCode = (isset($restaurantResult->restaurantQRCode)) ? $restaurantResult->restaurantQRCode : NULL;

                     $cityList = RestaurantCity::findOne(['pkRestaurantCityID' => $cityID]);
                     $cityName = (isset($cityList->cityName)) ? $cityList->cityName : NULL;


                     $locationList = RestaurantLocation::findOne(['pkRestaurantLoactionID' => $locationID]);
                     $locationName = (isset($locationList->locationName)) ? $locationList->locationName : NULL;
                    }


                        $data['code'] = 200;
                        $data['message'] = 'Offers Images';
                        $data['discountID'] = $discountID;
                        $data['imageUrl'] = $offerImage;
                        $data['discount'] = $discount;
                        $data['restaurantID'] = $restaurantID;
                        $data['name'] = $name;
                        $data['description'] = $description;
                        $data['status'] = $status;
                        $data['cityName'] = $cityName;
                        $data['locationName'] = $locationName;
                        $data['latitude'] = $latitude;
                        $data['longitude'] = $longitude;
                }
                else
                {
                    $data['code'] = 500;
                    $data['message'] = 'No offers found';

                }

            }
            else
            {

            $data['code'] = 500;
            $data['message'] = 'One or more parameters are missing';
           
            }

            return $data;
    }

    /**
     * restaurant offers
     * @return json offers result
     */
    public static function nearByRestaurant($request)
    {
        $data = array();
        // Required
         $latitude = (isset($request->latitude)) ? $request->latitude : NULL;
         $longitude = (isset($request->longitude)) ? $request->longitude : NULL;
         $pageNumber = (isset($request->page)) ? $request->page : NULL;
         $userId = (isset($request->userId)) ? $request->userId : NULL;
         $limit = (isset($request->limit)) ? $request->limit : 20;
        
            if($latitude && $longitude && $pageNumber)
            {
        //$limit = '10';
        if($pageNumber)
        {

            if($pageNumber == '1')
            {
                $offset = $limit*($pageNumber - 1);
                
            }
            elseif ($pageNumber > 1) 
            {
                $offset = $limit*($pageNumber - 1);
            
            }
            else
            {
                $limit = '-1';
                $offset = '0';
                $data['code'] = 500;
                $data['message'] = 'Page number has not correct value';
            }

        }
        else
        {
            $limit = '-1';
            $offset = '0';
        }


                $lat = $latitude;
                $lng = $longitude;          
            // In Kms
            $distanceWithin = 30;
           
            // Distance Algo
            $distanceSql = "(6371*acos(cos(radians( $lat ))*cos(radians( restaurantLatitude ))*cos(radians( restaurantLongitude )-radians( $lng ))+sin(radians( $lat ))*sin(radians( restaurantLatitude )))) AS distance";

            $restaurantResults = Restaurant::find()->select(['pkRestaurantID', 'restaurantLatitude', 'restaurantLongitude', $distanceSql])
                ->having(['<=', 'distance', $distanceWithin])
                ->orderBy(['distance' => SORT_ASC])
                ->offset($offset)->limit($limit)
                ->all();                  
                
                if($restaurantResults)
            {
                $restaurantList = array();
                foreach($restaurantResults as $restaurantResult)
                {
                $restaurantID = (isset($restaurantResult->pkRestaurantID)) ? $restaurantResult->pkRestaurantID : NULL;
                $image = (isset($restaurantResult->restaurantLogo)) ? $restaurantResult->restaurantLogo : NULL;
                $status = (isset($restaurantResult->restaurantStatus)) ? $restaurantResult->restaurantStatus : NULL;
                $name = (isset($restaurantResult->restaurantName)) ? $restaurantResult->restaurantName : NULL;
                $timing = (isset($restaurantResult->restaurantTimings)) ? $restaurantResult->restaurantTimings : NULL;
                $cityID = (isset($restaurantResult->restaurantCityID)) ? $restaurantResult->restaurantCityID : NULL;
                $locationID = (isset($restaurantResult->restaurantLocationID)) ? $restaurantResult->restaurantLocationID : NULL;
                $latitude = (isset($restaurantResult->restaurantLatitude)) ? $restaurantResult->restaurantLatitude : NULL;
                $longitude = (isset($restaurantResult->restaurantLongitude)) ? $restaurantResult->restaurantLongitude : NULL;
                $QRCode = (isset($restaurantResult->restaurantQRCode)) ? $restaurantResult->restaurantQRCode : NULL;

             $cityList = RestaurantCity::findOne(['pkRestaurantCityID' => $cityID]);
             $cityName = (isset($cityList->cityName)) ? $cityList->cityName : NULL;


             $locationList = RestaurantLocation::findOne(['pkRestaurantLoactionID' => $locationID]);
             $locationName = (isset($locationList->locationName)) ? $locationList->locationName : NULL;


             $userVisitedNumbers = UserRestaurantBooking::findAll(['fkRestaurantID' => $restaurantID, 'fkUserID' => $userId, 'userCancelBookong' => '0']);
             $countVisited = count($userVisitedNumbers);
             $userVisited = (isset($countVisited)) ? $countVisited : 0;

             $restaurantCuisineIDs = Restaurant_cuisine::findAll(['fkRestaurantID' => $restaurantID]);
              //  print_r($restaurantCuisineIDs);
                if($restaurantCuisineIDs)
                  { 
                    $restaurantCuisineList = array();
                    foreach($restaurantCuisineIDs as $restaurantCuisineID)
                    {
                        $cuisuineID = $restaurantCuisineID->pkRestaurantCuisineNameID;
                        $restaurantCuisineNamesResult = RestaurantCuisineNames::findOne(['pkRestaurantCuisineNameID' => $cuisuineID]);
                        $cuisineName = (isset($restaurantCuisineNamesResult->cuisineName)) ? $restaurantCuisineNamesResult->cuisineName : NULL;
                        $restaurantCuisineList[] = $cuisineName;
                    }

                  }else{
                    $restaurantCuisineList = array();
                  }
                  
                    $restaurantList[] = array(
                    "restaurantID" => $restaurantID,
                    "image" => $image,
                    "status" => $status,
                    "name" => $name,
                    "timimng" => $timing,
                    "city" => $cityName,
                    "locationName" => $locationName,
                    "cuisineList" => $restaurantCuisineList,
                    "latitude" => $latitude,
                    "longitude" => $longitude,
                    "QRCode" => $QRCode,
                    "noOfTimesVisited" => $userVisited
                    );
                }
                $data['code'] = 200;
                $data['message'] = 'Restaurant list';
                $data['pageNumber'] = $pageNumber;
                $data['restaurantList'] = $restaurantList;
                
            }
            else
            {

                $data['code'] = 500;
                $data['message'] = 'No result found';  

            }

                
            }
            else
            {

            $data['code'] = 500;
            $data['message'] = 'One or more parameters are missing';
            }

            return $data;
    }

    /*
     *function is for get userId by qr code
    */
    public static function getRestaurantIdByQRCode($request)
    {

        $QRCode = (isset($request->QRCode)) ? $request->QRCode : NULL;
        if($QRCode)
        {
           $restaurantResult = Restaurant::find()->select(['pkRestaurantID','restaurantLatitude','restaurantLongitude'])->where(['restaurantQRCode'=>$QRCode])->one();
            
            if($restaurantResult) 
            {
                $restaurantID = $restaurantResult->pkRestaurantID;

                $data['code'] = 200;
                $data['message'] = 'Restaurant id';
                $data['restaurantID'] = $restaurantID;
                $data['latitude'] = $restaurantResult->restaurantLatitude;
                $data['longitude'] = $restaurantResult->restaurantLongitude;
                $data['qrcode'] = $QRCode;
                
            }
            else
            {
                $data['code'] = 500;
                $data['message'] = 'No restaurant found';
            }
        }
        else
        {
            $data['code'] = 500;
            $data['message'] = 'One or more parameters are missing';   
        }
        return $data; 
    }

    /*
     *function is for get the redemtion summary
    */
    public static function redemtionSummary($request)
    {

        $userId = (isset($request->userId)) ? $request->userId : NULL;
        $startLimit = (isset($request->startLimit)) ? $request->startLimit : '0';
        $endLimit = (isset($request->endLimit)) ? $request->endLimit : '10';
        $finalLimit = $endLimit - $startLimit;
        if($userId)
            {
            $restaurantBookingRestaurantIDs = UserRestaurantBooking::find()
            ->select(['fkRestaurantID'])
            ->where(['fkUserID' => $userId, 'userCancelBookong' => '0'])
            ->orderBy(['pkUserRestaurantBookingID' => SORT_DESC])
            ->groupBy(['fkRestaurantID'])
            ->offset($startLimit)
            ->limit($finalLimit)
            ->all();
            
                if($restaurantBookingRestaurantIDs)
                {   
                    $restaurantList = array();
                    foreach($restaurantBookingRestaurantIDs as $restaurantBookingRestaurantID)
                    {
                        
                        $restaurantResult = Restaurant::findOne(['pkRestaurantID', $restaurantBookingRestaurantID->fkRestaurantID]);
                        if($restaurantResult)
                        {
                            $restaurantID = $restaurantBookingRestaurantID->fkRestaurantID;
                            $image = (isset($restaurantResult->restaurantLogo)) ? $restaurantResult->restaurantLogo : NULL;
                            $name = (isset($restaurantResult->restaurantName)) ? $restaurantResult->restaurantName : NULL;
                            $description = (isset($restaurantResult->restaurantDescription)) ? $restaurantResult->restaurantDescription : NULL;

                            $cityID = (isset($restaurantResult->restaurantCityID)) ? $restaurantResult->restaurantCityID : NULL;
                            $locationID = (isset($restaurantResult->restaurantLocationID)) ? $restaurantResult->restaurantLocationID : NULL;


                            $cityList = RestaurantCity::findOne(['pkRestaurantCityID' => $cityID]);
                            $cityName = (isset($cityList->cityName)) ? $cityList->cityName : NULL;


                            $locationList = RestaurantLocation::findOne(['pkRestaurantLoactionID' => $locationID]);
                            $locationName = (isset($locationList->locationName)) ? $locationList->locationName : NULL;
                        }

                        $restaurantBookingResult = UserRestaurantBooking::find()
                        ->where(['fkUserID' => $userId, 'userCancelBookong' => '0' ,'fkRestaurantID' => $restaurantBookingRestaurantID->fkRestaurantID])
                        ->all();
                        //print_r($restaurantBookingResult);
                            
                            $userPointsEarnedPoints=0;
                            $waitingTime = 0;
                            foreach($restaurantBookingResult as $restaurantBookingResults)
                            {
                                $pkUserRestaurantBookingID = $restaurantBookingResults->pkUserRestaurantBookingID;
                                $fkRestaurantID = $restaurantBookingResults->fkRestaurantID;

                                $userPointsEarned = UserPointsEarned::findOne(['fkRestaurantID' => $fkRestaurantID, 'fkUserID' => $userId, 'fkUserRestaurantBookingID' => $pkUserRestaurantBookingID]);
                                
                                if($userPointsEarned)
                                {   
                                    $userPointsEarnedPoints += $userPointsEarned->userPointsEarned;
                                }
                                    $waitingTime += $restaurantBookingResults->waitingTime;
                                
                            }
                                    $noOfTimesVisited = count($restaurantBookingResult);

                                        $restaurantList[] = array(
                                        "restaurantID" => $restaurantID,
                                        "cityName" => $cityName,
                                        "locationName" => $locationName,
                                        "image" => $image,
                                        "name" => $name,
                                        "description" => $description,
                                        "noOfTimesVisited" => $noOfTimesVisited,                            
                                        "waitingTime" => $waitingTime,
                                        "userPointsEarned" => $userPointsEarnedPoints,
                                        ); 
                    }
                    
                    $data['code'] = 200;
                    $data['message'] = 'Reward Summary details';
                    $data['rewardSummary'] = $restaurantList;

                }
                else
                {
                    $data['code'] = 500;
                    $data['message'] = 'No result found';

                }
                             
            }
            else
            {

            $data['code'] = 500;
            $data['message'] = 'One or more parameters are missing';

            }
        return $data; 
    }

    /*
     *function is for get userId by qr code
    */
    public static function checkMobileNumber($request)
    {

        $userNumber = (isset($request->userNumber)) ? $request->userNumber : NULL;
        if($userNumber)
        {
           $user = Users::findOne(['userNumber' => $userNumber]);
            if($user)
            {

               $data['code'] = 500;
               $data['message'] = 'This number is already exists';
                     
            }
            else
            {
               $data['code'] = 200;
               $data['message'] = 'Available'; 
               $data['userNumber'] = $userNumber; 

            }
        }
        else
        {
            $data['code'] = 500;
            $data['message'] = 'One or more parameters are missing';   
        }
        return $data; 
    }


}

function sendPushNotification($data, $ids)
{
    // Insert real GCM API key from the Google APIs Console
    // https://code.google.com/apis/console/        
    $apiKey = 'AIzaSyBSCaIhQiYPZq24go_z0QHsEgWAxJcScXM';

    // Set POST request body
    $post = array(
                    'to'  => $ids,
                    'notification'  => ['body' => $data, 'priority' => 10],
                 );

    // Set CURL request headers 
    $headers = array( 
                        'Authorization: key=' . $apiKey,
                        'Content-Type: application/json'
                    );

    // Initialize curl handle       
    $ch = curl_init();

    // Set URL to GCM push endpoint     
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');

    // Set request method to POST       
    curl_setopt($ch, CURLOPT_POST, true);

    // Set custom request headers       
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    // Get the response back as string instead of printing it       
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Set JSON post data
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));

    // Actually send the request    
    $result = curl_exec($ch);

    // Handle errors
    if (curl_errno($ch))
    {
        echo 'GCM error: ' . curl_error($ch);
    }

    // Close curl handle
    curl_close($ch);

    // Debug GCM response       
    //echo '<pre>';
    //echo $result;
}


