<?php

namespace frontend\modules\api\models;

use Yii;

/**
 * This is the model class for table "restaurant".
 *
 * @property string $pkRestaurantID
 * @property string $restaurantName
 * @property string $restaurantDescription
 * @property string $restaurantCity
 * @property string $restaurantTimings
 * @property string $restaurantType
 * @property string $restaurantLogo
 * @property string $restaurantLocation
 * @property string $restaurantAddress
 */
class Restaurant extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restaurant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['restaurantName', 'restaurantLogo'], 'string', 'max' => 200],
            [['restaurantDescription'], 'string', 'max' => 1000],
            [['restaurantCity'], 'string', 'max' => 20],
            [['restaurantTimings', 'restaurantType', 'restaurantLocation'], 'string', 'max' => 100],
            [['restaurantAddress'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkRestaurantID' => 'Pk Restaurant ID',
            'restaurantName' => 'Restaurant Name',
            'restaurantDescription' => 'Restaurant Description',
            'restaurantCity' => 'Restaurant City',
            'restaurantTimings' => 'Restaurant Timings',
            'restaurantType' => 'Restaurant Type',
            'restaurantLogo' => 'Restaurant Logo',
            'restaurantLocation' => 'Restaurant Location',
            'restaurantAddress' => 'Restaurant Address',
        ];
    }
}
