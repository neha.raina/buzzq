<?php

namespace frontend\modules\api\models;

use Yii;

/**
 * This is the model class for table "user_restaurant_booking".
 *
 * @property string $pkUserRestaurantBookingID
 * @property string $fkRestaurantID
 * @property string $fkUserID
 * @property string $date
 * @property integer $userCancelBookong
 */
class UserRestaurantBooking extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_restaurant_booking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkRestaurantID', 'fkUserID', 'userCancelBookong'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkUserRestaurantBookingID' => 'Pk User Restaurant Booking ID',
            'fkRestaurantID' => 'Fk Restaurant ID',
            'fkUserID' => 'Fk User ID',
            'date' => 'Date',
            'userCancelBookong' => 'User Cancel Bookong',
        ];
    }
}
