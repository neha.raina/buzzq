<?php

namespace frontend\modules\api\models;

use Yii;

/**
 * This is the model class for table "restaurant_location".
 *
 * @property integer $pkRestaurantLoactionID
 * @property string $locationName
 */
class RestaurantLocation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restaurant_location';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['locationName'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkRestaurantLoactionID' => 'Pk Restaurant Loaction ID',
            'locationName' => 'Location Name',
        ];
    }
}
