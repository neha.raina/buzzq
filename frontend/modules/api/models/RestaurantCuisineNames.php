<?php

namespace frontend\modules\api\models;

use Yii;

/**
 * This is the model class for table "restaurant_cuisine_names".
 *
 * @property integer $pkRestaurantCuisineNameID
 * @property string $cuisineName
 */
class RestaurantCuisineNames extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restaurant_cuisine_names';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cuisineName'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkRestaurantCuisineNameID' => 'Pk Restaurant Cuisine Name ID',
            'cuisineName' => 'Cuisine Name',
        ];
    }
}
