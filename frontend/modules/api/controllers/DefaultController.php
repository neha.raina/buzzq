<?php

namespace frontend\modules\api\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use frontend\models\Api;
use frontend\modules\api\models\Restaurant;
use frontend\modules\api\models\App;


/**
 * Request Handler Controller Class
 * 
 */
class DefaultController extends Controller
{
    public $request;

    /**
     * @inheritdoc
     * Accept only Post Requests
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['post']
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     * Standalone action for error
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    // public $enableCsrfValidation = false;
    /**
     * Perform this action before every request
     * Date modified : Jul 13 2016
     */
    public function beforeAction($event)
    {

       /**
         * Snippet to check if a request method (GET/POST)
         * on an action is allowed or not
         */
        $action = $event->id;
        
        if(isset($this->actions[$action]))
        {
            $verbs = $this->actions[$action];
        }
        elseif(isset($this->actions['*']))
        {
            $verbs = $this->actions['*'];
        }
        else
        {
        }
        
        $verb = Yii::$app->getRequest()->getMethod();
        $allowed = array_map('strtoupper', $verbs);
        
        if(!in_array($verb, $allowed))
        {
            $response = Yii::$app->response;
            $response->format = \yii\web\Response::FORMAT_JSON;
            $response->data = ['code' => 201,'message' => 'Request method not allowed'];
            return;
        }



        /**
         * Set default request parser to json
         * Parse every request as Json
         * Set the parsed request to $request property
         */

        // mail('saurabh.sharma@mail.vinove.com', 'Tafoos : Json Payload', Yii::$app->request->post('json_data'));
        $this->request = Json::decode(Yii::$app->request->post('json_data'), false);


        /**
         * Send all requests in Json format
         * Set Content Type as Json
         */
        Yii::$app->response->format = 'json';


        /**
         * Default Csrf protection on API Module
         */
        $this->enableCsrfValidation = false;
        return parent::beforeAction($event);
    }

    public function actionIndex()
    {
        
        
        //$request = Yii::$app->request->post('jsonData'); // $_POST['jsonData']

       // $jsonData = json_decode( $request, true );
        $data = array();

        $method = $this->request->method;

        switch( $method ){
            /**
             * Accounts and Profile related WebServices
             * @author Saurabh Sharma
             * @package Tafoos
             */
            
            case 'validateOTP':
            $data = App::validateOTP($this->request);
            break;

            case 'signup':
            $data = App::signup($this->request);
            break;

            case 'resendOTP':
            $data = App::resendOTP($this->request);
            break;  

            case 'login':
            $data = App::login($this->request);
            break;  
            
            case 'forgetPassword':
            $data = App::forgetPassword($this->request);
            break;

            case 'getCities':
            $data = App::getCities($this->request);
            break; 

            case 'getCuisines':
            $data = App::getCuisines($this->request);
            break; 

            case 'getLocations':
            $data = App::getLocations($this->request);
            break;
            
            case 'searchRestaurant':
            $data = App::searchRestaurant($this->request);
            break; 

            case 'restaurantDetail';
            $data = App::restaurantDetail($this->request);
            break;

            case 'restaurantBooking';
            $data = App::restaurantBooking($this->request);
            break;

            case 'cancelBooking';
            $data = App::cancelBooking($this->request);
            break;

            case 'offers';
            $data = App::offers($this->request);
            break;

            case 'updateProfile';
            $data = App::updateProfile($this->request);
            break;

            case 'buzzIn';
            $data = App::buzzIn($this->request);
            break;

            case 'buzzOut';
            $data = App::buzzOut($this->request);
            break;

            case 'transaction';
            $data = App::transaction($this->request);
            break;

            case 'rewardSummaryDate';
            $data = App::rewardSummaryDate($this->request);
            break;

            case 'rewardSummaryRestaurant';
            $data = App::rewardSummaryRestaurant($this->request);
            break;
            
            case 'notifications';
            $data = App::notifications($this->request);
            break;
            
            case 'autoSearch';
            $data = App::autoSearch($this->request);
            break;

            case 'setLike';
            $data = App::setLike($this->request);
            break;

            case 'favouriteRestaurants';
            $data = App::favouriteRestaurants($this->request);
            break;
            
            case 'changePassword';
            $data = App::changePassword($this->request);
            break;
            
            case 'offersDetails';
            $data = App::offersDetails($this->request);
            break; 
                       
            case 'nearByRestaurant';
            $data = App::nearByRestaurant($this->request);
            break;
             
            case 'getRestaurantIdByQRCode';
            $data = App::getRestaurantIdByQRCode($this->request);
            break;
            
            case 'redemtionSummary';
            $data = App::redemtionSummary($this->request);
            break;
            
            case 'checkMobileNumber';
            $data = App::checkMobileNumber($this->request);
            break;
            
        }

        return $data;
    }
}
