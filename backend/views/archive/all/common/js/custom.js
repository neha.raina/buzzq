$(document).ready(function(){

    $(".time-toggle").click (function(){
        $(".timer-box").toggle();
    });


   function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputFile").change(function () {
        readURL(this);
    });
	
	
	$('.owl-carousel').owlCarousel({
    loop:true,
    margin:0,
	autoplay: true,
    nav:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})

    
});

/* ///////////////////////////////////////////////////////////////////// 
// Preloader 
/////////////////////////////////////////////////////////////////////*/
 $(window).load(function(){
    $('#preloader').fadeOut('slow',function(){$(this).remove();});
  });