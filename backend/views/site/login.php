<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>

<div class="col-xs-12 col-sm-10 col-sm-push-1 col-md-6 col-md-push-3 col-lg-push-3 col-lg-6">
    <div class="signin_box">
        <div class="signin_logo text-center">
            <img src="<?php echo Url::to('@web/images/signin-logo.png') ?>" alt="Signin Logo"/>
        </div>
        <div class="signin_inner_box">
            <h1 class="text-center">Sign in to your account</h1>


            <?php $form = ActiveForm::begin(['action' => '', 'options' => ['class' => 'form-signin']]); ?>   

            <label for="inputEmail" class="sr-only">Email address</label>
            <?php
            echo $form->field($model, 'username', [
                'inputOptions' => ['autofocus' => '', 'class' => 'form-control', 'id' => 'inputEmail']
            ])->textInput()->input('email', ['placeholder' => "Email"])->label(false);
            ?>

            <label for="inputPassword" class="sr-only">Password</label>

            <?php
            echo $form->field($model, 'password', [
                'inputOptions' => ['autofocus' => '', 'class' => 'form-control', 'id' => 'inputPassword']
            ])->textInput()->input('password', ['placeholder' => "Password"])->label(false);
            ?>



            <!--<label for="test1">Remember Me</label>-->				
             <?= Html::a('SignUp', ['signup']) ?>
             <br>
            <?php echo $form->field($model, 'rememberMe', ['inputOptions' => ['id' => 'ttt']])->checkbox()->label('Remember Me'); ?>


            <?php //echo Html::a('Forgot Password?', Yii::$app->urlManager->createUrl(['site/request-password-reset']), ['class' => 'pull-right']) ?>

            <?= Html::submitButton('Sign in', ['class' => 'btn btn-lg btn-primary btn-block', 'name' => 'login-button']) ?>



            <?php ActiveForm::end(); ?>


        </div>

    </div>
</div>

