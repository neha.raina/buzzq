 <?php
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Url;
    use backend\models\UserRestaurantManage;
    use backend\models\UserRestaurantBooking;


    $this->title = 'Dashboard';
?>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header"></h1>

          <div class="row">
            <div class="col-lg-4 col-sm-4">
            <?php
            $currUserId = Yii::$app->user->getId();
            $countRestaurant = UserRestaurantManage::find()->where(['fkUserRestaurantOwnerID' => $currUserId])->count();
            $countBookings = UserRestaurantBooking::find()->joinWith(['fkUserManage'])->where(['user_restaurant_manage.fkUserRestaurantOwnerID' => $currUserId])->count();
            
            $toDate = isset($dataProvider['dateTo']) ? $dataProvider['dateTo'] : NULL;
            $fromDate = isset($dataProvider['dateFrom']) ? $dataProvider['dateFrom'] : NULL;
            ?>
                <div class="numof bookings"><h3><span><?php echo $countBookings;?></span><br>No of Bookings</h3></div>
                <div class="numof stores"><h3><span><?php echo $countRestaurant;?></span><br>No of Restaurants</h3></div>
            </div>

            <div class="col-lg-8 col-sm-8">
                  <div class="track-record">
                    <h2>Track Report</h2> 
                      <?php $form = ActiveForm::begin([
                          'action' => ['index'],
                          'method' => 'post',
                      ]); ?>
                        <div class="row">
                            <div class="col-lg-6 col-sm-6">
                                <div  class="fieldBox from-field form-group">
                                    <input type="text" id="mainDateFrom" name="dateFrom" value="<?php if(@$fromDate){ echo $fromDate;}?>" placeholder="From">
                                </div>
                            </div>

                            <div class="col-lg-5 col-sm-5">
                                <div  class="fieldBox from-field form-group">
                                    <input type="text" id="mainDateTo" name="dateTo" value="<?php if(@$toDate){ echo $toDate;}?>" placeholder="To">
                                </div>
                            </div>

                            <!-- <div class="col-lg-6 col-sm-6">
                                <div  class="fieldBox time-field form-group">
                                    <input class="time-toggle" type="text" id="usr2" placeholder="Time">
                                    <div class="timer-box timer-dots">
                                    <div class="inner">
                                        <div class="timer-set">
                                          <a class="up" href="javascript:void(0)"></a>
                                          <input type="text" value="04">
                                          <a class="down" href="javascript:void(0)"></a>
                                        </div>

                                        <div class="timer-set">
                                          <a class="up" href="javascript:void(0)"></a>
                                          <input type="text" value="30">
                                          <a class="down" href="javascript:void(0)"></a>
                                        </div>

                                        <div class="timer-set dot-none">
                                          <a class="up" href="javascript:void(0)"></a>
                                          <input type="text" value="AM">
                                          <a class="down" href="javascript:void(0)"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             </div> -->

                            <div class="col-lg-5 col-sm-5">
                                <div  class="btnBox">
                                    <input type="submit" class="btn" value="Submit" id="graphSubmit">
                                </div>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                  </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-12 col-sm-12">
               <div class="graph-container">
                <div class="row">
                <div class="col-lg-3 col-sm-4 pull-right"> 
                    <div class="graph-notification">
                          <div class="text"><span class="blue"></span>Total Bookings</div>
                          <!-- <div class="text"><span class="green"></span>Total Stores</div> -->
                    </div>
                </div>
                <?php
                    
                    $date1=date_create($fromDate);
                    $date2=date_create($toDate);
                    $diff=date_diff($date1,$date2);
                    $days = $diff->format("%d");
                    $days = $days + 1;
                    $months = $diff->format("%m");
                    
                    if($months < 1)
                    {
                        for($d=0; $d<$days; $d++)
                        {
                            $addDay = strtotime($fromDate . '+'.$d.' day');
                            $addDayModified = date('Y-m-d',$addDay);

                               $bookings = UserRestaurantBooking::find()->joinWith(['fkUserManage'])->where(['user_restaurant_manage.fkUserRestaurantOwnerID' => $currUserId,'date' => $addDayModified])
                               ->count();

                               if($bookings > 0)
                               {
                                  $valueArray[] = (int)$bookings;
                                  $dateArray[] = date('F j',$addDay);
                               }

                        }
                        // print_r($dateArray);
                        // print_r($valueArray);
                    }
                    else
                    {
                        $newMonths = $months + 1;
                        for($d=0; $d<$newMonths; $d++)
                        {
                            if($months == $d){

                              $fromEndDate = $toDate;
                            }
                            else
                            {
                              $fromEndDate = date("Y-m-t", strtotime($fromDate));
                            }
                            $fromEndDateMonth = strtotime($fromEndDate);
                             
                             $bookings = UserRestaurantBooking::find()->joinWith(['fkUserManage'])->where(['user_restaurant_manage.fkUserRestaurantOwnerID' => $currUserId])
                             ->andWhere(['<=', 'date', $fromEndDate])
                             ->andWhere(['>=', 'date', $fromDate])
                             ->count();

                             $fromDate1 = strtotime($fromEndDate.'+1 day');
                             $fromDate = date('Y-m-d',$fromDate1);

                             $valueArray[] = (int)$bookings;
                             $dateArray[] = date('F',$fromEndDateMonth);
                        }
                    }
                        if(@!$valueArray){ $valueArray = NULL; }
                        if(@!$dateArray){ $dateArray = NULL; }
                ?>

                <div class="col-lg-9 col-sm-8">
                    <div class="graph-in">
                    <div id="container" style="width:100%; height:400px;"></div>
                        
                    </div>
                </div>
              </div>
              </div>
          </div>
      </div>
      <div class="coptyright">BuzzQ © 2016. All Rights Reserved.</div>
        </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script>

            var dateArray = '<?php echo json_encode($dateArray);?>';
            var valueArray = '<?php echo json_encode($valueArray);?>';
            
            newDateArray = $.parseJSON(dateArray);
            newValueArray = $.parseJSON(valueArray);

            $(function () {
            var myChart = Highcharts.chart('container', {
              title: {
                  text: 'Track Report',
                  x: -20 //center
              },
              credits: {
                      enabled: false
                  },
              xAxis: {
                  categories: newDateArray
              },
              yAxis: {
                  title: {
                      text: 'Numbers'
                  },
                  plotLines: [{
                      value: 0,
                      width: 1,
                      color: '#808080'
                  }]
              },
              legend: {
                  layout: 'vertical',
                  align: 'right',
                  verticalAlign: 'middle',
                  borderWidth: 0
              },
              series: [{
                  name: 'Reservation',
                  data: newValueArray
              }]
              });
            });

        </script>
