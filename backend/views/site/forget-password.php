<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="common/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="common/css/dashboard.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body class="signin_page">
	<!-- Page Preloader -->
	<div id="preloader"></div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-sm-push-1 col-md-6 col-md-push-3 col-lg-push-3 col-lg-6">
				<div class="signin_box">
					<div class="signin_logo text-center">
						<img src="common/images/signin-logo.png" alt="Signin Logo"/>
					</div>
					<div class="signin_inner_box forget_password_box">
						<h1 class="text-center">Forgot your password?</h1>
						<p class="text-center">Enter your email address below and we'll send you password reset instructions.</p>
						<form class="form-signin">
						<label for="inputEmail" class="sr-only">Email address</label>
						<input id="inputEmail" class="form-control" placeholder="Email" required="" autofocus="" type="email">
						<button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="common/js/bootstrap.min.js"></script>
    <script src="common/js/custom.js"></script>

  </body>
</html>
