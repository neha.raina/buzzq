<?php

use yii\helpers\Url;
use yii\widgets\Menu;
?>
<div class="col-sm-3 col-md-2 sidebar">
    <div class="logo"><img src="<?php echo Url::to('@web/images/logo.png'); ?>" alt="logo"></div>

     <?php
            echo           Menu::widget(
                            [
                                'options' => ['class' => 'nav nav-sidebar'],
                                "items" => [
                                    [
                                        "label" => "Dashboard",
                                        'options'=>['class'=>'dashboard'],
                                        "url" => ["site/index"],
                                    ],
                                
                                    [
                                        "label" => "Reservations",
                                        'options'=>['class'=>'reservations'],
                                        "url" => ["user-restaurant-booking/index"],
                                    ],
                                    
                                    [
                                        "label" => "Store & Offers",
                                        'options'=>['class'=>'store'],
                                        "url" => ["restaurant-discounts/index"],
                                    ],
                                    
                                    [
                                        "label" => "Restaurants",
                                        'options'=>['class'=>'restaurants'],
                                        "url" => ["restaurant/index"],
                                    ],
                                    
                                    [
                                        "label" => "Transactions",
                                        'options'=>['class'=>'transactions'],
                                        "url" => ["user-restaurant-booking/transactions"],
                                    ],
                                    
                                    [
                                        "label" => "Logout",
                                        'options'=>['class'=>'logout'],
                                        "url" => ["site/logout"],
                                    ],
                                    
                                  ],
                            ]
                        )
                        ?>
</div>