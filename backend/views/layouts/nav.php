<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use backend\models\UserRestaurantOwner;
?>
<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Dashboard</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
           <?= Html::encode($this->title) ?>
          <ul class="nav navbar-nav navbar-right">
          <?php
             $currUserId = Yii::$app->user->getId();
             $result = UserRestaurantOwner::find()->where(['pkUserRestaurantOwnerID' => $currUserId])->one();
          ?>
            <li class="dropdown"><img style="width:48px;height:48px" src="<?php if($result->userPicture){ echo $result->userPicture;}else{echo Url::to('@web/images/wecome-user.png');} ?>" alt="user"><a class="dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Welcome <small><?php echo $result->userName;?></small></a>
                <ul class="user-menu dropdown-menu">
                  <!-- <li><a href="#">Profile</a></li> -->
                  <li><?php echo Html::a('Logout', ['site/logout']) ?></li>
                </ul>
            </li>
          </ul>
        </div>
      </div>
</nav>