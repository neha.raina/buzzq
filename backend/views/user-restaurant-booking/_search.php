<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Restaurant;

/* @var $this yii\web\View */
/* @var $model backend\models\SearchReservations */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="content-area search-bookig">
    <h2>Search</h2>

    <?php
    $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
    ]);
    ?>
    <div class="row">
        <div class="col-lg-4 col-sm-4">
            <div class="fieldBox booking-id">

<?php
echo $form->field($model, 'pkUserRestaurantBookingID')->textInput()->input('text', ['placeholder' => "Booking ID"])->label(false);
?>
            </div>   
        </div>

        <div class="col-lg-4 col-sm-4">
            <div class="fieldBox name">

<?php
echo $form->field($model, 'userName')->textInput()->input('text', ['placeholder' => "Name"])->label(false);
?>

            </div>   
        </div>

        <div class="col-lg-4 col-sm-4">
            <div class="fieldBox time">
               
<?php
echo $form->field($model, 'waitingTime')->textInput()->input('text', ['placeholder' => "Waiting Time"])->label(false);
?>
            </div>   
        </div>

        <div class="col-lg-4 col-sm-4">
            <div class="fieldBox phone">
                
<?php
echo $form->field($model, 'userNumber')->textInput()->input('text', ['placeholder' => "Phone No"])->label(false);
?>
            </div>   
        </div>

        <div class="col-lg-4 col-sm-4">
            <div class="fieldBox date-book">
                <div class="styled-select slate">

<?php echo $form->field($model, 'userVip')->dropDownList(['' => 'Select Customer Category', '0' => 'Normal', '1' => 'VIP'])->label(false); ?>
                </div>
            </div>  
        </div>
        

        <div class="col-lg-4 col-sm-4">
            <div class="fieldBox date-book">
                <div class="styled-select slate">
            <?php
            $currUserId = Yii::$app->user->getId();
            $currentuserId = isset($currUserId) ? $currUserId : '0';
            $items = ArrayHelper::map(Restaurant::find()->joinWith(['userRestaurantManages'])
            ->where(['user_restaurant_manage.fkUserRestaurantOwnerID' => $currentuserId])->all(), 'pkRestaurantID', 'restaurantName');
          
    echo $form->field($model, 'fkRestaurantID')->dropdownList($items,
    ['prompt'=>'Select Restaurant'])->label(false);     
    ?>               
                </div>
            </div>  
        </div>

        <div class="col-lg-4 col-sm-4">
            <div class="btnBox">
              <input class="btn" value="Search" type="submit">
                <?php  //echo Html::submitButton('Search', ['class' => 'btn']) ?>
                <?php  //echo Html::a('Reset',  ['user-restaurant-booking/index']) ?>
            </div> 
        </div>
    </div>
<?php ActiveForm::end(); ?>
</div>

