<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\RestaurantLocation;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserRestaurantBookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'Manage Reservation';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
<h1>Transactions</h1>
     <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php  echo $this->render('_searchTransaction', ['model' => $searchModel]); ?>
    <div class="total-bookigs">

   <div class="booking-list">

            <div class="table-responsive">
    <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'Transaction ID',
                            'value' => 'pkUserRestaurantBookingID'
                        ],
                        [
                            'attribute' => 'Date',
                            'value' => 'date',
                            'format' => ['date', 'php:d-M-Y']
                        ],
                        [
                            'attribute' => 'Reservation Person',
                            'value' => 'fkUser.userName',
                        ],
                        [
                            'attribute' => 'Restaurant',
                            'value' => function($dataProvider){
                                $locationName = RestaurantLocation::findOne(['pkRestaurantLoactionID' => $dataProvider->fkRestaurant->restaurantLocationID]);
                                return $dataProvider->fkRestaurant->restaurantName.' ('.$locationName->locationName.')';
                            },
                        ],
                        [
                            'attribute' => 'Phone No',
                            'value' => 'fkUser.userNumber',
                        ],
                        [
                            'attribute' => 'Email',
                            'value' => 'fkUser.userEmail',
                        ],
                       // ['class' => 'yii\grid\ActionColumn'],
                        ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
                    ],
                ]);
                ?>
</div>
</div>

<div class="coptyright">BuzzQ © 2016. All Rights Reserved.</div>
    </div>
</div>
