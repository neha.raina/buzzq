<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\UserRestaurantBooking */

$this->title = Yii::t('app', 'Create Restaurant Booking');
//$this->params['breadcrumbs'][] = ['label' => 'User Restaurant Bookings', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
