<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\UserRestaurantBooking */

$this->title = 'Update User Restaurant Booking: ' . $model->pkUserRestaurantBookingID;
$this->params['breadcrumbs'][] = ['label' => 'User Restaurant Bookings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pkUserRestaurantBookingID, 'url' => ['view', 'id' => $model->pkUserRestaurantBookingID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-restaurant-booking-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
