<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\RestaurantLocation;
use backend\models\UserRestaurantManage;
use backend\models\UserRestaurantBooking;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserRestaurantBookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Reservation';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
<h1>Manage Reservation</h1>
     <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    
<?php 

        $currUserId = Yii::$app->user->getId();
$count = UserRestaurantBooking::find()->joinWith(['fkUserManage'])->where(['user_restaurant_manage.fkUserRestaurantOwnerID' => $currUserId])->count();
//$count = Yii::$app->db->createCommand('SELECT COUNT(*) FROM user_restaurant_booking')->queryScalar(); ?>
    <div class="total-bookigs">
        <h4>Total No of Booking: <?php echo $count;?></h4>

        <?= Html::a('+ Add New', ['create'], ['class' => 'action-addNew']) ?>
   <div class="booking-list">

            <div class="table-responsive">
    <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'Category',
                            'value' => function($dataProvider){ 
                                //echo '<pre>'; print_r($dataProvider); echo '</pre>'; die();
                                if ($dataProvider->fkUser->userVip == '1')
                                    return 'Vip';
                                else
                                    return 'Normal';
                            }
                            //'value' => 'fkUser.userVip'
                        ],
                        [
                            'attribute' => 'Booking_ID',
                            'value' => 'pkUserRestaurantBookingID'
                        ],
                        [
                            'attribute' => 'Date of Booking',
                            'value' => 'date',
                            'format' => ['date', 'php:d-M-Y']
                        ],
                        [
                            'attribute' => 'Name of the Person',
                            'value' => 'fkUser.userName',
                        ],
                        [
                            'attribute' => 'Restaurant',
                            'value' => function($dataProvider){
                                $locationName = RestaurantLocation::findOne(['pkRestaurantLoactionID' => $dataProvider->fkRestaurant->restaurantLocationID]);
                                return $dataProvider->fkRestaurant->restaurantName.' ('.$locationName->locationName.')';
                            },
                        ],
                        [
                            'attribute' => 'waiting Time',
                            'format' => 'raw',
                            'value' => function($dataProvider){
                                $time1 = $dataProvider->timestamp;
                                $time2 = time();
                                $timeInterval  = abs($time1 - $time2);
                                $minutes   = round($timeInterval / 60);
                                $bookingID = $dataProvider->pkUserRestaurantBookingID;
                                
                                if($minutes < $dataProvider->waitingTime){
                                    return '<span id="span_'.$bookingID.'">'.$dataProvider->waitingTime.'</span>
                                    <input style="display:none; width:40%;" id="input_'.$bookingID.'" type="text" name="waitingTime" value="'.$dataProvider->waitingTime.'">'.
                                    'Min <button id="edit_time_'.$bookingID.'" class="edit-value" bookingid="'.$bookingID.'"></button>
                                    <button style="display:none;" id="update_time_'.$bookingID.'" class="update-value" bookingid="'.$bookingID.'"></button>';
                                }
                                else{
            
                                    return '<span id="span_'.$bookingID.'">'.$dataProvider->waitingTime.' Min</span>';
                                }
                                
                                },
                        ],
                        [
                            'attribute' => 'Phone No',
                            'value' => 'fkUser.userNumber',
                        ],
                        [
                            'attribute' => 'Email',
                            'value' => 'fkUser.userEmail',
                        ],
                       // ['class' => 'yii\grid\ActionColumn'],
                        ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
                    ],
                ]);
                ?>
</div>
</div>

<div class="coptyright">BuzzQ © 2016. All Rights Reserved.</div>
    </div>
</div>
