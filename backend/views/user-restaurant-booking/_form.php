<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Users;
use backend\models\Restaurant;

/* @var $this yii\web\View */
/* @var $model backend\models\UserRestaurantBooking */
/* @var $form yii\widgets\ActiveForm */
$currentuserId = Yii::$app->user->getId();
$restaurants = Restaurant::find()->joinWith(['restaurantCity','restaurantLocation','userRestaurantManages'])->where(['user_restaurant_manage.fkUserRestaurantOwnerID' => $currentuserId])->all();
//echo '<pre>'; print_r($restaurants); echo '</pre>'; die;
?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">.
<h1><?php echo $this->title;?></h1>
              <div class="content-area restaurant">
                  <div class="row">
                      

                      <div class="col-lg-10 col-md-10">
                          <form class="booking_details" method="post">
                          <?php $form = ActiveForm::begin(); ?>
                              <div class="fieldBox name">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <input placeholder="Select User" type="text" disabled>
                                        </div>
                                        <div class="col-xs-4">
                                        <select name="userId">
                                        <option>--Select User--</option>
                                        <?php $users = Users::find()->all();
                                        
                                        if($users)
                                        { 
                                            foreach($users as $user){
                                            ?>
                                        <option value="<?php if(@$user){ echo $user->pkUserID;} ?>"><?php echo $user->userName.'('.$user->userNumber.')'; ?></option>
                                        <?php
                                        }
                                        }
                                        ?>
                                        </select>
                                        </div>
                                    </div>
                              </div>

                              <!-- <div class="fieldBox date-book">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="styled-select slate">
                                            <select disabled>
                                                <option>Date of booking</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <span>09-Aug-2016</span>
                                    </div>
                                </div>
                              </div> -->

                              <div class="fieldBox restaurant-name">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <input placeholder="Restaurant" type="text" disabled>
                                    </div>
                                    <div class="col-xs-4">
                                        <select name="restaurantId"  id="restaurantNames">
                                        <option>--Select Restaurant--</option>                                        
                                       <?php
                                        if($restaurants)
                                        { 
                                            foreach($restaurants as $restaurant){
                                            ?>
                                        <option value="<?php if(@$restaurant){ echo $restaurant->pkRestaurantID;} ?>"><?php echo $restaurant->restaurantName.' ('.$restaurant->restaurantCity->cityName.', '.$restaurant->restaurantLocation->locationName.')'; ?></option>
                                        <?php
                                        }
                                        }
                                        ?>
                                        </select>
                                    </div>
                                </div>
                              </div>

                              <!-- <div class="fieldBox email">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <input placeholder="Email" type="text" disabled>
                                    </div>
                                    <div class="col-xs-4">
                                        <span>info@gmail.com</span>
                                    </div>
                                </div>
                              </div> -->

                              <!-- <div class="fieldBox phone">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <input placeholder="Phone No" type="text" disabled>
                                    </div>
                                    <div class="col-xs-4">
                                        <span>983937893</span>
                                    </div>
                                </div>
                              </div> -->

                              <!-- <div class="fieldBox time">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="styled-select slate">
                                            <select disabled>
                                                <option>Time</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <span>09:30 PM</span>
                                    </div>
                                </div>
                              </div> -->
                              
                              <div class="fieldBox time">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="styled-select slate">
                                            <select disabled>
                                                <option>Waiting Time</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <span>
                                        <?= $form->field($model, 'waitingTime')->textInput()->input('text',['id' => 'restaurantWaiting'])->label(false); ?>
                                        <!-- <input type="text" name="waitingTime" id="restaurantWaiting"> --></span>
                                    </div>
                                    
                                </div>
                              </div>
                              <div class="fieldBox current-wait">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="styled-select slate">
                                            <select disabled>
                                                <option>Number of Seats</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <span>
                                    <?= $form->field($model, 'userNumberSeats')->textInput()->input('text')->label(false); ?>
                                    </span>
                                    </div>
                                    
                                </div>
                              </div>

                              <!-- <div class="fieldBox rewards">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <input placeholder="Rewards" type="text" disabled>
                                    </div>
                                    <div class="col-xs-6">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                                    </div>
                                    <div class="col-xs-2 text-right">
                                        <a href="#"><img src="common/images/edit-icon.png" alt="EDIT"/></a>
                                    </div>
                                </div>
                              </div> -->
                              <div class="row">
                                <div class="btnBox col-lg-3 pull-right">
                                    <input class="btn" value="<?php if($model->isNewRecord){echo 'Submit';}else{echo 'Update';}?>" id="restaurantSubmit" type="submit">
                                </div>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
               <div class="coptyright">BuzzQ © 2016. All Rights Reserved.</div>
        </div>
    <?php ActiveForm::end(); ?>
