<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\UserRestaurantBooking */

$this->title = 'View Booking';
$this->params['breadcrumbs'][] = ['label' => 'User Restaurant Bookings', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
<div class="content-area restaurant">
<div class="row">
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <p>
        <?= Html::a('Delete', ['delete', 'id' => $model->pkUserRestaurantBookingID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p> 
    <?php //echo '<pre>'; print_r($model); echo '</pre>'; 
    $bookinId = (isset($model->pkUserRestaurantBookingID)) ? $model->pkUserRestaurantBookingID : '';
    $date = (isset($model->date)) ? $model->date : '';
    $userName = (isset($model->fkUser->userName)) ? $model->fkUser->userName : '';
    $userEmail = (isset($model->fkUser->userEmail)) ? $model->fkUser->userEmail : '';
    $userNumber = (isset($model->fkUser->userNumber)) ? $model->fkUser->userNumber : '';
    $UserTime = (isset($model->timestamp)) ? $model->timestamp : '';
    if($UserTime)
    {
        $time = date('h:i A', $UserTime);
    }
    else
    {
        $time = NULL;
    }
    
    $waitingTime = (isset($model->waitingTime)) ? $model->waitingTime : '';
    $userPicture = (isset($model->fkUser->userPicture)) ? $model->fkUser->userPicture : '';
    $userId = (isset($model->fkUser->pkUserID)) ? $model->fkUser->pkUserID : '';
    $isVip = (isset($model->fkUser->userVip)) ? $model->fkUser->userVip : '0';
    ?>
                    
                      <div class="col-lg-2 col-md-2">
                          <div class="pic">
                              <img id="image_upload_preview" src="<?php if(@$userPicture){echo $userPicture;}else{ echo '../images/dummy.png';} ?>" alt="Pic"/>
                                <div class="checkbox_box">
                                    <input type="checkbox" id="markVip" <?php if(@$isVip == '1'){echo 'checked';} ?> uid ="<?php echo $userId;?>" />
                                    <label for="markVip">Mark as VIP</label>              
                                </div>
                          </div>
                      </div>

                      <div class="col-lg-10 col-md-10">
                          <form class="booking_details">
                              <div class="fieldBox booking-id">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <input placeholder="Booking ID" type="text" disabled>
                                        </div>
                                        <div class="col-xs-4">
                                            <span><?php echo $bookinId;?></span>
                                        </div>
                                    </div>
                              </div>

                              <div class="fieldBox date-book">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="styled-select slate">
                                            <select disabled>
                                                <option>Date of booking</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <span><?php echo $date; ?></span>
                                    </div>
                                </div>
                              </div>

                              <div class="fieldBox name">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <input placeholder="Name of the Person" type="text" disabled>
                                    </div>
                                    <div class="col-xs-4">
                                        <span><?php echo $userName; ?></span>
                                    </div>
                                </div>
                              </div>

                              <div class="fieldBox email">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <input placeholder="Email" type="text" disabled>
                                    </div>
                                    <div class="col-xs-4">
                                        <span><?php echo $userEmail; ?></span>
                                    </div>
                                </div>
                              </div>

                              <div class="fieldBox phone">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <input placeholder="Phone No" type="text" disabled>
                                    </div>
                                    <div class="col-xs-4">
                                        <span><?php echo $userNumber; ?></span>
                                    </div>
                                </div>
                              </div>

                              <div class="fieldBox time">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="styled-select slate">
                                            <select disabled>
                                                <option>Time</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <span><?php echo $time; ?></span>
                                    </div>
                                </div>
                              </div>
                              
                              <div class="fieldBox time">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="styled-select slate">
                                            <select disabled>
                                                <option>Waiting Time</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <span><?php echo $waitingTime.' Min'; ?></span>
                                    </div>
                                    <!-- <div class="col-xs-2 text-right">
                                        <a href="#"><img src="../images/edit-icon.png" alt="EDIT"/></a>
                                    </div> -->
                                </div>
                              </div>

                              <!-- <div class="fieldBox rewards">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <input placeholder="Rewards" type="text" disabled>
                                    </div>
                                    <div class="col-xs-6">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                                    </div>
                                    <div class="col-xs-2 text-right">
                                        <a href="#"><img src="common/images/edit-icon.png" alt="EDIT"/></a>
                                    </div>
                                </div>
                              </div> -->
                          </form>
                      </div>
                  </div>   
</div>
</div>
