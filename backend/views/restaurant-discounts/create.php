<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\RestaurantDiscounts */

$this->title = 'Create Store';
//$this->params['breadcrumbs'][] = ['label' => 'Restaurant Discounts', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>