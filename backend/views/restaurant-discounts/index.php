<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\RestaurantDiscountsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Stores & Offers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
<div class="total-bookigs">
    <?= Html::a('+ Add New', ['create'], ['class' => 'action-addNew']) ?>

     <div class="booking-list">
        <div class="table-responsive">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'Store ID',
                'value' => 'pkRestaurantDiscountsID'
        
            ],
            [
                'attribute' => 'Name of the Store',
                'value' => 'storeName'
        
            ],
            [
                'attribute' => 'Restaurant Name',
                'value' => 'fkRestaurant.restaurantName'
        
            ],
            [
                'attribute' => 'Discount %',
                'value' => function($dataProvider){
                    return $dataProvider->discount.'%';
                }
        
            ],
            [
                'attribute' => 'Points',
                'value' => 'pointsEarned'
        
            ],
            [
                'attribute' => 'Location',
                'value' => 'storeAddress'
        
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div>

<div class="coptyright">BuzzQ © 2016. All Rights Reserved.</div>
    </div>
</div>
