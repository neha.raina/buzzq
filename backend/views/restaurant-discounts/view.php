<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\RestaurantDiscounts */

$this->title = 'View Store';
$this->params['breadcrumbs'][] = ['label' => 'Restaurant Discounts', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
              <div class="content-area restaurant storeDetail">
                  <div class="row">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->pkRestaurantDiscountsID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->pkRestaurantDiscountsID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
<?php //echo '<pre>'; print_r($model); echo '</pre>';
$pkRestaurantDiscountsID = (isset($model->pkRestaurantDiscountsID)) ? $model->pkRestaurantDiscountsID : '';
$storeName = (isset($model->storeName)) ? $model->storeName : '';
$discount = (isset($model->discount)) ? $model->discount : '';
$discountImage = (isset($model->discountImage)) ? $model->discountImage : '';
$pointsEarned = (isset($model->pointsEarned)) ? $model->pointsEarned : '';
$storeAddress = (isset($model->storeAddress)) ? $model->storeAddress : '';
$restaurantName = (isset($model->fkRestaurant->restaurantName)) ? $model->fkRestaurant->restaurantName : '';
?>
    <div class="col-lg-2 col-md-2">
                          <div class="pic">
                              <img id="image_upload_preview" src="<?php if(@$discountImage){echo $discountImage;}else{ echo '../images/dummy.png';} ?>" alt="Pic">
                          </div>
                      </div>

                      <div class="col-lg-10 col-md-10">
                          <form class="">
                              <div class="fieldBox store-id">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <input placeholder="Store ID" type="text" disabled>
                                    </div>
                                    <div class="col-xs-4">
                                        <span><?php echo $pkRestaurantDiscountsID;?></span>
                                    </div>
                                </div>
                              </div>

                              <div class="fieldBox store-name">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <input placeholder="Store Name" type="text" disabled>
                                    </div>
                                    <div class="col-xs-4">
                                        <span><?php echo $storeName;?></span>
                                    </div>
                                </div>
                              </div>
                              <div class="fieldBox restaurant-name">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <input placeholder="Restaurant Name" type="text" disabled>
                                    </div>
                                    <div class="col-xs-4">
                                        <span><?php echo $restaurantName;?></span>
                                    </div>
                                </div>
                              </div>
                              
                              <div class="fieldBox discount">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <input placeholder="Discount (%)" type="text" disabled>
                                    </div>
                                    <div class="col-xs-4">
                                        <span><?php echo $discount;?>%</span>
                                    </div>
                                </div>
                              </div>

                              <div class="fieldBox discount">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <input placeholder="Points" type="text" disabled>
                                    </div>
                                    <div class="col-xs-4">
                                        <span><?php echo $pointsEarned;?></span>
                                    </div>
                                </div>
                              </div>

                              <div class="fieldBox location">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <input placeholder="Location" type="text" disabled>
                                    </div>
                                    <div class="col-xs-4">
                                        <p><?php echo $storeAddress;?></p>
                                    </div>
                                </div>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
               <div class="coptyright">BuzzQ © 2016. All Rights Reserved.</div>
        </div>
      