<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\RestaurantDiscounts */

$this->title = 'Update Store';
//$this->params['breadcrumbs'][] = ['label' => 'Restaurant Discounts', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->pkRestaurantDiscountsID, 'url' => ['view', 'id' => $model->pkRestaurantDiscountsID]];
//$this->params['breadcrumbs'][] = 'Update';
?>
<div class="restaurant-discounts-update">

      <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
