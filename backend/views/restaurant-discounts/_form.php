<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Restaurant;

/* @var $this yii\web\View */
/* @var $model backend\models\RestaurantDiscounts */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
$discountImage = (isset($model->discountImage)) ? $model->discountImage : '';
?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
<h1><?php echo $this->title;?></h1>
 <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
              <div class="content-area restaurant">
                  <div class="row">
                      <div class="col-lg-2 col-md-2">
                          <div class="pic">
                          <?php if(@$discountImage)
                          { ?>
                          <img id="image_upload_preview" src="<?php echo $discountImage;?>" alt="Pic">
                           <?php }else
                          { ?>
                          <img id="image_upload_preview" src="../images/restaurant-placeholder.jpg" alt="Pic">
                          <?php }
                          ?>
                              <div class="edit upload">
                                <div class="browse-file">
                                       <input id="inputFile" type="file" name="storeImage"/></div>
                                </div>
                          </div>
                      </div>

                      <div class="col-lg-10 col-md-10">
                          <form class="">
                              <div class="fieldBox store-name">
                                  <?= $form->field($model, 'storeName')->textInput()->input('text', ['placeholder' => "Store Name"])->label(false); ?>
                              </div>

                              <div class="fieldBox restaurant-name">
                                 <?php
                                $currUserId = Yii::$app->user->getId();
                                $currentuserId = isset($currUserId) ? $currUserId : '0';
                                $items = ArrayHelper::map(Restaurant::find()->joinWith(['userRestaurantManages'])
                                ->where(['user_restaurant_manage.fkUserRestaurantOwnerID' => $currentuserId])->all(), 'pkRestaurantID', 'restaurantName');
                              
                                echo $form->field($model, 'fkRestaurantID')->dropdownList($items,
                                ['prompt'=>'Select Restaurant'])->label(false);     
                                ?>    
                              </div>

                              <div class="fieldBox discount"> 
                                <?= $form->field($model, 'discount')->textInput()->input('text', ['placeholder' => "Discount (%)"])->label(false); ?>
                              </div>

                              <div class="fieldBox discount">
                                <?= $form->field($model, 'pointsEarned')->textInput()->input('text', ['placeholder' => "Points"])->label(false); ?>
                              </div>

                              <div class="fieldBox location">
                                <?= $form->field($model, 'storeAddress')->textInput()->input('text', ['placeholder' => "Location"])->label(false); ?>
                              </div>

                              <div class="row">
                                <div class="btnBox col-lg-3 pull-right">
                                    <input class="btn" value="Submit" type="submit">
                                </div>
                              </div>
                          <?php ActiveForm::end(); ?>
                      </div>
                  </div>
              </div>
               <div class="coptyright">BuzzQ © 2016. All Rights Reserved.</div>
        </div>
