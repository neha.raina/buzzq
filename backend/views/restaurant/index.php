<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\RestaurantSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Restaurants');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    

    <div class="total-bookigs">

                  <?= Html::a('+ Add New', ['create'], ['class' => 'action-addNew']) ?>
                  <div class="booking-list">

                          <div class="table-responsive">
    
<?php //echo '<pre>'; print_r(Yii::$app->user->getId()); echo '</pre>';?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                            'attribute' => 'Restaurant ID',
                            'value' => 'pkRestaurantID'
        
            ],
            [
                            'attribute' => 'Restaurant Name',
                            'value' => 'restaurantName'
            ],
            [
                            'attribute' => 'Description',
                            'value' => 'restaurantDescription'
            ],
            [
                            'attribute' => 'City',
                            'value' => 'restaurantCity.cityName',
            ],
            [
                            'attribute' => 'Location',
                            'value' => 'restaurantLocation.locationName'
            ],
            [
                            'attribute' => 'Timings',
                            'value' => 'restaurantTimings'
            ],
            [
                            'attribute' => 'Waiting Time',
                            'value' => 'restaurantWaitingTime'
            ],
            [
                            'attribute' => 'Location',
                            'value' => 'restaurantLocation.locationName'
            ],
            //'pkRestaurantID',
            //'restaurantQRCode',
            //'restaurantName',
            //'restaurantDescription',
            //'restaurantCityID',
            // 'restaurantTimings',
            // 'restaurantType',
            // 'restaurantLogo',
            // 'restaurantLink',
            // 'restaurantLocationID',
            // 'restaurantAddress',
            // 'restaurantLatitude',
            // 'restaurantLongitude',
            // 'restaurantWaitingTime:datetime',
            // 'restaurantStatus',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div>

<div class="coptyright">BuzzQ © 2016. All Rights Reserved.</div>
    </div>
</div>