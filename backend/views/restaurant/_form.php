<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\RestaurantCity;
use backend\models\RestaurantLocation;

/* @var $this yii\web\View */
/* @var $model backend\models\Restaurant */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
//echo '<pre>'; print_r($model); echo '</pre>'; die;
$restaurantCityID = (isset($model->restaurantCityID)) ? $model->restaurantCityID : '';
$restaurantLocationID = (isset($model->restaurantLocationID)) ? $model->restaurantLocationID : '';
$restaurantLogo = (isset($model->restaurantLogo)) ? $model->restaurantLogo : '';
$restaurantMenus = (isset($model->restaurantMenus)) ? $model->restaurantMenus : '';
?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
<h1><?php echo $this->title;?></h1>
             <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
              <div class="content-area restaurant">
                  <div class="row">
                      <div class="col-lg-2 col-md-2">
                          <div class="pic">
                          <?php if(@$restaurantLogo)
                          { ?>
                          <img id="image_upload_preview" src="<?php echo $restaurantLogo;?>" alt="Pic">
                           <?php }else
                          { ?>
                          <img id="image_upload_preview" src="../images/restaurant-placeholder.jpg" alt="Pic">
                          <?php }
                          ?>
                              
                              <div class="edit upload">
                                <div class="browse-file">
                                        <input id="inputFile" type="file" name="restaurantLogo"/>
                                </div>
                                </div>
                          </div>
                      </div>

                      <div class="col-lg-10 col-md-10">
                              <div class="fieldBox restaurant-name">
                                 <?= $form->field($model, 'restaurantName')->textInput()->input('text', ['placeholder' => "Restaurant Name"])->label(false); ?>
                              </div>
                              <div class="fieldBox reservation-box">
                                  <?= $form->field($model, 'restaurantDescription')->textarea(['rows' => '4','placeholder' => "Restaurant Description"])->label(false); ?>
                              </div>
                              <div class="fieldBox time">
                                  <?= $form->field($model, 'restaurantTimings')->textInput()->input('text', ['placeholder' => "Restaurant Timings (00:00 AM to 00:00 PM)"])->label(false); ?>
                              </div>
                              <div class="fieldBox location">
                                  <?= $form->field($model, 'restaurantAddress')->textarea(['rows' => '4','placeholder' => "Restaurant Address"])->label(false); ?>
                              </div>
                              <div class="fieldBox location">
                                    <?php $restaurantCity = RestaurantCity::find()->groupBy('cityName')->all(); ?>
                                    <select name="restaurantCityID" id="restaurant-restaurantcityid">
                                    <option value=""> --Select City-- </option>
                                    <?php 
                                    if($restaurantCity){
                                    foreach($restaurantCity as $restaurantCities){ ?>
                                    <?php 
                                    if(@$restaurantCityID){
                                      if($restaurantCities->pkRestaurantCityID == $restaurantCityID){$select = 'yes';}else{$select = NULL;}
                                      }?>
                                    <option value="<?php if(@$restaurantCities->pkRestaurantCityID){ echo $restaurantCities->pkRestaurantCityID; } ?>" <?php if(@$select){ echo 'selected';}?>><?php if(@$restaurantCities->cityName){ echo $restaurantCities->cityName; } ?></option>
                                   <?php }} ?>
                                   <option value="other">Other</option>
                                    </select>
                                   <input placeholder="Enter City Name" style="display:none;" id="restaurantOtherCity" type="text" name="restaurantOtherCity">
                              </div>
                              <?php
                              if(@$restaurantLocationID)
                              {
                                $RestaurantLocation = RestaurantLocation::find()->where(['fkRestaurantCityID' => $restaurantCityID])->all();
                              }
                              ?>
                              <div class="fieldBox location">
                                  <!-- <input placeholder="Location" type="text"> -->
                                  <select id="locationID" name="restaurantLocationID">
                                 <?php if(@$RestaurantLocation)
                                  {
                                    foreach($RestaurantLocation as $RestaurantLocations)
                                    { 
                                    ?>
                                    <option value="<?php echo $RestaurantLocations->pkRestaurantLoactionID;?>" <?php if($RestaurantLocations->pkRestaurantLoactionID == $restaurantLocationID){echo 'selected';} ?> ><?php echo $RestaurantLocations->locationName; ?></option>
                                   <?php
                                    }
                                  }
                                  ?>
                                  </select>
                                  <input placeholder="Enter Location Name" style="display:none;" id="restaurantOtherLocation" type="text" name="restaurantOtherLocation">

                              </div>
                              <div class="fieldBox time">
                                  <?= $form->field($model, 'restaurantWaitingTime')->textInput()->input('text', ['placeholder' => "Waiting Time"])->label(false); ?>
                              </div>
                              <?php if(@$restaurantMenus){ ?>
                              <div class="fieldBox rest-menu">
                            <div class="row">
                                <div class="col-xs-4">
                                    <input placeholder="Menu" type="text" disabled>
                                </div>
                                <div class="col-xs-8">
                                <?php
                                foreach($restaurantMenus as $restaurantMenu)
                                {
                                    $pkRestaurantMenuID = $restaurantMenu->pkRestaurantMenuID;
                                    $resturantMenuName = $restaurantMenu->menuName;
                                    $resturantMenuimage = $restaurantMenu->restaurantPicture;
                                ?>
                            <div class="menu_item_main" id="divMenu_<?php echo $pkRestaurantMenuID;?>">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <img src="<?php echo $resturantMenuimage;?>" alt="img"/>
                                    </div>
                                    <div class="col-xs-5">
                                        <input type="text" id="menuName_<?php echo $pkRestaurantMenuID;?>" value="<?php if($resturantMenuName){ echo $resturantMenuName;}?>"></p>
                                    </div>
                                    <div class="col-xs-2">
                                    <a href="javascript:void(0)" class="updateMenu" itemid="<?php echo $pkRestaurantMenuID;?>">Update</a>
                                    </div>
                                    <div class="col-xs-2">
                                    <a href="javascript:void(0)" class="deleteMenu" itemid="<?php echo $pkRestaurantMenuID;?>"><img src="../images/item-close.png" alt="img"></a>
                                    </div>
                                </div>
                            </div>
                                <?php }
                                ?>
                                </div>
                            </div>
                          </div>  
                          <?php }?>
                              <div class="fieldBox menu">
                                  <input placeholder="Add new menu" name="restaurantMenuName" type="text">
                                  <div class="upoad-image">
                                    <div class="upload">
                                      <a href="javascript:void(0);">Upload Image </a><input id="inputFile2" type="file" name="menuImages[]" multiple/>
                                  </div>
                                  </div>
                              </div>

                              <div class="row">
                                <div class="btnBox col-lg-3 pull-right">
                                    <input class="btn" value="<?php if($model->isNewRecord){echo 'Submit';}else{echo 'Update';}?>" id="restaurantSubmit" type="submit">
                                </div>
                              </div>
                      </div>
                  </div>
    <?php ActiveForm::end(); ?>
  </div>
        <div class="coptyright">BuzzQ © 2016. All Rights Reserved.</div>
  </div>
