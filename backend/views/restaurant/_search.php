<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\RestaurantSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="content-area search-bookig">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

     <div class="row">

                <div class="col-lg-4 col-sm-4">
                  <div class="fieldBox restaurant-id">
        <?php
        echo $form->field($model, 'pkRestaurantID')->textInput()->input('text', ['placeholder' => "Restaurant ID"])->label(false);
        ?>
                  </div>
                </div>   

                <div class="col-lg-4 col-sm-4">
                  <div class="fieldBox restaurant-name">
        <?php
        echo $form->field($model, 'restaurantName')->textInput()->input('text', ['placeholder' => "Restaurant Name"])->label(false);
        ?>
                  </div>   
                </div>

                <div class="col-lg-4 col-sm-4">
                    <div class="fieldBox location">
        <?php
        echo $form->field($model, 'locationName')->textInput()->input('text', ['placeholder' => "Location Name"])->label(false);
        ?>
                    </div>
                </div> 

                <div class="col-lg-4 col-sm-4 pull-right">
                    <div class="btnBox">
                        <input class="btn" value="Search" type="submit">
                    </div> 
                </div>    
    </div>
    <?php ActiveForm::end(); ?>

</div>
