<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Restaurant */

$this->title = Yii::t('app', 'Update Restaurant');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Restaurants'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->pkRestaurantID, 'url' => ['view', 'id' => $model->pkRestaurantID]];
//$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="restaurant-update">
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
