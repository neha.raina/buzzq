<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\UserRestaurantBooking;

/* @var $this yii\web\View */
/* @var $model backend\models\Restaurant */

$this->title = 'View Restaurant';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Restaurants'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-sm-9 col-sm-offset-3 col-md-9 col-md-offset-2 main">
            <div class="content-area search-bookig">
                <div class="row">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->pkRestaurantID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->pkRestaurantID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this restaurant?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php 
    //echo '<pre>'; print_r($model); echo '</pre>'; die; 
    $pkRestaurantID = (isset($model->pkRestaurantID)) ? $model->pkRestaurantID : '';
    $restaurantQRCode = (isset($model->restaurantQRCode)) ? $model->restaurantQRCode : '';
    $restaurantName = (isset($model->restaurantName)) ? $model->restaurantName : '';
    $restaurantDescription = (isset($model->restaurantDescription)) ? $model->restaurantDescription : '';
    $restaurantTimings = (isset($model->restaurantTimings)) ? $model->restaurantTimings : '';
    $restaurantLogo = (isset($model->restaurantLogo)) ? $model->restaurantLogo : '';
    $restaurantAddress = (isset($model->restaurantAddress)) ? $model->restaurantAddress : '';
    $restaurantWaitingTime = (isset($model->restaurantWaitingTime)) ? $model->restaurantWaitingTime : '';
    $restaurantCity = (isset($model->restaurantCity->cityName)) ? $model->restaurantCity->cityName : '';
    $restaurantLocation = (isset($model->restaurantLocation->locationName)) ? $model->restaurantLocation->locationName : '';
    $restaurantMenus = (isset($model->restaurantMenus)) ? $model->restaurantMenus : '';    
    $totalBooking = UserRestaurantBooking::find()->where(['fkRestaurantID' => $pkRestaurantID])->count();
    //die;
    ?>
        <div class="col-lg-2 col-md-2">
            <div class="pic">
                <img id="image_upload_preview" src="<?php if(@$restaurantLogo){echo $restaurantLogo;}else{echo '../images/no-image-icon.jpg';}?>" alt=""/>
            </div>
        </div>
        <div class="col-xs-9">
            <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#detail" role="tab" data-toggle="tab">Detail</a></li>
            <li role="presentation"><a href="#photos" role="tab" data-toggle="tab">Menu Photos</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="detail">
                    <form class="booking_details">
                        <div class="fieldBox restaurant-id">
                            <div class="row">
                                <div class="col-xs-4">
                                    <input placeholder="Restaurant ID" type="text" disabled>
                                </div>
                                <div class="col-xs-4">
                                    <span><?php echo $pkRestaurantID;?></span>
                                </div>
                            </div>
                        </div>
                        <div class="fieldBox restaurant-name">
                            <div class="row">
                                <div class="col-xs-4">
                                    <input placeholder="Restaurant Name" type="text" disabled>
                                </div>
                                <div class="col-xs-4">
                                    <span><?php echo $restaurantName;?></span>
                                </div>
                            </div>
                        </div>
                        <div class="fieldBox restaurant-name">
                            <div class="row">
                                <div class="col-xs-4">
                                    <input placeholder="Description" type="text" disabled>
                                </div>
                                <div class="col-xs-8">
                                    <span><?php echo $restaurantDescription;?></span>
                                </div>
                            </div>
                        </div>
                        <div class="fieldBox time">
                            <div class="row">
                                <div class="col-xs-4">
                                    <input placeholder="Timings" type="text" disabled>
                                </div>
                                <div class="col-xs-4">
                                    <span><?php echo $restaurantTimings;?></span>
                                </div>
                            </div>
                        </div>
                        <div class="fieldBox location">
                            <div class="row">
                                <div class="col-xs-4">
                                    <input placeholder="Address" type="text" disabled>
                                </div>
                                <div class="col-xs-8">
                                    <span><?php echo $restaurantAddress;?></span>
                                </div>
                            </div>
                        </div>
                        <div class="fieldBox location">
                            <div class="row">
                                <div class="col-xs-4">
                                    <input placeholder="City" type="text" disabled>
                                </div>
                                <div class="col-xs-8">
                                    <span><?php echo $restaurantCity;?></span>
                                </div>
                            </div>
                        </div>
                        <div class="fieldBox location">
                            <div class="row">
                                <div class="col-xs-4">
                                    <input placeholder="Location" type="text" disabled>
                                </div>
                                <div class="col-xs-8">
                                    <span><?php echo $restaurantLocation;?></span>
                                </div>
                            </div>
                        </div>
                        <div class="fieldBox rest-menu">
                            <div class="row">
                                <div class="col-xs-4">
                                    <input placeholder="QR Image" type="text" disabled>
                                </div>
                                <div class="col-xs-8">
                                    <div class="menu_item_main last">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=$restaurantQRCode&choe=UTF-8" alt="img"/>
                                            <a download="qrcode.png" href="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=$restaurantQRCode&choe=UTF-8" title="ImageName">Download</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fieldBox rest-menu">
                            <div class="row">
                                <div class="col-xs-4">
                                    <input placeholder="Menu" type="text" disabled>
                                </div>
                                <div class="col-xs-8">
                                <?php
                                foreach($restaurantMenus as $restaurantMenu)
                                { 
                                    $resturantMenuName = $restaurantMenu->menuName;
                                    $resturantMenuimage = $restaurantMenu->restaurantPicture;
                                ?>
                            <div class="menu_item_main">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <img src="<?php echo $resturantMenuimage;?>" alt="img"/>
                                                        </div>
                                                        <div class="col-xs-7">
                                                            <p><?php if($resturantMenuName){ echo $resturantMenuName;}?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                    
                                <?php }
                                ?>
                                </div>
                            </div>
                        </div>
                        <div class="fieldBox total-booking">
                            <div class="row">
                                <div class="col-xs-4">
                                    <input placeholder="Total No of Booking" type="text" disabled>
                                </div>
                                <div class="col-xs-8">
                                    <span><?php echo $totalBooking;?></span>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="fieldBox current-wait">
                            <div class="row">
                                <div class="col-xs-4">
                                    <input placeholder="Current people Waiting" type="text" disabled>
                                </div>
                                <div class="col-xs-8">
                                    <span>19</span>
                                </div>
                            </div>
                        </div> -->
                        
                        <div class="fieldBox time">
                            <div class="row">
                                <div class="col-xs-4">
                                    <div class="styled-select slate">
                                        <select disabled>
                                            <option>Waiting Time</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <span><?php echo $restaurantWaitingTime.' Mins';?></span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div role="tabpanel" class="tab-pane" id="photos">
                    <div class="row">
                        <?php
                        foreach($restaurantMenus as $restaurantMenu)
                                { 
                                    $resturantMenuimage = $restaurantMenu->restaurantPicture;
                                ?>
                        <div class="col-xs-4">
                            <div class="img-gallery-box">
                                <img src="<?php echo $resturantMenuimage;?>" alt="Restaurant"/>
                            </div>
                        </div>
                        <?php }?>
                    </div>
                </div>
            </div>
            </div>
        </div>
</div>
</div>
</div>
