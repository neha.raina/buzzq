<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Restaurant */

$this->title = Yii::t('app', 'Create Restaurant');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Restaurants'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>