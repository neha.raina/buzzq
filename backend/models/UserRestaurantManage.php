<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user_restaurant_manage".
 *
 * @property string $pkUserRestaurantManageID
 * @property string $fkUserRestaurantOwnerID
 * @property string $fkRestaurantID
 *
 * @property UserRestaurantOwner $fkUserRestaurantOwner
 * @property Restaurant $fkRestaurant
 */
class UserRestaurantManage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_restaurant_manage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkUserRestaurantOwnerID', 'fkRestaurantID'], 'integer'],
            [['fkUserRestaurantOwnerID'], 'exist', 'skipOnError' => true, 'targetClass' => UserRestaurantOwner::className(), 'targetAttribute' => ['fkUserRestaurantOwnerID' => 'pkUserRestaurantOwnerID']],
            [['fkRestaurantID'], 'exist', 'skipOnError' => true, 'targetClass' => Restaurant::className(), 'targetAttribute' => ['fkRestaurantID' => 'pkRestaurantID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkUserRestaurantManageID' => 'Pk User Restaurant Manage ID',
            'fkUserRestaurantOwnerID' => 'Fk User Restaurant Owner ID',
            'fkRestaurantID' => 'Fk Restaurant ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkUserRestaurantOwner()
    {
        return $this->hasOne(UserRestaurantOwner::className(), ['pkUserRestaurantOwnerID' => 'fkUserRestaurantOwnerID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkRestaurant()
    {
        return $this->hasOne(Restaurant::className(), ['pkRestaurantID' => 'fkRestaurantID']);
    }
}
