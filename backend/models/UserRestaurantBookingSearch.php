<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\UserRestaurantBooking;
use backend\models\UserRestaurantManage;

/**
 * UserRestaurantBookingSearch represents the model behind the search form about `backend\models\UserRestaurantBooking`.
 */
class UserRestaurantBookingSearch extends UserRestaurantBooking
{
    /**
     * @inheritdoc
     */
    public $userName;
    public $userNumber;
    public $userVip;
    public $fromDate;
    public $toDate;

    public function rules()
    {
        return [
            [['pkUserRestaurantBookingID', 'fkRestaurantID', 'fkUserID', 'userNumberSeats', 'waitingTime', 'buzzInTime', 'userCancelBookong'], 'integer'],
            [['userRestaurantQRCode', 'date','userName','userNumber','userVip', 'timestamp','fromDate','toDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserRestaurantBooking::find()->joinWith('fkUser')
        ->joinWith('fkRestaurant');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);

        if(!$this->validate()){
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $currUserId = Yii::$app->user->getId();
        $currentuserId = isset($currUserId) ? $currUserId : '0';
        $restList = UserRestaurantManage::find()->where(['fkUserRestaurantOwnerID' => $currentuserId])->all();
        if(@$restList){
            foreach($restList as $restLists){
                $restaurantArray[] = $restLists->fkRestaurantID;
            }
        }else{
            $restaurantArray = NULL;
        }
        // grid filtering conditions
            
        $query->andFilterWhere([
            'pkUserRestaurantBookingID' => $this->pkUserRestaurantBookingID,
            'users.userVip' => $this->userVip,
            //'fkRestaurantID' => $this->fkRestaurantID,
            //'userNumberSeats' => $this->userNumberSeats,
            'waitingTime' => $this->waitingTime,
            //'buzzInTime' => $this->buzzInTime,
            //'date' => $this->date,
            //'userCancelBookong' => $this->userCancelBookong,
        ]);
        if(@$this->fkRestaurantID){

            $query->andFilterWhere(['fkRestaurantID'=> $this->fkRestaurantID]);
        }else{
            $query->where(['fkRestaurantID'=> $restaurantArray]);
        }
        $query->andFilterWhere(['like','users.userName',$this->userName]);
        $query->andFilterWhere(['like','users.userNumber',$this->userNumber]);
        //$query->andFilterWhere(['like', 'userRestaurantQRCode', $this->userRestaurantQRCode]);

        return $dataProvider;
    }


     public function searchTransaction($params)
    {

        $query = UserRestaurantBooking::find()->joinWith('fkUser')
        ->joinWith('fkRestaurant');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);

        if(!$this->validate()){
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $currUserId = Yii::$app->user->getId();
        $currentuserId = isset($currUserId) ? $currUserId : '0';
        $restList = UserRestaurantManage::find()->where(['fkUserRestaurantOwnerID' => $currentuserId])->all();
        if(@$restList){
            foreach($restList as $restLists){
                $restaurantArray[] = $restLists->fkRestaurantID;
            }
        }else{
            $restaurantArray = NULL;
        }
        // grid filtering conditions
        //echo '<pre>';print_r($this);echo '</pre>'; 
        //die;
        $query->andFilterWhere([
            'pkUserRestaurantBookingID' => $this->pkUserRestaurantBookingID,
            'users.userVip' => $this->userVip,
            //'fkRestaurantID' => $this->fkRestaurantID,
            //'userNumberSeats' => $this->userNumberSeats,
            'waitingTime' => $this->waitingTime,
            //'buzzInTime' => $this->buzzInTime,
            //'date' => $this->date,
            //'userCancelBookong' => $this->userCancelBookong,
        ]);
        if(@$this->fkRestaurantID){
            $query->andFilterWhere(['fkRestaurantID'=> $this->fkRestaurantID]);
        }else{
            $query->where(['fkRestaurantID'=> $restaurantArray]);
        }
        
        $query->andFilterWhere(['like','users.userName',$this->userName]);
        $query->andFilterWhere(['like','users.userNumber',$this->userNumber]);
        $query->andFilterWhere(['<=', 'date', $this->toDate]);    
        $query->andFilterWhere(['>=', 'date', $this->fromDate]);
        
        //$query->andFilterWhere(['like', 'userRestaurantQRCode', $this->userRestaurantQRCode]);
        return $dataProvider;
    }


}
