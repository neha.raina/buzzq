<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "restaurant_city".
 *
 * @property string $pkRestaurantCityID
 * @property string $cityName
 *
 * @property Restaurant[] $restaurants
 * @property RestaurantLocation[] $restaurantLocations
 */
class RestaurantCity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restaurant_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cityName'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkRestaurantCityID' => 'Pk Restaurant City ID',
            'cityName' => 'City Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurants()
    {
        return $this->hasMany(Restaurant::className(), ['restaurantCityID' => 'pkRestaurantCityID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantLocations()
    {
        return $this->hasMany(RestaurantLocation::className(), ['fkRestaurantCityID' => 'pkRestaurantCityID']);
    }
}
