<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user_restaurant_owner".
 *
 * @property string $pkUserRestaurantOwnerID
 * @property string $userName
 * @property string $userEmail
 * @property string $userNumber
 * @property string $password_hash
 * @property string $auth_key
 * @property string $userOTP
 * @property string $userSex
 * @property string $userNationality
 * @property string $userPicture
 * @property string $password_reset_token
 * @property integer $fkCountryID
 * @property string $userCity
 * @property string $userAddress
 * @property string $userStatus
 * @property string $created_at
 * @property string $updated_at
 *
 * @property UserRestaurantManage[] $userRestaurantManages
 */
class UserRestaurantOwner extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_restaurant_owner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pkUserRestaurantOwnerID'], 'required'],
            [['pkUserRestaurantOwnerID', 'userNumber', 'fkCountryID', 'created_at', 'updated_at'], 'integer'],
            [['userSex', 'userStatus'], 'string'],
            [['userName'], 'string', 'max' => 50],
            [['userEmail', 'password_hash'], 'string', 'max' => 150],
            [['auth_key'], 'string', 'max' => 100],
            [['userOTP', 'userNationality'], 'string', 'max' => 20],
            [['userPicture'], 'string', 'max' => 1000],
            [['password_reset_token', 'userCity', 'userAddress'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkUserRestaurantOwnerID' => 'Pk User Restaurant Owner ID',
            'userName' => 'User Name',
            'userEmail' => 'User Email',
            'userNumber' => 'User Number',
            'password_hash' => 'Password Hash',
            'auth_key' => 'Auth Key',
            'userOTP' => 'User Otp',
            'userSex' => 'User Sex',
            'userNationality' => 'User Nationality',
            'userPicture' => 'User Picture',
            'password_reset_token' => 'Password Reset Token',
            'fkCountryID' => 'Fk Country ID',
            'userCity' => 'User City',
            'userAddress' => 'User Address',
            'userStatus' => 'User Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRestaurantManages()
    {
        return $this->hasMany(UserRestaurantManage::className(), ['fkUserRestaurantOwnerID' => 'pkUserRestaurantOwnerID']);
    }
}
