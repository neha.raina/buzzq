<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Restaurant;

/**
 * RestaurantSearch represents the model behind the search form about `backend\models\Restaurant`.
 */
class RestaurantSearch extends Restaurant
{
    /**
     * @inheritdoc
     */

    public $locationName;

    public function rules()
    {
        return [
            [['pkRestaurantID', 'restaurantCityID', 'restaurantLocationID', 'restaurantWaitingTime', 'restaurantStatus'], 'integer'],
            [['restaurantQRCode', 'restaurantName', 'restaurantDescription', 'restaurantTimings', 'restaurantType', 'restaurantLogo', 'restaurantLink', 'restaurantAddress', 'restaurantLatitude', 'restaurantLongitude','locationName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        
        $currUserId = Yii::$app->user->getId();
        $currentuserId = isset($currUserId) ? $currUserId : '0';
        $query = Restaurant::find()->joinWith(['restaurantCity','restaurantLocation','userRestaurantManages'])
        ->where(['user_restaurant_manage.fkUserRestaurantOwnerID' => $currentuserId]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pkRestaurantID' => $this->pkRestaurantID,
            //'restaurantCityID' => $this->restaurantCityID,
            //'restaurantLocationID' => $this->restaurantLocationID,
            //'restaurantWaitingTime' => $this->restaurantWaitingTime,
            //'restaurantStatus' => $this->restaurantStatus,
        ]);

            $query
            //->andFilterWhere(['like', 'restaurantQRCode', $this->restaurantQRCode])
            ->andFilterWhere(['like', 'restaurantName', $this->restaurantName])
            ->andFilterWhere(['like', 'restaurant_location.locationName', $this->locationName])
            //->andFilterWhere(['like', 'restaurantDescription', $this->restaurantDescription])
            //->andFilterWhere(['like', 'restaurantTimings', $this->restaurantTimings])
            // ->andFilterWhere(['like', 'restaurantType', $this->restaurantType])
            //->andFilterWhere(['like', 'restaurantLogo', $this->restaurantLogo])
            // ->andFilterWhere(['like', 'restaurantLink', $this->restaurantLink])
            //->andFilterWhere(['like', 'restaurantAddress', $this->restaurantAddress])
            //->andFilterWhere(['like', 'restaurantLatitude', $this->restaurantLatitude])
            //->andFilterWhere(['like', 'restaurantLongitude', $this->restaurantLongitude])
            ;

        return $dataProvider;
    }
}
