<?php

namespace backend\models;

use Yii;
use backend\models\Users;
use backend\models\UserRestaurantManage;

/**
 * This is the model class for table "user_restaurant_booking".
 *
 * @property string $pkUserRestaurantBookingID
 * @property string $fkRestaurantID
 * @property string $fkUserID
 * @property string $userRestaurantQRCode
 * @property integer $userNumberSeats
 * @property integer $waitingTime
 * @property integer $buzzInTime
 * @property string $date
 * @property integer $userCancelBookong
 *
 * @property Users $fkUser
 * @property Restaurant $fkRestaurant
 */
class UserRestaurantBooking extends \yii\db\ActiveRecord
{
    public $bookingId;
    public $timeValue;
    public $userId;
    public $isVip;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_restaurant_booking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkRestaurantID', 'fkUserID', 'userNumberSeats', 'waitingTime', 'buzzInTime', 'userCancelBookong'], 'integer'],
            [['date','timestamp','bookingId', 'timeValue','userId','isVip'], 'safe'],
            [['userRestaurantQRCode'], 'string', 'max' => 100],
            [['fkUserID'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['fkUserID' => 'pkUserID']],
            [['fkRestaurantID'], 'exist', 'skipOnError' => true, 'targetClass' => Restaurant::className(), 'targetAttribute' => ['fkRestaurantID' => 'pkRestaurantID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkUserRestaurantBookingID' => 'Pk User Restaurant Booking ID',
            'fkRestaurantID' => 'Fk Restaurant ID',
            'fkUserID' => 'Fk User ID',
            'userRestaurantQRCode' => 'User Restaurant Qrcode',
            'userNumberSeats' => 'User Number Seats',
            'waitingTime' => 'Waiting Time',
            'buzzInTime' => 'Buzz In Time',
            'date' => 'Date',
            'bookingId' => 'bookingId',
            'userCancelBookong' => 'User Cancel Bookong',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkUser()
    {
        return $this->hasOne(Users::className(), ['pkUserID' => 'fkUserID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkRestaurant()
    {
        return $this->hasOne(Restaurant::className(), ['pkRestaurantID' => 'fkRestaurantID']);
    }

    public function getFkUserManage()
    {
        return $this->hasOne(UserRestaurantManage::className(), ['fkRestaurantID' => 'fkRestaurantID']);
    }


    public function updateTime($params)
    {
        $this->load(array('UserRestaurantBooking' => $params));
        // return $params;
        //$bookingId = $this->bookingId;

        $result = self::findOne(['pkUserRestaurantBookingID' => $this->bookingId]);

        if($result)
        {
             $result->waitingTime = $this->timeValue;
             if($result->save()){
                return 'changed';
             }else{
                return false;
             }
        }

    }

    public function updateVip($params)
    {

        $this->load(array('UserRestaurantBooking' => $params));
        $result = Users::find()->where(['pkUserID' => $this->userId])->one();
        if($result)
        {
            $result->userVip = $this->isVip;

            if($result->save())
            {
                if($this->isVip == '0')
                {
                    return 'notVip';

                }elseif($this->isVip == '1')
                {
                    return 'vip';
                }
                
            }
            else
            {
                return false;
            }
        }
    }

    public function createRestaurantBooking()
    {
        $postValues = Yii::$app->request->post();
        //echo '<pre>'; print_r($postValues); echo '</pre>';
        //die;

        $userId = (isset($postValues['userId'])) ? $postValues['userId'] : NULL ;
        $restaurantId = (isset($postValues['restaurantId'])) ? $postValues['restaurantId'] : NULL ;
        $todayDate = date("Y-m-d");
        $timestamp = time();

        $this->fkUserID = $userId;
        $this->fkRestaurantID = $restaurantId;
        $this->date = $todayDate;
        $this->timestamp = $timestamp;

        if($this->save())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
