<?php

namespace backend\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "restaurant_discounts".
 *
 * @property string $pkRestaurantDiscountsID
 * @property string $fkRestaurantID
 * @property string $storeName
 * @property integer $discount
 * @property string $discountImage
 * @property integer $pointsEarned
 * @property string $storeAddress
 * @property string $storeLatitude
 * @property string $storeLongitude
 *
 * @property Restaurant $fkRestaurant
 */
class RestaurantDiscounts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restaurant_discounts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkRestaurantID', 'discount', 'pointsEarned'], 'integer'],
            [['storeName', 'storeLatitude', 'storeLongitude'], 'string', 'max' => 100],
            [['discountImage'], 'string', 'max' => 1000],
            [['storeAddress'], 'string', 'max' => 200],
            [['fkRestaurantID'], 'exist', 'skipOnError' => true, 'targetClass' => Restaurant::className(), 'targetAttribute' => ['fkRestaurantID' => 'pkRestaurantID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkRestaurantDiscountsID' => 'Pk Restaurant Discounts ID',
            'fkRestaurantID' => 'Fk Restaurant ID',
            'storeName' => 'Store Name',
            'discount' => 'Discount',
            'discountImage' => 'Discount Image',
            'pointsEarned' => 'Points Earned',
            'storeAddress' => 'Store Address',
            'storeLatitude' => 'Store Latitude',
            'storeLongitude' => 'Store Longitude',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkRestaurant()
    {
        return $this->hasOne(Restaurant::className(), ['pkRestaurantID' => 'fkRestaurantID']);
    }

    public function createRestaurant($param)
    {
            //echo '<pre>'; print_r($_FILES); echo '</pre>'; die;

            if(@$_FILES['storeImage']['name'])
            {
                $storeImage = self::uploadStoreImage($_FILES['storeImage']);
                //echo '<pre>'; print_r($result); echo '</pre>';
                    if($storeImage['fileUrl'])
                    {
                        $this->discountImage = $storeImage['fileUrl'];
                    }
            }
            if($this->save()){
                return true;
            }
            else
            {
                return false;
            }


    }

    public static function uploadStoreImage($params)
    {
        if(isset($params) && $params['size'] > 0)
        {
            $arrMediaInfo = pathinfo($params['name']);
            $extension = $arrMediaInfo['extension'];
            $rand = Yii::$app->security->generateRandomString();
            $uploadDir = Url::to('@backend').'/web/restaurant/stores'; 
    
            $arrMediaInfo['filename'] = str_replace(' ', '', $arrMediaInfo['filename']);
            $varFileNameWithTimeStamp = $arrMediaInfo['filename'].'-'.$rand;
            $newFileName = $uploadDir.'/'.$varFileNameWithTimeStamp.'.'.$extension;

            if(move_uploaded_file($params['tmp_name'], $newFileName))
            {
                    $data['fileName'] = $varFileNameWithTimeStamp.'.'.$extension;
                    $data['fileUrl'] = Url::base(true).'/restaurant/stores/'.$varFileNameWithTimeStamp.'.'.$extension;
                    return $data;
            }
            return false;
        }
    }




}
