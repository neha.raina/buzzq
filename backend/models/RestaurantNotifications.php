<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "restaurant_notifications".
 *
 * @property string $pkRestaurantNotificationsID
 * @property string $fkRestaurantID
 * @property string $notificationImage
 * @property string $description
 * @property integer $discount
 * @property integer $pointsEarned
 *
 * @property Restaurant $fkRestaurant
 */
class RestaurantNotifications extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restaurant_notifications';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkRestaurantID', 'discount', 'pointsEarned'], 'integer'],
            [['notificationImage'], 'string', 'max' => 1000],
            [['description'], 'string', 'max' => 2000],
            [['fkRestaurantID'], 'exist', 'skipOnError' => true, 'targetClass' => Restaurant::className(), 'targetAttribute' => ['fkRestaurantID' => 'pkRestaurantID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkRestaurantNotificationsID' => 'Pk Restaurant Notifications ID',
            'fkRestaurantID' => 'Fk Restaurant ID',
            'notificationImage' => 'Notification Image',
            'description' => 'Description',
            'discount' => 'Discount',
            'pointsEarned' => 'Points Earned',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkRestaurant()
    {
        return $this->hasOne(Restaurant::className(), ['pkRestaurantID' => 'fkRestaurantID']);
    }
}
