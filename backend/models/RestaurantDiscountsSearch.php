<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\RestaurantDiscounts;

/**
 * RestaurantDiscountsSearch represents the model behind the search form about `backend\models\RestaurantDiscounts`.
 */
class RestaurantDiscountsSearch extends RestaurantDiscounts
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pkRestaurantDiscountsID', 'fkRestaurantID', 'discount', 'pointsEarned'], 'integer'],
            [['storeName', 'discountImage', 'storeAddress', 'storeLatitude', 'storeLongitude'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RestaurantDiscounts::find()->joinWith('fkRestaurant');

        $currUserId = Yii::$app->user->getId();
        $currentuserId = isset($currUserId) ? $currUserId : '0';
        $restList = UserRestaurantManage::find()->where(['fkUserRestaurantOwnerID' => $currentuserId])->all();
        if(@$restList){
            foreach($restList as $restLists){
                $restaurantArray[] = $restLists->fkRestaurantID;
            }
        }else{
            $restaurantArray = NULL;
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pkRestaurantDiscountsID' => $this->pkRestaurantDiscountsID,
            //'fkRestaurantID' => $this->fkRestaurantID,
            'discount' => $this->discount,
            //'pointsEarned' => $this->pointsEarned,
        ]);

        $query->andFilterWhere(['like', 'storeName', $this->storeName])
            //->andFilterWhere(['like', 'discountImage', $this->discountImage])
            ->andFilterWhere(['like', 'storeAddress', $this->storeAddress]);
            //->andFilterWhere(['like', 'storeLatitude', $this->storeLatitude])
            //->andFilterWhere(['like', 'storeLongitude', $this->storeLongitude]);

            if(@$this->fkRestaurantID){
            $query->andFilterWhere(['fkRestaurantID'=> $this->fkRestaurantID]);
        }else{
            $query->where(['fkRestaurantID'=> $restaurantArray]);
        }

        return $dataProvider;
    }
}
