<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "restaurant_menu".
 *
 * @property string $pkRestaurantMenuID
 * @property string $fkRestaurantID
 * @property string $menuName
 * @property string $restaurantPicture
 *
 * @property Restaurant $fkRestaurant
 */
class RestaurantMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public $menuId;
    public $menuChangeName;

    public static function tableName()
    {
        return 'restaurant_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkRestaurantID'], 'integer'],
            [['restaurantPicture'], 'string', 'max' => 200],
            [['menuId','menuChangeName'], 'safe'],
            [['fkRestaurantID'], 'exist', 'skipOnError' => true, 'targetClass' => Restaurant::className(), 'targetAttribute' => ['fkRestaurantID' => 'pkRestaurantID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkRestaurantMenuID' => 'Pk Restaurant Menu ID',
            'fkRestaurantID' => 'Fk Restaurant ID',
            'menuName' => 'Menu Name',
            'restaurantPicture' => 'Restaurant Picture',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkRestaurant()
    {
        return $this->hasOne(Restaurant::className(), ['pkRestaurantID' => 'fkRestaurantID']);
    }

    public function updateMenuName($params){

        $this->load(array('RestaurantMenu' => $params));
        $result = self::find()->where(['pkRestaurantMenuID' => $this->menuId])->one();
        if($result)
        {   
            $result->menuName = $this->menuChangeName;
            if($result->save()){
                return 'saved';
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public function deleteMenu($params){

        $this->load(array('RestaurantMenu' => $params));
        $result = self::find()->where(['pkRestaurantMenuID' => $this->menuId])->one();
        if($result)
        {  
            if($result->delete()){
                return 'deleted';
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

}
