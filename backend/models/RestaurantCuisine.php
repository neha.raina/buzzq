<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "restaurant_cuisine".
 *
 * @property string $pkRestaurantCuisineID
 * @property string $fkRestaurantID
 * @property integer $pkRestaurantCuisineNameID
 *
 * @property Restaurant $fkRestaurant
 * @property RestaurantCuisineNames $pkRestaurantCuisineName
 */
class RestaurantCuisine extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restaurant_cuisine';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkRestaurantID', 'pkRestaurantCuisineNameID'], 'integer'],
            [['fkRestaurantID'], 'exist', 'skipOnError' => true, 'targetClass' => Restaurant::className(), 'targetAttribute' => ['fkRestaurantID' => 'pkRestaurantID']],
            [['pkRestaurantCuisineNameID'], 'exist', 'skipOnError' => true, 'targetClass' => RestaurantCuisineNames::className(), 'targetAttribute' => ['pkRestaurantCuisineNameID' => 'pkRestaurantCuisineNameID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkRestaurantCuisineID' => 'Pk Restaurant Cuisine ID',
            'fkRestaurantID' => 'Fk Restaurant ID',
            'pkRestaurantCuisineNameID' => 'Pk Restaurant Cuisine Name ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkRestaurant()
    {
        return $this->hasOne(Restaurant::className(), ['pkRestaurantID' => 'fkRestaurantID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPkRestaurantCuisineName()
    {
        return $this->hasOne(RestaurantCuisineNames::className(), ['pkRestaurantCuisineNameID' => 'pkRestaurantCuisineNameID']);
    }
}
