<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property string $pkUserID
 * @property string $userEmail
 * @property string $userPassword
 * @property string $userResetToken
 * @property string $userAuthKey
 * @property string $userOTP
 * @property string $userOtpTime
 * @property string $userSignupType
 * @property string $userSocialID
 * @property string $userName
 * @property string $userNumber
 * @property string $userDOB
 * @property string $userSex
 * @property string $userNationality
 * @property string $userPicture
 * @property string $userDeviceToken
 * @property integer $userVip
 * @property integer $userEmailVerified
 * @property string $userStatus
 * @property string $userAdded
 * @property string $userModified
 *
 * @property UserRestaurantBooking[] $userRestaurantBookings
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userOtpTime', 'userVip', 'userEmailVerified'], 'integer'],
            [['userDOB', 'userAdded', 'userModified'], 'safe'],
            [['userEmail', 'userPassword'], 'string', 'max' => 150],
            [['userResetToken'], 'string', 'max' => 255],
            [['userAuthKey', 'userSocialID'], 'string', 'max' => 100],
            [['userOTP'], 'string', 'max' => 15],
            [['userSignupType', 'userName', 'userNationality'], 'string', 'max' => 50],
            [['userNumber', 'userSex'], 'string', 'max' => 20],
            [['userPicture'], 'string', 'max' => 1000],
            [['userDeviceToken'], 'string', 'max' => 500],
            [['userStatus'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkUserID' => 'Pk User ID',
            'userEmail' => 'User Email',
            'userPassword' => 'User Password',
            'userResetToken' => 'User Reset Token',
            'userAuthKey' => 'User Auth Key',
            'userOTP' => 'User Otp',
            'userOtpTime' => 'User Otp Time',
            'userSignupType' => 'User Signup Type',
            'userSocialID' => 'User Social ID',
            'userName' => 'User Name',
            'userNumber' => 'User Number',
            'userDOB' => 'User Dob',
            'userSex' => 'User Sex',
            'userNationality' => 'User Nationality',
            'userPicture' => 'User Picture',
            'userDeviceToken' => 'User Device Token',
            'userVip' => 'User Vip',
            'userEmailVerified' => 'User Email Verified',
            'userStatus' => 'User Status',
            'userAdded' => 'User Added',
            'userModified' => 'User Modified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRestaurantBookings()
    {
        return $this->hasMany(UserRestaurantBooking::className(), ['fkUserID' => 'pkUserID']);
    }
}
