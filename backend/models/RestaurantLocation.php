<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "restaurant_location".
 *
 * @property integer $pkRestaurantLoactionID
 * @property string $fkRestaurantCityID
 * @property string $locationName
 *
 * @property Restaurant[] $restaurants
 * @property RestaurantCity $fkRestaurantCity
 */
class RestaurantLocation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public $cityId;
    public static function tableName()
    {
        return 'restaurant_location';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkRestaurantCityID'], 'integer'],
            [['locationName'], 'string', 'max' => 100],
            [['cityId'], 'safe'],
            [['fkRestaurantCityID'], 'exist', 'skipOnError' => true, 'targetClass' => RestaurantCity::className(), 'targetAttribute' => ['fkRestaurantCityID' => 'pkRestaurantCityID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkRestaurantLoactionID' => 'Pk Restaurant Loaction ID',
            'fkRestaurantCityID' => 'Fk Restaurant City ID',
            'locationName' => 'Location Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurants()
    {
        return $this->hasMany(Restaurant::className(), ['restaurantLocationID' => 'pkRestaurantLoactionID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkRestaurantCity()
    {
        return $this->hasOne(RestaurantCity::className(), ['pkRestaurantCityID' => 'fkRestaurantCityID']);
    }

    public function getLocations($params){

        $this->load(array('RestaurantLocation' => $params));
        $result = self::find()->where(['fkRestaurantCityID' => $this->cityId])->groupBy('locationName')->all();
        //return json_encode($result);
        if($result)
        {   
            $response = '';
            foreach($result as $results)
            {
                $response .= '<option value="'.$results->pkRestaurantLoactionID.'">'.$results->locationName.'</option>';
            }
                $response .= '<option value="other">Other</option>';
        }
        else
        {
            return false;
        }
        return json_encode($response);
    }
}
