<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "restaurant_cuisine_names".
 *
 * @property integer $pkRestaurantCuisineNameID
 * @property string $cuisineName
 *
 * @property RestaurantCuisine[] $restaurantCuisines
 */
class RestaurantCuisineNames extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restaurant_cuisine_names';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cuisineName'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkRestaurantCuisineNameID' => 'Pk Restaurant Cuisine Name ID',
            'cuisineName' => 'Cuisine Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantCuisines()
    {
        return $this->hasMany(RestaurantCuisine::className(), ['pkRestaurantCuisineNameID' => 'pkRestaurantCuisineNameID']);
    }
}
