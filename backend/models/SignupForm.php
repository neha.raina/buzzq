<?php
namespace backend\models;

use yii\base\Model;
use backend\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $userName;
    public $userEmail;
    public $password;
    public $cpassword;
    public $city;
   // public $timings;
  //  public $restuarantName;
    public $userNumber;
    public $address;
   // public $aboutrestuarant;
    public $subscribeNews;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['userName', 'trim'],
            ['userName', 'required'],
            ['userName', 'unique', 'targetClass' => '\backend\models\User', 'message' => 'This username has already been taken.'],
            ['userName', 'string', 'min' => 2, 'max' => 255],
            
            
             ['userNumber', 'trim'],
             ['userNumber', 'required'],
             ['userNumber', 'unique', 'targetClass' => 'backend\models\User', 'message' => 'This Phone No has already been taken.'],
            

            ['userEmail', 'trim'],
            ['userEmail', 'required'],
            ['userEmail', 'email'],
            ['userEmail', 'string', 'max' => 255],
            ['userEmail', 'unique', 'targetClass' => 'backend\models\User', 'message' => 'This email address has already been taken.'],

             ['password', 'required'],
             ['cpassword', 'required'],
             ['password', 'string', 'min' => 6],
             ['cpassword','safe'],
             ['cpassword', 'compare', 'compareAttribute' => 'password','message'=>'Password dont match'],
            
             ['city', 'required'],
             ['address', 'required']
             
            
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->userName = $this->userName;
        $user->userEmail = $this->userEmail;
        $user->userNumber = $this->userNumber;
        $user->userCity = $this->city;
         $user->userAddress = $this->address;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }
}
