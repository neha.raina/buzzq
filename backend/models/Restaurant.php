<?php

namespace backend\models;

use Yii;
use yii\helpers\Url;
use backend\models\RestaurantCity;
use backend\models\RestaurantLocation;
use backend\models\RestaurantMenu;
use backend\models\UserRestaurantManage;


/**
 * This is the model class for table "restaurant".
 *
 * @property string $pkRestaurantID
 * @property string $restaurantQRCode
 * @property string $restaurantName
 * @property string $restaurantDescription
 * @property string $restaurantCityID
 * @property string $restaurantTimings
 * @property string $restaurantType
 * @property string $restaurantLogo
 * @property string $restaurantLink
 * @property integer $restaurantLocationID
 * @property string $restaurantAddress
 * @property string $restaurantLatitude
 * @property string $restaurantLongitude
 * @property integer $restaurantWaitingTime
 * @property integer $restaurantStatus
 *
 * @property RestaurantCity $restaurantCity
 * @property RestaurantLocation $restaurantLocation
 * @property RestaurantCuisine[] $restaurantCuisines
 * @property RestaurantDiscounts[] $restaurantDiscounts
 * @property RestaurantMenu[] $restaurantMenus
 * @property RestaurantNotifications[] $restaurantNotifications
 * @property UserRestaurantBooking[] $userRestaurantBookings
 * @property UserRestaurantManage[] $userRestaurantManages
 */
class Restaurant extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $restaurantId;
    public static function tableName()
    {
        return 'restaurant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['restaurantCityID', 'restaurantLocationID', 'restaurantWaitingTime', 'restaurantStatus'], 'integer'],
            [['restaurantName', 'restaurantWaitingTime','restaurantTimings'], 'required'],
            [['restaurantQRCode'], 'string', 'max' => 500],
            [['restaurantId'], 'safe'],
            [['restaurantName', 'restaurantLogo'], 'string', 'max' => 200],
            [['restaurantDescription', 'restaurantLink'], 'string', 'max' => 1000],
            [['restaurantTimings', 'restaurantType'], 'string', 'max' => 100],
            [['restaurantAddress'], 'string', 'max' => 255],
            [['restaurantCityID'], 'exist', 'skipOnError' => true, 'targetClass' => RestaurantCity::className(), 'targetAttribute' => ['restaurantCityID' => 'pkRestaurantCityID']],
            [['restaurantLocationID'], 'exist', 'skipOnError' => true, 'targetClass' => RestaurantLocation::className(), 'targetAttribute' => ['restaurantLocationID' => 'pkRestaurantLoactionID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkRestaurantID' => 'Pk Restaurant ID',
            'restaurantQRCode' => 'Restaurant Qrcode',
            'restaurantName' => 'Restaurant Name',
            'restaurantDescription' => 'Restaurant Description',
            'restaurantCityID' => 'Restaurant City ID',
            'restaurantTimings' => 'Restaurant Timings',
            'restaurantType' => 'Restaurant Type',
            'restaurantLogo' => 'Restaurant Logo',
            'restaurantLink' => 'Restaurant Link',
            'restaurantLocationID' => 'Restaurant Location ID',
            'restaurantAddress' => 'Restaurant Address',
            'restaurantLatitude' => 'Restaurant Latitude',
            'restaurantLongitude' => 'Restaurant Longitude',
            'restaurantWaitingTime' => 'Restaurant Waiting Time',
            'restaurantStatus' => 'Restaurant Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantCity()
    {
        return $this->hasOne(RestaurantCity::className(), ['pkRestaurantCityID' => 'restaurantCityID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantLocation()
    {
        return $this->hasOne(RestaurantLocation::className(), ['pkRestaurantLoactionID' => 'restaurantLocationID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantCuisines()
    {
        return $this->hasMany(RestaurantCuisine::className(), ['fkRestaurantID' => 'pkRestaurantID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantDiscounts()
    {
        return $this->hasMany(RestaurantDiscounts::className(), ['fkRestaurantID' => 'pkRestaurantID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantMenus()
    {
        return $this->hasMany(RestaurantMenu::className(), ['fkRestaurantID' => 'pkRestaurantID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantNotifications()
    {
        return $this->hasMany(RestaurantNotifications::className(), ['fkRestaurantID' => 'pkRestaurantID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRestaurantBookings()
    {
        return $this->hasMany(UserRestaurantBooking::className(), ['fkRestaurantID' => 'pkRestaurantID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRestaurantManages()
    {
        return $this->hasMany(UserRestaurantManage::className(), ['fkRestaurantID' => 'pkRestaurantID']);
    }

    public function createRestaurant($param)
    {
        $postValues = Yii::$app->request->post();
        //echo '<pre>'; print_r($param); echo '</pre>';
        
        $restaurantCityID = (isset($postValues['restaurantCityID'])) ? $postValues['restaurantCityID'] : NULL ;
        $restaurantLocationID = (isset($postValues['restaurantLocationID'])) ? $postValues['restaurantLocationID'] : NULL ;
        $restaurantMenuName = (isset($postValues['restaurantMenuName'])) ? $postValues['restaurantMenuName'] : NULL ;

        if(($restaurantCityID) && ($restaurantCityID != 'other'))
        {
            $this->restaurantCityID = $restaurantCityID;
            $cityName = RestaurantCity::findOne(['pkRestaurantCityID' => $restaurantCityID]);
            if($cityName)
            {
                $restaurantCityName = $cityName->cityName;
            }
            else
            {
                $restaurantCityName = NULL;
            }
        }
        else
        {
            $restaurantOtherCity = (isset($postValues['restaurantOtherCity'])) ? $postValues['restaurantOtherCity'] : NULL ;
            $restaurantCityName = $restaurantOtherCity;
            if($restaurantOtherCity)
            {
                $city = new RestaurantCity();
                $city->cityName = $restaurantOtherCity;

                if($city->save()){
                    $restaurantCityID = $city->pkRestaurantCityID;
                }else{
                    $restaurantCityID = NULL;    
                }

                $this->restaurantCityID = $restaurantCityID;
            }

        }

        if(($restaurantLocationID) && ($restaurantLocationID != 'other'))
        {
            $this->restaurantLocationID = $restaurantLocationID;
            $locationName = RestaurantLocation::findOne(['pkRestaurantLoactionID' => $restaurantLocationID]);
            if($locationName)
            {
                $restaurantLocationName = $locationName->locationName;
            }
            else
            {
                $restaurantLocationName = NULL;
            }
        }
        else
        {   
            $restaurantOtherLocation = (isset($postValues['restaurantOtherLocation'])) ? $postValues['restaurantOtherLocation'] : NULL ;
            $restaurantLocationName = $restaurantOtherLocation;
            if($restaurantOtherLocation)
            {
                $location = new RestaurantLocation();
                $location->fkRestaurantCityID = $restaurantCityID; 
                $location->locationName = $restaurantOtherLocation;

                if($location->save())
                {
                    $restaurantLocationID = $location->pkRestaurantLoactionID;
                }
                else
                {
                    $restaurantLocationID = NULL;    
                }

                $this->restaurantLocationID = $restaurantLocationID;
            }

        }

        $restaurantCityName = trim($restaurantCityName);
        $restaurantCityName = str_replace(" ", "+", $restaurantCityName);
        $restaurantLocationName = trim($restaurantLocationName);
        $restaurantLocationName = str_replace(" ", "+", $restaurantLocationName);
        $address = $restaurantLocationName.','.$restaurantCityName;
        $jsonAddress = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false");
        $addressLatLong = json_decode($jsonAddress);
        if($addressLatLong->results[0]->geometry->location)
        {
            $latitude = (isset($addressLatLong->results[0]->geometry->location->lat)) ? $addressLatLong->results[0]->geometry->location->lat : NULL;
            $longitude = (isset($addressLatLong->results[0]->geometry->location->lng)) ? $addressLatLong->results[0]->geometry->location->lng : NULL;

            $this->restaurantLatitude = $latitude;
            $this->restaurantLongitude = $longitude;
        }

            $rand = Yii::$app->security->generateRandomString();
            $restaurantName = str_replace(" ", "", $this->restaurantName);
            $restaurantName = trim($restaurantName);
            $restaurantQRCode = $restaurantName.'-'.$rand;
            
            if(@!$this->pkRestaurantID){
                $this->restaurantQRCode = $restaurantQRCode;
            }
            


            if(@$_FILES['restaurantLogo']['name'])
            {
                $restaurantLogo = self::uploadRestaurantLogo($_FILES['restaurantLogo']);
                //echo '<pre>'; print_r($result); echo '</pre>';
                    if($restaurantLogo['fileUrl'])
                    {
                        $this->restaurantLogo = $restaurantLogo['fileUrl'];
                    }
            }

            if(@$_FILES['menuImages']['name'][0])
            {
               $restaurantMenus = self::uploadMenuImages($_FILES['menuImages']); 
            }

            if($this->save())
            {
                $pkRestaurantID = $this->pkRestaurantID;
            }
            //echo '<pre>'; print_r($this); echo '</pre>';  die;
            if(@$param == 'create'){

                $currUserId = Yii::$app->user->getId();
                $userRestaurantManage = new UserRestaurantManage();
                $userRestaurantManage->fkUserRestaurantOwnerID = $currUserId;
                $userRestaurantManage->fkRestaurantID = $pkRestaurantID;

                $userRestaurantManage->save();
            }

            

            if(@$restaurantMenus['fileUrl'][0])
            {
                if($restaurantMenuName)
                {
                    $menuName = $restaurantMenuName;
                }
                else
                {
                    $menuName = 'Menu';
                }
                $x=1;
                for ($i = 0.; $i < count($restaurantMenus['fileUrl']); $i++)
                {
                    $restaurantMenu = new RestaurantMenu();
                    $restaurantMenu->fkRestaurantID = $pkRestaurantID;
                    $restaurantMenu->menuName = $menuName.$x;
                    $restaurantMenu->restaurantPicture = $restaurantMenus['fileUrl'][$i];

                    $restaurantMenu->save();
                    $x++;
                }
            }
            // echo '<pre>'; print_r($this); echo '</pre>';
            return true;
    }

    public static function uploadRestaurantLogo($params)
    {
        if(isset($params) && $params['size'] > 0)
        {
            $arrMediaInfo = pathinfo($params['name']);
            $extension = $arrMediaInfo['extension'];
            $rand = Yii::$app->security->generateRandomString();
            $uploadDir = Url::to('@backend').'/web/restaurant/logos'; 
    
            $arrMediaInfo['filename'] = str_replace(' ', '', $arrMediaInfo['filename']);
            $varFileNameWithTimeStamp = $arrMediaInfo['filename'].'-'.$rand;
            $newFileName = $uploadDir.'/'.$varFileNameWithTimeStamp.'.'.$extension;

            if(move_uploaded_file($params['tmp_name'], $newFileName))
            {
                    $data['fileName'] = $varFileNameWithTimeStamp.'.'.$extension;
                    $data['fileUrl'] = Url::base(true).'/restaurant/logos/'.$varFileNameWithTimeStamp.'.'.$extension;
                    return $data;
            }
            return false;
        }
    }

    public static function uploadMenuImages($params)
    {
        if(isset($params['name']) && $params['size'] > 0)
        {
            for ($i = 0; $i < count($params['name']); $i++)
            {
                $arrMediaInfo = pathinfo($params['name'][$i]);
                $extension = $arrMediaInfo['extension'];
                $rand = Yii::$app->security->generateRandomString();
                $uploadDir = Url::to('@backend').'/web/restaurant/menus'; 
        
                $arrMediaInfo['filename'] = str_replace(' ', '', $arrMediaInfo['filename']);
                $varFileNameWithTimeStamp = $arrMediaInfo['filename'].'-'.$rand;
                $newFileName = $uploadDir.'/'.$varFileNameWithTimeStamp.'.'.$extension;

                if(move_uploaded_file($params['tmp_name'][$i], $newFileName))
                {
                        $data['fileName'][$i] = $varFileNameWithTimeStamp.'.'.$extension;
                        $data['fileUrl'][$i] = Url::base(true).'/restaurant/menus/'.$varFileNameWithTimeStamp.'.'.$extension;     
                }   
            }
                return $data;
        }
            return false;
    }

}
