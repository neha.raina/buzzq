<?php

namespace backend\controllers;

use Yii;
use backend\models\UserRestaurantBooking;
use backend\models\UserRestaurantBookingSearch;
use backend\models\Restaurant;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UserRestaurantBookingController implements the CRUD actions for UserRestaurantBooking model.
 */
class UserRestaurantBookingController extends Controller
{
    public $request;
    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['POST'],
    //             ],
    //         ],
    //     ];
    // }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login','signup', 'error','update-time','update-vip','restaurant-details'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['view', 'index','create','update','delete','update-time','update-vip','restaurant-details','transactions'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
              /*  'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],*/
          ];
    }

    /**
     * Lists all UserRestaurantBooking models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserRestaurantBookingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTransactions()
    {
        $searchModel = new UserRestaurantBookingSearch();
        $dataProvider = $searchModel->searchTransaction(Yii::$app->request->queryParams);

        return $this->render('transaction', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionUpdateTime()
    {
        
        $values = Yii::$app->request->post();
        $model = new UserRestaurantBooking();
        $response = $model->updateTime($values);
        return $response;
    }

    public function actionUpdateVip()
    {
        
        $values = Yii::$app->request->post();
        // print_r($values);
        // die;
        $model = new UserRestaurantBooking();
        $response = $model->updateVip($values);
        return $response;
    }

    public function actionRestaurantDetails()
    {
        $restaurantID = Yii::$app->request->post('restaurantId');

        if(isset( $restaurantID ))
        {
          $response = Restaurant::findOne($restaurantID); 

          if($response)
          {
              return \yii\helpers\Json::encode($response);
          }
        }
        else
        {
          return false;
        }
      
    }
    /**
     * Displays a single UserRestaurantBooking model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModelCustom($id),
        ]);

    }

    /**
     * Creates a new UserRestaurantBooking model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserRestaurantBooking();
        if ($model->load(Yii::$app->request->post()) && $model->createRestaurantBooking()) {
            return $this->redirect(['view', 'id' => $model->pkUserRestaurantBookingID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UserRestaurantBooking model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pkUserRestaurantBookingID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UserRestaurantBooking model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserRestaurantBooking model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return UserRestaurantBooking the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserRestaurantBooking::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelCustom($id)
    {
        if (($model = UserRestaurantBooking::find()->joinWith(['fkUser','fkRestaurant'])->where(['user_restaurant_booking.pkUserRestaurantBookingID' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

