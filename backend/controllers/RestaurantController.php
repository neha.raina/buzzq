<?php

namespace backend\controllers;

use Yii;
use backend\models\Restaurant;
use backend\models\RestaurantSearch;
use backend\models\RestaurantLocation;
use backend\models\RestaurantMenu;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RestaurantController implements the CRUD actions for Restaurant model.
 */
class RestaurantController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Restaurant models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RestaurantSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Restaurant model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModelCustom($id),
        ]);
    }

    public function actionGetLocations()
    {
        
        $values = Yii::$app->request->post();
         // print_r($values);
         // die;
        $model = new RestaurantLocation();
        $response = $model->getLocations($values);
        return $response;
    }

    public function actionUpdateMenuname()
    {
        
        $values = Yii::$app->request->post();
        $model = new RestaurantMenu();
        $response = $model->updateMenuName($values);
        return $response;
    }

    public function actionDeleteMenu()
    {   
        $values = Yii::$app->request->post();
        $model = new RestaurantMenu();
        $response = $model->deleteMenu($values);
        return $response;
    }

    /**
     * Creates a new Restaurant model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Restaurant();
        if ($model->load(Yii::$app->request->post()) && $model->createRestaurant('create')) {
            return $this->redirect(['view', 'id' => $model->pkRestaurantID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Restaurant model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModelCustom($id);
        if ($model->load(Yii::$app->request->post()) && $model->createRestaurant('update')) {
            return $this->redirect(['view', 'id' => $model->pkRestaurantID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Restaurant model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Restaurant model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Restaurant the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Restaurant::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelCustom($id)
    {
        if (($model = Restaurant::find()->joinWith(['restaurantCity','restaurantLocation','restaurantMenus'])->where(['restaurant.pkRestaurantID' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
