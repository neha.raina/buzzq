$( function() {
    $( "#datepickerFrom" ).datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd',
    maxDate: '+0d',
    });

    $( "#datepickerTo" ).datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd',
    maxDate: '+0d',
    });
    
    $("#mainDateFrom" ).datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd',
    maxDate: '+0d',
    });

    $("#mainDateTo" ).datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd',
    maxDate: '+0d',
    });    

    });

$(document).ready(function(){
    //swal("Here's a message!");
    
    $('.edit-value').click(function(){
    var bookingId = $(this).attr('bookingid');
    //alert(bookingId);
    $('#span_'+bookingId).hide();
    $('#input_'+bookingId).show();

    $('#edit_time_'+bookingId).hide();
    $('#update_time_'+bookingId).show();
    });

    $('.update-value').click(function(){
    var bookingId = $(this).attr('bookingid');
    var timeValue = $('#input_'+bookingId).val();
    $('#span_'+bookingId).text(timeValue);

    $('#span_'+bookingId).show();
    $('#input_'+bookingId).hide();

    $('#edit_time_'+bookingId).show();
    $('#update_time_'+bookingId).hide()

    $.ajax({
    type     :'POST',
    cache    : false,
    url      : 'update-time',
    data     : {bookingId:bookingId, timeValue:timeValue},
    success  : function(response) {
        if(response == 'changed')
        {
            swal('The time has been changed.');
        }
        else
        {
            swal('There is some error occuring while changing the time.');
        }

    }
    });

    });

    $('#markVip').click(function(){
            
        var status;
        if($(this).is(":checked")){
            status = '1';          
        }
        else if($(this).is(":not(:checked)")){
            status = '0';
        }
        
        var userId = $(this).attr('uid');

        $.ajax({
        type     :'POST',
        cache    : false,
        url      : 'update-vip',
        data     : {userId:userId, isVip:status},
        success  : function(response) {
            if(response == 'notVip')
            {
                swal('The user has been changed from Vip to Normal.');
            }
            else if(response == 'vip')
            {
                swal('The user has been changed from Normal to Vip.');
                   
            }else
            {
                swal('There is some error occuring while changing the status.');
            }

        }
        });

    });

    $('#restaurantSubmit').click(function(res){
      var cityId =  $('#restaurant-restaurantcityid').val();
      var locationId =  $('#locationID').val();
      
      if(cityId == '')
      {
        swal('Please select any of one city.');
        return false;
      }
      else if(cityId == 'other')
      {
        var otherCity = $('#restaurantOtherCity').val();
        if(otherCity == '')
        {
            swal('Please enter city name.');
            $("#restaurantOtherCity").focus();
            return false;
        }
        else
        {
            if(locationId == 'other')
            {
                var otherlocation = $('#restaurantOtherLocation').val();
                if(otherlocation == ''){
                swal('Please enter location name.');
                $("#restaurantOtherLocation").focus();
                return false;
                }
                else
                {  
                return true;
                }
            }
            else
            {
            swal('Please select any of one location.');
            return false;
            }
        }
      }
      else
      {
            if(locationId != 'other'){
                return true;
            }else
            if(locationId == 'other')
            {
                var otherlocation = $('#restaurantOtherLocation').val();
                if(otherlocation == ''){
                swal('Please enter location name.');
                $("#restaurantOtherLocation").focus();
                return false;
                }
                else
                {  
                return true;
                }
            }
            else
            {
            swal('Please select any of one location.');
            return false;
            }
      }


    });


    //$('#restaurant-restaurantcityid').append('<option value="other">Other</option>');
    $('#restaurant-restaurantcityid').change(function(){
        var cityId = $(this).val();
        $('#locationID').empty();
        if(cityId != ''){

            if(cityId == 'other'){
                $('#restaurantOtherCity').show();
                $('#restaurantOtherLocation').show();
                $('#locationID').empty();
                $('#locationID').append('<option value="other">Other</option>');
            }
            else
            {
                $('#restaurantOtherLocation').hide();
                $('#restaurantOtherCity').hide();
                $.ajax({
                type     :'POST',
                cache    : false,
                url      : 'get-locations',
                data     : {cityId:cityId},
                success  : function(response) {
                    //console.log(response);
                    var json_obj = $.parseJSON(response);
                    $('#locationID').empty();
                    $('#locationID').append(json_obj);

                }
                });
            }
        }
    });

    $('#locationID').change(function(){
        var locationId = $(this).val();
        if(locationId != ''){

            if(locationId == 'other'){
                $('#restaurantOtherLocation').show();
            }
            else
            {   
                $('#restaurantOtherLocation').hide();
            }
        }

        });


    $('.updateMenu').click(function(){
        var menuId = $(this).attr('itemid');
        var menuChangeName = $('#menuName_'+menuId).val();

        $.ajax({
                type     :'POST',
                cache    : false,
                url      : 'update-menuname',
                data     : {menuId:menuId, menuChangeName:menuChangeName},
                success  : function(response) {
                    if(response)
                    {
                        swal('The menu name has been updated.');
                    }

                }
                });

    });

    $('.deleteMenu').click(function(){
        var menuId = $(this).attr('itemid');
        var a = confirm("Are you sure you want to delete this menu!");
        if(a == true)
        {
            $.ajax({
                type     :'POST',
                cache    : false,
                url      : 'delete-menu',
                data     : {menuId:menuId},
                success  : function(response) {
                    if(response == 'deleted')
                    {
                       $('#divMenu_'+menuId).remove();
                    }
                    

                    }
                   });
        }

    });

    $('#restaurantNames').change(function()
    {
        var restaurantId = $(this).val();

        $.ajax({
                type     :'POST',
                cache    : false,
                url      : 'restaurant-details',
                data     : {restaurantId:restaurantId},
                success  : function(response) {
                    
                    var json_obj = $.parseJSON(response);
                    $('#restaurantWaiting').val(json_obj.restaurantWaitingTime);
                    //console.log(json_obj.restaurantWaitingTime);

                }
                });

    });

    $('#graphSubmit').click(function(){
        var mainDateFrom = $('#mainDateFrom').val();
        var mainDateTo = $('#mainDateTo').val();

            if(mainDateFrom == '')
            {
                    swal('Please select From date.');    
                    return false;
            }
            else if(mainDateTo == '')
            {
                swal('Please select To date.')
                return false;
            }
            else
            {

                return true;
            }
    });

    $(".time-toggle").click (function()
    {
        $(".timer-box").toggle();
    });


   function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputFile").change(function () {
        readURL(this);
    });
	
	
	$('.owl-carousel').owlCarousel({
    loop:true,
    margin:0,
	autoplay: true,
    nav:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})

    
});

/* ///////////////////////////////////////////////////////////////////// 
// Preloader 
/////////////////////////////////////////////////////////////////////*/
 $(window).load(function(){
    $('#preloader').fadeOut('slow',function(){$(this).remove();});
  });


  


