<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\UserEnquirySearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <a class="heading" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            Search
            <i class="more-less glyphicon glyphicon-<?php echo ( Yii::$app->request->get( "UserEnquirySearch" ) == null ?"plus":"minus" ) ?> pull-right"></i>
        </a>
    </div>
    <div id="collapseOne" class="panel-body <?php echo ( Yii::$app->request->get( "UserEnquirySearch" ) == null ?"collapse":"collapse in" ) ?>">
        <div class="users-search">

            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]); ?>
        
        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
	    <div class="col-md-3">
                <?= $form->field($model, 'UserEmailId') ?>
            </div>
        
            </div>
            <div class="form-group">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Reset', ['index'], ['class' => 'btn btn-default']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>        
    </div>
</div>


    
