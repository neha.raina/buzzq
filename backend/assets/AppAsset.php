<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
       // 'css/site.css',
        'css/bootstrap.min.css',
        'css/dashboard.css',
        'css/owl.carousel.css',
        'css/sweetalert.css',
        'css/jquery-ui.css',
    ];
    public $js = [
        'js/bootstrap.min.js',
        'js/owl.carousel.js',
        'js/custom.js',
        'js/sweetalert.js',
        'js/jquery-ui.js',
        'js/highcharts.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
