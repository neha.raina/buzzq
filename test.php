-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 06, 2017 at 04:08 PM
-- Server version: 5.5.43
-- PHP Version: 5.4.45-4+deprecated+dontuse+deb.sury.org~precise+1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `yii_buzzq`
--

-- --------------------------------------------------------

--
-- Table structure for table `user_restaurant_manage`
--

CREATE TABLE IF NOT EXISTS `user_restaurant_manage` (
  `pkUserRestaurantManageID` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'User Restaurant Manage ID',
  `fkUserRestaurantOwnerID` bigint(11) DEFAULT NULL COMMENT 'User Restaurant Owner ID',
  `fkRestaurantID` bigint(11) DEFAULT NULL COMMENT 'Restaurant ID',
  PRIMARY KEY (`pkUserRestaurantManageID`),
  KEY `fkUserRestaurantOwnerID` (`fkUserRestaurantOwnerID`),
  KEY `fkRestaurantID` (`fkRestaurantID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user_restaurant_manage`
--

INSERT INTO `user_restaurant_manage` (`pkUserRestaurantManageID`, `fkUserRestaurantOwnerID`, `fkRestaurantID`) VALUES
(2, 1, 2),
(3, 1, 7),
(4, 1, 13);

-- --------------------------------------------------------

--
-- Table structure for table `user_restaurant_owner`
--

CREATE TABLE IF NOT EXISTS `user_restaurant_owner` (
  `pkUserRestaurantOwnerID` bigint(11) NOT NULL COMMENT 'Restaurant Owner ID',
  `userName` varchar(50) DEFAULT NULL COMMENT 'User Name',
  `userEmail` varchar(150) DEFAULT NULL COMMENT 'User Email',
  `userNumber` bigint(20) DEFAULT NULL COMMENT 'User Number',
  `password_hash` varchar(150) DEFAULT NULL COMMENT 'User Password',
  `auth_key` varchar(100) DEFAULT NULL COMMENT 'User AuthKey',
  `userOTP` varchar(20) DEFAULT NULL COMMENT 'User OTP',
  `userSex` enum('Male','Female','Other') DEFAULT NULL COMMENT 'userSex',
  `userNationality` varchar(20) DEFAULT NULL COMMENT 'User Nationality',
  `userPicture` varchar(1000) DEFAULT NULL COMMENT 'User Picture',
  `password_reset_token` varchar(255) DEFAULT NULL,
  `fkCountryID` int(10) DEFAULT NULL,
  `userCity` varchar(255) DEFAULT NULL,
  `userAddress` varchar(255) DEFAULT NULL,
  `userStatus` enum('Active','Inactive','Block') NOT NULL DEFAULT 'Active',
  `created_at` bigint(11) DEFAULT NULL COMMENT 'User Added',
  `updated_at` bigint(11) DEFAULT NULL COMMENT 'User Modified',
  PRIMARY KEY (`pkUserRestaurantOwnerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_restaurant_owner`
--

INSERT INTO `user_restaurant_owner` (`pkUserRestaurantOwnerID`, `userName`, `userEmail`, `userNumber`, `password_hash`, `auth_key`, `userOTP`, `userSex`, `userNationality`, `userPicture`, `password_reset_token`, `fkCountryID`, `userCity`, `userAddress`, `userStatus`, `created_at`, `updated_at`) VALUES
(1, 'vasu', 'bansal.vasu1@gmail.com', 8010469674, '$2y$13$AJ22dr6SeDxNKMwbp1ow0.ggaY2WxGvYiuwScAyXrxBkXCBTT4a0G', 'HgB_6lAIJIfU4LmSIUUAsSPp_jR2mZOj', NULL, NULL, NULL, 'http://dev.iworklab.com/buzzq/frontend/web/profile_images/image-4-msioosi6ouedcsrylw81nqvwjqc-mq48', NULL, NULL, 'delhi', 'delhi', 'Active', 1484286814, 1484286814);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_restaurant_manage`
--
ALTER TABLE `user_restaurant_manage`
  ADD CONSTRAINT `user_restaurant_manage_ibfk_1` FOREIGN KEY (`fkRestaurantID`) REFERENCES `restaurant` (`pkRestaurantID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_restaurant_manage_ibfk_2` FOREIGN KEY (`fkUserRestaurantOwnerID`) REFERENCES `user_restaurant_owner` (`pkUserRestaurantOwnerID`) ON DELETE CASCADE ON UPDATE CASCADE;

